<div class="content-first content-shadow">
    <div id="profile-banner">
        <a href="{$playerHash.avatar}" class="avatar-link facebox-tr" facebox-type="fb-img">
            <img src="{$playerHash.avatar}" class="avatar fl player-avatar-chg" alt="" />
        </a>
        <div id="profile-banner-playerdata" class="fl">
            <div>
                {$playerHash.firstname} {$playerHash.lastname}
            </div> 
            <div style="font-size: 14px;">
                Dabei seit {$registeredSince} 
		{if $playerHash.id == $pgUser.id}<span class="profile-banner-thisisyou">(Das bist du!)</span>{/if}
            </div>
        </div>
    </div>

    <div class="fr">
        <div id="profile-controls" class="fr btn-group posRel">
            <div class="profile-controls-divider">
                <button class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}"><i class="icon-home"></i> Übersicht</button>
                <div class="control-dropdown-handle">
                    <button id="player-profile-btn-stats" class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}/stats"><i class="icon-bar-chart"></i> Statistik</button>
                    <div class="control-dropdown">
                        <div class="triangle"></div>
                        <ul>
                            <li><button class="btn-redirect" data-href="/player/{$playerHash.uid}/stats">Saison</button></li>
                            <li><button class="btn-redirect" data-href="/player/{$playerHash.uid}/stats/alltime">Alltime</button></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="profile-controls-divider">
                <button class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}/awards"><i class="icon-trophy"></i> Awards</button>
                <button class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}/seasons"><i class="icon-folder-open"></i> Saisons</button>
            </div>
            <div class="profile-controls-divider">
                <button class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}/matches"><i class="icon-fire"></i> Spiele</button>
                <div class="control-dropdown-handle">
                    <button class="btn-redirect btn-cntrl" data-href="/player/{$playerHash.uid}/compare"><i class="icon-signal"></i> Vergleich</button>
                    {if $playerHash.id != $pgUser.id}
		    <div class="control-dropdown">
                        <div class="triangle"></div>
                        <ul>
                            <li><button class="btn-redirect" data-href="/player/{$playerHash.uid}/compare">Saison</button></li>
                            <li><button class="btn-redirect" data-href="/player/{$playerHash.uid}/compare/alltime">Alltime</button></li>
                        </ul>
                    </div>
		    {/if}
                </div>
            </div>
        </div>
        <div class="fr marginr20 margint10" style="font-size: 47px; color: #eee;">//{$section}</div>
    </div>

    <div class="clear"></div>
</div>