{if $playerHash}

    {include 'banner.tpl'}

    {if $section == 'profile'}
        {include 'tabs/main.tpl'}

    {elseif $section == 'stats'}

        {include 'tabs/stats.tpl'}

    {elseif $section == 'awards'}

        {include 'tabs/awards.tpl'}

    {elseif $section == 'seasons'}

        {include 'tabs/seasons.tpl'}

    {elseif $section == 'compare'}

        {include 'tabs/compare.tpl'}

    {elseif $section == 'matches'}

        {include 'tabs/recentMatches.tpl'}

    {elseif $section == 'setup' && true == $isAuthorized}

        {include 'tabs/setup.tpl'}

    {/if}

{else}
<div class="content-first content-shadow">
    <h4 class="headline">
        <span>Fehler</span>
    </h4>
    Unbekannter Spieler
</div>
{/if}