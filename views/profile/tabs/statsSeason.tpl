<div class="grid4 content-first content-shadow" style="height: 184px;">
    <h4 class="headline"><span>Streaks</span></h4>
    <dl class="profileStats">
        <dt>Aktueller Win-Streak</dt>
        <dd>{$playerHash.stats.season.streaks.currentWinStreak}</dd>
        <dt>Aktueller Loss-Streak</dt>
        <dd>{$playerHash.stats.season.streaks.currentLossStreak}</dd>
        <dt>Längster Win-Streak</dt>
        <dd>{$playerHash.stats.season.streaks.bestWinStreak}</dd>
        <dt class="last">Längster Loss-Streak</dt>
        <dd class="last">{$playerHash.stats.season.streaks.bestLossStreak}</dd>
    </dl>
</div>

<div class="grid8 content-shadow" style="width: 602px;">
    <h4 class="headline"><span>Gegnerstatistik</span></h4>

    {$fearedPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedPlayer.playerId)}
    {$buddyPlayer = player('getHashForPlayerId', $playerHash.stats.season.buddyPlayer.playerId)}
    {$fearedTeamPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedTeamPlayer.playerId)}
    {$victimPlayer = player('getHashForPlayerId', $playerHash.stats.season.victimPlayer.playerId)}

    <div class="textcenter fl enemy-stats">
        {if $fearedPlayer}
            <a class="bold tipsy-tt" title="{$fearedPlayer.firstname} {$fearedPlayer.lastname}" href="/player/{$fearedPlayer.uid}">
                <img src="{$fearedPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angstgegner</div>
            {if $playerHash.stats.season.fearedPlayer.matchCount}
                {$playerHash.stats.season.fearedPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $victimPlayer}
            <a class="bold tipsy-tt" title="{$victimPlayer.firstname} {$victimPlayer.lastname}" href="/player/{$victimPlayer.uid}">
                <img src="{$victimPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Lieblingsgegner</div>
            {if $playerHash.stats.season.victimPlayer.matchCount}
                {$playerHash.stats.season.victimPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $buddyPlayer}
            <a class="bold tipsy-tt tipsy-n" title="{$buddyPlayer.firstname} {$buddyPlayer.lastname}" href="/player/{$buddyPlayer.uid}">
                <img src="{$buddyPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Kicker-Kumpel</div>
            {if $playerHash.stats.season.buddyPlayer.matchCount}
                {$playerHash.stats.season.buddyPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
    {if $fearedTeamPlayer}
        <a class="bold tipsy-tt" title="{$fearedTeamPlayer.firstname} {$fearedTeamPlayer.lastname}" href="/player/{$fearedTeamPlayer.uid}">
            <img src="{$fearedTeamPlayer.avatar}" alt="" class="avatar big" />
        </a>
        {else}
        <div class="avatar big unknown">?</div>
    {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angst-Mitspieler</div>
        {if $playerHash.stats.season.fearedTeamPlayer.matchCount}
            {$playerHash.stats.season.fearedTeamPlayer.matchCount} Spiele
            {else}
            ? Spiele
        {/if}
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="content-first content-shadow grid4">
    <h4 class="headline"><span>Spiele / Tore</span></h4>
    <dl class="profileStats">
        <dt>Spiele Gesamt</dt>
        <dd>{$playerHash.stats.season.countMatches}</dd>
        <dt>Gewonnene Spiele</dt>
        <dd>{$playerHash.stats.season.countWins}</dd>
        <dt>Verlorene Spiele</dt>
        <dd>{$playerHash.stats.season.countLosses}</dd>
        <dt class="last">Ratio</dt>
        <dd class="last">{$playerHash.stats.season.matchWinRatio}%</dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Erzielte Tore (Team)</dt>
        <dd>{$playerHash.stats.season.playerGoals}</dd>
        <dt class="last">Gegentore (Team)</dt>
        <dd class="last">{$playerHash.stats.season.enemyGoals}</dd>

        {*<dt>Tordifferenz</dt>
        <dd>{$playerHash.stats.season.playerGoals - $playerHash.stats.season.enemyGoals}</dd>*}
    </dl>
</div>
<div class="content-shadow grid4" style="height: 168px;">
    <h4 class="headline"><span>Monster Packs / Packs</span></h4>
    <dl class="profileStats">
        <dt>Pack verteilt</dt>
        <dd>{$playerHash.stats.season.packCount} <span class="italic">({$playerHash.stats.season.packQuote}%)</span></dd>
        <dt class="last">Pack kassiert</dt>
        <dd class="last">{$playerHash.stats.season.ownedPackCount} <span class="italic">({$playerHash.stats.season.ownedPackQuote}%)</span></dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Monster Pack verteilt</dt>
        <dd>{$playerHash.stats.season.monsterPackCount} <span class="italic">({$playerHash.stats.season.monsterPackQuote}%)</span></dd>
        <dt class="last">Monster Pack kassiert</dt>
        <dd class="last">{$playerHash.stats.season.ownedMonsterPackCount} <span class="italic">({$playerHash.stats.season.ownedMonsterPackQuote}%)</span></dd>
    </dl>
</div>
<div class="content-shadow grid4">
    <h4 class="headline"><span>Positionen</span></h4>
    <dl class="profileStats">
        <dt>Im Tor gespielt</dt>
        <dd>{$playerHash.stats.season.positions.goal}</dd>
        <dt>- davon gewonnen</dt>
        <dd>{$playerHash.stats.season.matchesWonInGoal} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.season.matchesWonInGoal, $playerHash.stats.season.positions.goal))}%)</span></dd>
        <dt class="last">- davon verloren</dt>
        <dd class="last">{$playerHash.stats.season.matchesLostInGoal} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.season.matchesLostInGoal, $playerHash.stats.season.positions.goal))}%)</span></dd>
        <dt class="last">&nbsp;</dt>
        <dd class="last">&nbsp;</dd>
        <dt>Im Sturm gespielt</dt>
        <dd>{$playerHash.stats.season.positions.offense}</dd>
        <dt>- davon gewonnen</dt>
        <dd>{$playerHash.stats.season.matchesWonInOffense} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.season.matchesWonInOffense, $playerHash.stats.season.positions.offense))}%)</span></dd>
        <dt class="last">- davon verloren</dt>
        <dd class="last">{$playerHash.stats.season.matchesLostInOffense} <span class="italic">({pgutils('calcPercents', array($playerHash.stats.season.matchesLostInOffense, $playerHash.stats.season.positions.offense))}%)</span></dd>
        {*<dt>&nbsp;</dt>
        <dd>&nbsp;</dd>
        <dt class="last">Alleine gespielt</dt>
        <dd class="last">{$playerHash.stats.season.positions.all}</dd>*}
    </dl>
</div>

<div class="clear"></div>

<div class="grid6 content-first content-shadow">
    <h4 class="headline"><span>Monster Packs / Packs / Spiele</span></h4>
    <div id="matches-packs"></div>
</div>

<div class="grid6 content-shadow">
    <h4 class="headline"><span>Siege / Niederlagen</span></h4>
    <div id="matches-stats"></div>
</div>

<div class="clear"></div>

<script type="text/javascript">
    $(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-stats',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Siege', 'Niederlagen'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.season.countWins}, {$playerHash.stats.season.countLosses}],
                color: '#3C3E45'
            }]
        });

        var matchSum = {math("$playerHash.stats.season.countMatches-$playerHash.stats.season.packCount-$playerHash.stats.season.monsterPackCount")};
        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-packs',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['MonsterPacks', 'Packs', 'Matches'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.season.monsterPackCount}, {$playerHash.stats.season.packCount}, matchSum],
                color: '#3C3E45'
            }]
        });
    });
</script>