{if $seasonStats}

    <div class="special-headline"><span>Statistik der Saison {$season.monthName}/{$season.year} ({$season.start} - {$season.end})</span></div>
    <div class="textcenter">
	<button class="btn-redirect margint15" data-href="/archive/season/{$season.year}/{$season.month}">
            <i class="icon-folder-open"></i> Die Tabelle zu dieser Saison gibt es im Archiv
        </button>
    </div>

    {$seasonStats}

{else}

	{if $seasons|count > 0}
	    {foreach reverse($seasons, true) sYear seasonYear name="seasonYears"}
		<div id="content" class="content-first content-shadow">
			<div class="textcenter posRel">

				<div class="special-headline content-toggle" style="margin-top: 0;">
					{if $dwoo.foreach.seasonYears.first}
					<span>Aktuelles Jahr ({$sYear})</span>
					{else}
					<span>Vergangene Saisons {$sYear}</span>
					{/if}
				</div>
				<div class="hide margint10">

                    {if $seasonYear|count > 0}
                        {foreach $seasonYear season}
                        <div class="season-tile">
                            <div>
                                <div class="season-title">{$season.monthName} {$season.year}</div>
                            </div>
                            <ul class="ranks">
                                {foreach $season.players player name="seasonTop3"}
                                    {if $player.stats.season.rank === 1}
                                        {$trophyType='gold'}
                                    {elseif $player.stats.season.rank === 2}
                                        {$trophyType='silver'}
                                    {elseif $player.stats.season.rank === 3}
                                        {$trophyType='bronze'}
                                    {else}
                                        {$trophyType=''}
                                    {/if}
                                    <li>
                                        {if $trophyType}
                                        <img src="/static/img/icons/trophies/trophy_{$trophyType}.png" alt="" class="fl" style="padding: 7px 5px;" />
                                        {else}
                                        <span class="fl{if $playerHash.id === $player.id} bold{/if}" style="width: 16px; height: 16px; padding: 7px 5px;">{$player.stats.season.rank}.</span>
                                        {/if}
                                        <span class="fl" style="padding: 7px 5px;">
                                            <a href="/player/{$player.uid}">
                                                {if $playerHash.id === $player.id}
                                                    <strong>{$player.firstname} {$player.lastname}</strong>
                                                {else}
                                                    {$player.firstname} {$player.lastname}
                                                {/if}
                                            </a>
                                            <span class="italic">({$player.stats.season.points} Pkt.)</span>
                                        </span><br class="clear" />
                                    </li>
                                {/foreach}
                            </ul>
                            <button class="btn-redirect" data-href="/archive/season/{$season.year}/{$season.month}">
                                <i class="icon-trophy"></i> Details zur Saison {$season.monthName} {$season.year}
                            </button>
                        </div>
                        {/foreach}
                    {else}
                        {$playerHash.firstname} hat keine Saison in diesem Jahr bestritten.
                    {/if}
					<div class="clear"></div>
				</div>
			</div>
		</div>
		{/foreach}

		<script>
			$(function() {
				setTimeout(function() {
					$('div.content-toggle:first').click();
				}, 600);
			});
		</script>

	{else}
		<div id="content" class="content-first content-shadow">
			<h4 class="headline">
				<span>Die bisherigen Saisons von {$playerHash.firstname}</span>
			</h4>
			Es stehen leider noch keine Saisons zur Verfügung
		</div>
	{/if}

{/if}