<div id="content" class="player-profile-awards content-first content-shadow">

    <h4 class="headline">
		<span>Spieler-Vergleich ({if $type == 'season'}Saison{else}Alltime{/if})</span>
		{if $selfCompare}
			{if $playerSeasons|count > 0}
			<span class="fr btn-group">
			<button id="choose-season" class="facebox-tr" href="#choose-seasons"><i class="icon-folder-open"></i>Saison wählen</button>
			</span>
			{/if}
		{/if}
    </h4>

    {if $error}

        {$error}

    {else}

	{if $selfCompare}
	    {if $playerSeasons|count > 0}
	    <div id="choose-seasons" class="hide">
		<h1>Saison zum Vergleich auswählen</h1>
		<div class="fb-notification-list-content">
		    <div class="notification-entry">
			<div>
			    <div class="textcenter">
					{foreach $playerSeasons sYear seasons name="playerSeasonsBox"}
					<div>
						<h4 class="headline" style="margin-top: 15px;">
							<span style="background-color: transparent !important;">Saisons {$sYear}</span>
						</h4>
						{foreach $seasons season}
						<button class="btn-redirect" data-href="/player/{$playerHash.uid}/compare/{$season.year}/{$season.month}">
							<i class="icon-trophy"></i> {$season.monthName} {$season.year}
						</button>
						{/foreach}
					</div>
					{/foreach}
			    </div>
			</div>
		    </div>
		    <div class="fb-controls textright">
			<button class="fb-close-trigger"><i class="icon-remove"></i>Schließen</button>
		    </div>
		</div>
	    </div>
	    {else}
		Für {$playerHash.firstname} stehen leider noch keine Saisons zur Verfügung
	    {/if}
	{/if}

        <table id="player-compare" style="width: 100%;">
            <thead>
                <tr>
                    <th id="versus">VS</th>
                    <th class="textcenter">
                        <span class="disBlock marginb5">
							<span class="bold">{$playerHash.firstname} {$playerHash.lastname}</span>

							{if $selfCompare}
							<br />(Aktuelle Saison)
							{/if}
						</span>
						<img src="{$playerHash.avatar}" alt="avatar" class="avatar big" />
                    </th>
                    <th class="textcenter v-last">
                        <span class="disBlock marginb5">
							<span class="bold">{$pgUser.firstname} {$pgUser.lastname}</span>

							{if $selfCompare}
							<br />({$seasonMonth} {$seasonYear})
							{/if}
						</span>
						<img src="{$pgUser.avatar}" alt="avatar" class="avatar big" />
                    </th>
                </tr>
            </thead>
            <tbody>

                {$profilePlayerStats = $playerHash.stats.$type}
                {$comparePlayerStats = $toComparePlayer.stats.$type}

                <tr>
                    <td class="sub">{if $type == 'season'}Saison{else}Alltime{/if}-Punkte</td>
                    <td class="textcenter">{$profilePlayerStats.points}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.points}</td>
                </tr>

                <tr>
                    <td class="sub">&Oslash; Punkte / Spiel</td>
                    <td class="textcenter">{$profilePlayerStats.averagePoints}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.averagePoints}</td>
                </tr>

                <tr>
                    <td class="sub">Sieg Ratio</td>
                    <td class="textcenter">{$profilePlayerStats.matchWinRatio}%</td>
                    <td class="textcenter v-last">{$comparePlayerStats.matchWinRatio}%</td>
                </tr>

                <tr>
                    <td class="sub">Gespielte Spiele</td>
                    <td class="textcenter">{$profilePlayerStats.countMatches}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.countMatches}</td>
                </tr>

                <tr>
                    <td class="sub">Gewonnene Spiele</td>
                    <td class="textcenter">{$profilePlayerStats.countWins}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.countWins}</td>
                </tr>

                <tr>
                    <td class="sub">Verlorene Spiele</td>
                    <td class="textcenter">{$profilePlayerStats.countLosses}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.countLosses}</td>
                </tr>

                <tr>
                    <td class="sub">Erzielte Tore (Team)</td>
                    <td class="textcenter">{$profilePlayerStats.playerGoals}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.playerGoals}</td>
                </tr>

                <tr>
                    <td class="sub {if $type == 'alltime'}h-last{/if}">Kassierte Tore (Team)</td>
                    <td class="textcenter{if $type == 'alltime'} h-last{/if}">{$profilePlayerStats.enemyGoals}</td>
                    <td class="textcenter v-last{if $type == 'alltime'} h-last{/if}">{$comparePlayerStats.enemyGoals}</td>
                </tr>

                {if $type == 'season'}
                <tr>
                    <td class="sub h-last" style="vertical-align: middle;">Punkte Trend</td>
                    <td class="textcenter h-last"><div id="pointTrendProfilePlayer"></div></td>
                    <td class="textcenter v-last h-last"><div id="pointTrendComparePlayer"></div></td>
                </tr>
                {/if}

                <tr>
                    <td colspan="3" class="v-last"><span class="category">Packs / Monster Packs</span></td>
                </tr>

                <tr>
                    <td class="sub">Packs verteilt</td>
                    <td class="textcenter">{$profilePlayerStats.packCount}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.packCount}</td>
                </tr>

                <tr>
                    <td class="sub">Pack Quote</td>
                    <td class="textcenter">{$profilePlayerStats.packQuote}%</td>
                    <td class="textcenter v-last">{$comparePlayerStats.packQuote}%</td>
                </tr>

                <tr>
                    <td class="sub">Monster Packs verteilt</td>
                    <td class="textcenter">{$profilePlayerStats.monsterPackCount}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.monsterPackCount}</td>
                </tr>

                <tr>
                    <td class="sub">Monster Pack Quote</td>
                    <td class="textcenter">{$profilePlayerStats.monsterPackQuote}%</td>
                    <td class="textcenter v-last">{$comparePlayerStats.monsterPackQuote}%</td>
                </tr>

                <tr>
                    <td class="sub">Packs kassiert</td>
                    <td class="textcenter">{$profilePlayerStats.ownedPackCount}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.ownedPackCount}</td>
                </tr>

                <tr>
                    <td class="sub">Monster Packs kassiert</td>
                    <td class="textcenter">{$profilePlayerStats.ownedMonsterPackCount}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.ownedMonsterPackCount}</td>
                </tr>

                <tr>
                    <td class="sub">Kassierte Packs Quote</td>
                    <td class="textcenter">{$profilePlayerStats.ownedPackQuote}%</td>
                    <td class="textcenter v-last">{$comparePlayerStats.ownedPackQuote}%</td>
                </tr>

                <tr>
                    <td class="sub h-last">Kassierte Monster Packs Quote</td>
                    <td class="textcenter h-last">{$profilePlayerStats.ownedMonsterPackQuote}%</td>
                    <td class="textcenter v-last h-last">{$comparePlayerStats.ownedMonsterPackQuote}%</td>
                </tr>

                <tr>
                    <td colspan="3" class="v-last"><span class="category">Streaks</span></td>
                </tr>

                {if $type == 'season'}
                <tr>
                    <td class="sub">Aktueller Streak</td>
                    <td class="textcenter">{$profilePlayerStats.streaks.currentStreak}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.streaks.currentStreak}</td>
                </tr>
                <tr>
                    <td class="sub">Aktueller Win Streak</td>
                    <td class="textcenter">{$profilePlayerStats.streaks.currentWinStreak}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.streaks.currentWinStreak}</td>
                </tr>
                <tr>
                    <td class="sub">Aktueller Loss Streak</td>
                    <td class="textcenter">{$profilePlayerStats.streaks.currentLossStreak}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.streaks.currentLossStreak}</td>
                </tr>
                {/if}

                <tr>
                    <td class="sub">Längster Win Streak</td>
                    <td class="textcenter">{$profilePlayerStats.streaks.bestWinStreak}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.streaks.bestWinStreak}</td>
                </tr>
                <tr>
                    <td class="sub h-last">Längster Loss Streak</td>
                    <td class="textcenter h-last">{$profilePlayerStats.streaks.bestLossStreak}</td>
                    <td class="textcenter v-last h-last">{$comparePlayerStats.streaks.bestLossStreak}</td>
                </tr>

                <tr>
                    <td colspan="3" class="v-last"><span class="category">Gegner Statistik</span></td>
                </tr>

                {$pFearedPlayer = player('getHashForPlayerId', $profilePlayerStats.fearedPlayer.playerId)}
                {$pBuddyPlayer = player('getHashForPlayerId', $profilePlayerStats.buddyPlayer.playerId)}
                {$pFearedTeamPlayer = player('getHashForPlayerId', $profilePlayerStats.fearedTeamPlayer.playerId)}
                {$pVictimPlayer = player('getHashForPlayerId', $profilePlayerStats.victimPlayer.playerId)}

                {$cFearedPlayer = player('getHashForPlayerId', $comparePlayerStats.fearedPlayer.playerId)}
                {$cBuddyPlayer = player('getHashForPlayerId', $comparePlayerStats.buddyPlayer.playerId)}
                {$cFearedTeamPlayer = player('getHashForPlayerId', $comparePlayerStats.fearedTeamPlayer.playerId)}
                {$cVictimPlayer = player('getHashForPlayerId', $comparePlayerStats.victimPlayer.playerId)}

                <tr>
                    <td class="sub">Angstgegner</td>
                    <td class="textcenter">{if $pFearedPlayer}<a href="/player/{$pFearedPlayer.uid}"><i class="icon-external-link"></i> {$pFearedPlayer.firstname} {$pFearedPlayer.lastname}</a>{else}-{/if}</td>
                    <td class="textcenter v-last">{if $cFearedPlayer}<a href="/player/{$cFearedPlayer.uid}"><i class="icon-external-link"></i> {$cFearedPlayer.firstname} {$cFearedPlayer.lastname}</a>{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="sub">Kicker Kumpel</td>
                    <td class="textcenter">{if $pBuddyPlayer}<a href="/player/{$pBuddyPlayer.uid}"><i class="icon-external-link"></i> {$pBuddyPlayer.firstname} {$pBuddyPlayer.lastname}</a>{else}-{/if}</td>
                    <td class="textcenter v-last">{if $cBuddyPlayer}<a href="/player/{$cBuddyPlayer.uid}"><i class="icon-external-link"></i> {$cBuddyPlayer.firstname} {$cBuddyPlayer.lastname}</a>{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="sub">Angst Mitspieler</td>
                    <td class="textcenter">{if $pFearedTeamPlayer}<a href="/player/{$pFearedTeamPlayer.uid}"><i class="icon-external-link"></i> {$pFearedTeamPlayer.firstname} {$pFearedTeamPlayer.lastname}</a>{else}-{/if}</td>
                    <td class="textcenter v-last">{if $cFearedTeamPlayer}<a href="/player/{$cVictimPlayer.uid}"><i class="icon-external-link"></i> {$cFearedTeamPlayer.firstname} {$cFearedTeamPlayer.lastname}</a>{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="sub h-last">Lieblingsgegner</td>
                    <td class="textcenter h-last">{if $pVictimPlayer}<a href="/player/{$pVictimPlayer.uid}"><i class="icon-external-link"></i> {$pVictimPlayer.firstname} {$pVictimPlayer.lastname}</a>{else}-{/if}</td>
                    <td class="textcenter h-last v-last">{if $cVictimPlayer}<a href="/player/{$cVictimPlayer.uid}"><i class="icon-external-link"></i> {$cVictimPlayer.firstname} {$cVictimPlayer.lastname}</a>{else}-{/if}</td>
                </tr>

                <tr>
                    <td colspan="3" class="v-last"><span class="category">Positionen</span></td>
                </tr>

                <tr>
                    <td class="sub">Alle (Gesamt)</td>
                    <td class="textcenter">{$profilePlayerStats.positions.all}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.positions.all}</td>
                </tr>
                <tr>
                    <td class="sub">Tor (Gesamt)</td>
                    <td class="textcenter">{$profilePlayerStats.positions.goal}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.positions.goal}</td>
                </tr>
                <tr>
                    <td class="sub2">Siege im Tor</td>
                    <td class="textcenter">{$profilePlayerStats.matchesWonInGoal}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.matchesWonInGoal}</td>
                </tr>

                <tr>
                    <td class="sub2">Niederlagen im Tor</td>
                    <td class="textcenter">{$profilePlayerStats.matchesLostInGoal}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.matchesLostInGoal}</td>
                </tr>

                <tr>
                    <td class="sub">Sturm (Gesamt)</td>
                    <td class="textcenter">{$profilePlayerStats.positions.offense}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.positions.offense}</td>
                </tr>
                <tr>
                    <td class="sub2">Siege im Sturm</td>
                    <td class="textcenter">{$profilePlayerStats.matchesWonInOffense}</td>
                    <td class="textcenter v-last">{$comparePlayerStats.matchesWonInOffense}</td>
                </tr>
                <tr>
                    <td class="sub2 h-last">Niederlagen im Sturm</td>
                    <td class="textcenter h-last">{$profilePlayerStats.matchesLostInOffense}</td>
                    <td class="textcenter h-last v-last">{$comparePlayerStats.matchesLostInOffense}</td>
                </tr>

            </tbody>
        </table>

        {if $type == 'season'}
        <script>
            var pointTrendProfilePlayerStats = {$profilePlayerStats.pointTrend};
            var pointTrendComparePlayerStats = {$comparePlayerStats.pointTrend};

            var pointTrendProfilePlayerChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'pointTrendProfilePlayer',
                    defaultSeriesType: 'line',
                    backgroundColor: '#fff',
                    height: 163
                },
                title: {
                    text: null
                },
                xAxis: {
                    gridLineColor: '#ccc',
                    lineColor: '#ccc',
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: 'Punktetrend der letzten 15 Spiele',
                        style: {
                            'color': '#3C3E45',
                            'font-weight': 'normal'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    gridLineColor: '#ccc',
                    lineColor: '#3C3E45'
                },
                tooltip: {
                    formatter: function() {
                        return '<strong>' + this.y + ' Punkte</strong>';
                    },
                    borderColor: '#ccc'
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Punkteverlauf',
                        data: pointTrendProfilePlayerStats,
                        color: '#3C3E45'
                    }]
            });
            var pointTrendComparePlayerChart = new Highcharts.Chart({
                chart: {
                    renderTo: 'pointTrendComparePlayer',
                    defaultSeriesType: 'line',
                    backgroundColor: '#fff',
                    height: 163
                },
                title: {
                    text: null
                },
                xAxis: {
                    gridLineColor: '#ccc',
                    lineColor: '#ccc',
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: 'Punktetrend der letzten 15 Spiele',
                        style: {
                            'color': '#3C3E45',
                            'font-weight': 'normal'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    gridLineColor: '#ccc',
                    lineColor: '#3C3E45'
                },
                tooltip: {
                    formatter: function() {
                        return '<strong>' + this.y + ' Punkte</strong>';
                    },
                    borderColor: '#ccc'
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Punkteverlauf',
                        data: pointTrendComparePlayerStats,
                        color: '#3C3E45'
                    }]
            });
        </script>
        {/if}

    {/if}

</div>