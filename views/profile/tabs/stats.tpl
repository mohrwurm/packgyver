{if $statsType == 'season'}

    <div class="special-headline"><span>Statistiken der aktuellen Saison</span></div>
    {include 'statsSeason.tpl'}

{else}

    <div class="special-headline marginb15"><span>Statistiken der gesamten PackGyver Laufzeit</span></div>
    <div class="textcenter special-headline-info">(Letzte Aktualisierung: {$playerHash.stats.alltime.lastUpdate})</div>
    {include 'statsAlltime.tpl'}

{/if}