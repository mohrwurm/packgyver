<script src="/static/js/plugins/jquery.form.js"></script>
<div class="content-first grid4 content-shadow" style="width: 245px;">
    <h4 class="headline">
        <span>Avatar ändern</span>
    </h4>
    <div class="playerAvatarContainer textcenter">
        <img src="{$playerHash.avatar}" class="player-avatar-chg avatar big" alt="" style="height: 210px; width: 170px;" />
    </div>
    <div id="avatarUpload" class="textcenter">
        <div class="profileAvatarEdit special-headline-info" style="margin-top: 10px;">
            (Größe: max 500KB / Format: .png/.jpg)
        </div>
        <form method="post" enctype="multipart/form-data" action='/players/handleavatar'>
            <button class="fakeUploadBtn margint20"><i class="icon-upload"></i>Upload</button>
            <input type="file" player="{$playerHash.id}" name="avatarUpload" class="fakeUpload" id="avatarFileInput" />
        </form>
    </div>
</div>
<div class="content-shadow" style="width: 627px; float: right;">
    <h4 class="headline">
        <span>Details</span>
    </h4>
    <form action="" method="post">
        <div class="marginb5">
            <label for="firstname">Vorname</label>
            <input type="text" id="firstname" name="firstname" value="{$playerHash.firstname}" disabled="disabled" />
        </div>
        <div class="marginb5">
            <label for="lastname">Nachname</label>
            <input type="text" id="lastname" name="lastname" value="{$playerHash.lastname}" disabled="disabled" />
        </div>
        <div class="marginb5">
            <label for="email">Email</label>
            <input type="text" id="email" name="email" value="{$playerHash.email}" disabled="disabled" />
        </div>
        <div class="marginb5">
            <label for="gtalk">Google Talk</label>
            <input type="text" id="gtalk" name="gtalk" value="{$playerHash.gtalk}" disabled="disabled" />
        </div>
    </form>
</div>
<div class="content-shadow" style="width: 627px; float: right;">
    <h4 class="headline">
        <span>Passwort ändern</span>
    </h4>
    <div id="changePassword">
        <form method="post" action="/players/changePassword" autocomplete="off">
            <div class="marginb10">
                <label for="oldPassword">Dein aktuelles Passwort:</label>
                <input type="password" player="{$playerHash.id}" name="oldPassword" id="oldPassword" />
            </div>

            <div class="marginb10">
                <label for="newPassword">Dein neues Passwort:</label>
                <input type="password" player="{$playerHash.id}" name="newPassword" id="newPassword" />
            </div>

            <div class="marginb10">
                <span class="italic">Das Passwort muss mindestens 4 Zeichen lang sein.</span>
            </div>

            <input type="hidden" name="playerId" value="{$playerHash.id}" />
            <button type="submit">Ändern und derzeitige Session löschen</button>
        </form>
    </div>
</div>

<script>
    $(function() {
        $('div#avatarUpload .fakeUploadBtn').click(function(e) {
            e.preventDefault();
            $('div#avatarUpload input#avatarFileInput').trigger('click');
        });
    });
</script>