<div id="content" class="content-shadow">
    <div id="not-found-page">
	<img src="/static/img/404.png" alt="404" />
	<div class="nf-headline">D´HO!</div>
	<div class="nf-text">
	    <strong>Die angeforderte Seite konnte leider nicht gefunden werden.</strong>
	    <div class="nf-text-small">
		Wenn du denkst, dass dies ein Fehler ist, sende bitte ein <a href="#pg-feedback-open" class="facebox-tr">Feedback</a> an PackGyver.
	    </div>
	</div>
    </div>
</div>