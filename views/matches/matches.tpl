<div id="match-content" class="content-first content-shadow grid8">
    <h4 class="headline">
        <span>Spiele</span>
        <span class="fr headline-info">Alle Spiele der Saison {$season.monthName}/{$season.year} ({$season.start} - {$season.end})</span>
    </h4>

    {if $function === 'all'}

        {include 'filter.tpl'}

        <div id="match-entries">
            {if $matches|count > 0}

                <div class="marginb10">{$pagination}</div>

                {include 'matchListItem.tpl'}

                <div class="margint20">{$pagination}</div>
            {else}

                <div class="alert textcenter" style="margin-bottom: 0;">
                    Es wurden keine Spiele zur Auswahl gefunden.
                </div>

            {/if}
        </div>

    {/if}
</div>
<div id="sidebar" class="grid4">
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerToday.tpl'}
    </div>
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerSeason.tpl'}
    </div>
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerAlltimeRanking.tpl'}
    </div>
</div>