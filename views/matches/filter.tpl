<div id="match-filter">
    <form action="" method="get">
        {$playerHashes=player('getPlayers', true)}

        <div class="margint15 fl" style="width: 320px;">
            <h2 class="bold">Spieler</h2>

            <div>
                <label for="flt-createdBy">
                    Erstellt von:
                </label>
                <select id="flt-createdBy" name="createdBy" style="width: 175px;">
                    <option value="0">- Auswählen -</option>
                    {foreach $playerHashes playerHash}
                        <option value="{$playerHash.id}"{if $filter.createdBy == $playerHash.id} selected="selected"{/if}>{$playerHash.firstname} {$playerHash.lastname}</option>
                    {/foreach}
                </select>
            </div>

            <div>
                <label for="flt-savedBy">
                    Gespeichert von:
                </label>
                <select id="flt-savedBy" name="savedBy" style="width: 175px;">
                    <option value="0">- Auswählen -</option>
                    {foreach $playerHashes playerHash}
                        <option value="{$playerHash.id}"{if $filter.savedBy == $playerHash.id} selected="selected"{/if}>{$playerHash.firstname} {$playerHash.lastname}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="margint15 fl" style="width: 320px;">
            <h2 class="bold">Zeitraum</h2>

            <div style="position: relative;">
                <label for="flt-dateSavedFrom">
                    Erstellt:
                </label>
                <input type="text" id="flt-dateSavedFrom" name="dateSavedFrom" value="{$filter.dateSavedFrom}" class="pickert" style="width: 175px;"/>
            </div>
            <div style="position: relative;">
                <label for="flt-dateSavedTo">
                    Erstellt bis:
                </label>
                <input type="text" id="flt-dateSavedTo" name="dateSavedTo" value="{$filter.dateSavedTo}" class="pickert" style="width: 175px;"/>
            </div>
        </div>

        <div class="margint10 fl" style="width: 320px;">
            <div>

                <label for="flt-matchResultType">
                    Zeige:
                </label>
                <select id="flt-matchResultType" name="matchResultType" style="width: 175px;" class="no-search">
                    <option value=""{if !$filter.matchResultType} selected="selected"{/if}>Alle</option>
                    <option value="mp"{if $filter.matchResultType === 'mp'} selected="selected"{/if}>MonsterPacks</option>
                    <option value="p"{if $filter.matchResultType === 'p'} selected="selected"{/if}>Packs</option>
                </select>
            </div>
        </div>
        <div class="margint10 fl" style="width: 320px;">
            <div>
                <label for="flt-sortMode">
                    Sortierung:
                </label>
                <select id="flt-sortMode" name="sortMode" style="width: 175px;" class="no-search">
                    <option value="ASC"{if $filter.sortMode === 'ASC'} selected="selected"{/if}>Aufsteigend</option>
                    <option value="DESC"{if $filter.sortMode === 'DESC'} selected="selected"{/if}>Absteigend</option>
                </select>
            </div>
        </div>
        <div class="clear"></div>

        <div class="textcenter margint15">
            <input type="hidden" name="filter" value="1" />
            <a href="/matches" style="font-size: 11px;">Filter zurücksetzen</a>
            <button class="btn"><i class="icon-search"></i> Spiele filtern</button>
        </div>

        {if $results >= 0}
        <div class="textright" style="font-size: 11px;">
            ({$results} Spiele gefunden)
        </div>
        {/if}
    </form>
</div>