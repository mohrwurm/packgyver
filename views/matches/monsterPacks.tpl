<div id="match-content" class="content-first content-shadow grid8">
    <h4 class="headline">
        <span>Alltime MonsterPacks ({$matches|count})</span>
    </h4>

    <div id="match-entries">
        {if $matches|count > 0}

            {include 'matchListItem.tpl'}

        {else}
            Keine MonsterPacks vefügbar.
        {/if}
    </div>
</div>
<div id="sidebar" class="grid4">
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerToday.tpl'}
    </div>
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerSeason.tpl'}
    </div>
    <div class="content-shadow">
        {include '../inc/sidebar/mostMatchesPlayerAlltimeRanking.tpl'}
    </div>
</div>