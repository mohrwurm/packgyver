<div class="margint10">
    <div class="marginb10">Dir wurde ein neuer Award verliehen:</div>
    <div class="award fl">
        <img src="{$award.logo}" alt="">
    </div>
    <div class="award-description margint15 fl">
        <div class="bold">{$award.name}</div>
        {$award.description}
    </div>
    <div class="clear"></div>
</div>