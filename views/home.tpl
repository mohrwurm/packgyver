{$curStreak = $playerHash.stats.season.streaks.currentStreak}

{if !$playerHash.active}
<div id="player-home-inactive" class="alert alert-info">
    <strong>Du wurdest deaktiviert - </strong> Du kannst an keinem Spiel teilnehmen und wirst nicht in der Tabelle angezeigt.
    Melde dich bei einem Admin, wenn du wieder aktiviert werden möchtest.
</div>
{/if}

<div class="content-first content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Derzeitiger Platz</span></h4>
    <div style="font-size: 72px;" class="textcenter">{$seasonRank}</div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>&Oslash; Punkte / Spiel</span></h4>
    <div style="font-size: 72px;" class="textcenter">{$playerHash.stats.season.averagePoints}</div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Packs / MonsterP.</span></h4>
    <div style="font-size: 72px;" class="textcenter nowrap">
        {$playerHash.stats.season.packCount} / {$playerHash.stats.season.monsterPackCount}
    </div>
</div>
<div class="content-shadow grid3" style="width: 187px;">
    <h4 class="headline"><span>Aktueller Streak</span></h4>

    {if player('isStreakPositive', $curStreak)}
        {$streakClass = ' posTrend'}
    {elseif 0 == $curStreak}
        {$streakClass = ''}
    {else}
        {$streakClass = ' negTrend'}
    {/if}
    <div style="font-size: 72px;" class="textcenter{$streakClass}">{$playerHash.stats.season.streaks.currentStreak}</div>
</div>

<div class="grid6 content-first content-shadow paddingb0">
    <h4 class="headline"><span>15 Spiele Trend</span></h4>
    <div id="pointTrend"></div>
</div>

<div class="grid6 content-shadow" style="height: 168px;">
    <h4 class="headline"><span>Mein Ranking</span></h4>

	{if $tableSequence}
    <table class="ranking sidebar-player-ranking">
        <thead>
            <tr>
                <th class="textcenter bold">Pl.</th>
                <th class="bold">Name</th>
                <th class="bold textcenter">Streak</th>
                <th class="bold textcenter">Spiele</th>
                <th class="textcenter bold last-h">Pkt.</th>
            </tr>
        </thead>
        <tbody>
            {foreach $tableSequence tsPlayer name="tableSequence"}
            {if $dwoo.foreach.tableSequence.last}
                {$isLastV=' last-v'}
            {/if}

            {if $tsPlayer.tsPointsState == '+'}
                {$tsDiff = ' posTrend'}
                {$tsArrow = '&UpArrow;'}
                {$pointsTip = ' Punkte im Vorsprung'}
            {elseif $tsPlayer.tsPointsState == '-'}
                {$tsDiff = ' negTrend'}
                {$tsArrow = '&DownArrow;'}
                {$pointsTip = ' Punkte im Rückstand'}
            {else}
                {$tsDiff = ''}
                {$tsArrow = ''}
                {$pointsTip = ''}
            {/if}

            {if player('isStreakPositive', $tsPlayer.stats.season.streaks.currentStreak)}
                {$streak=' posTrend'}
            {elseif 0 != $tsPlayer.stats.season.streaks.currentStreak}
                {$streak=' negTrend'}
            {else}
                {$streak=''}
                {$streakTip = ''}
            {/if}

            <tr class="{cycle values=array('','odd')}">
                <td class="big textcenter ranking-rank{$isLastV}">{$tsPlayer.stats.season.rank}.</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}{$isLastV}">
                    <img src="{$tsPlayer.avatar}" alt="" class="avatar small fl" />
                    <span class="ranking-player-name"><a href="/player/{$tsPlayer.uid}">{$tsPlayer.firstname} {$tsPlayer.lastname}</a></span>
                </td>
                <td class="textcenter ranking-rank{$isLastV}{$streak}">{$tsPlayer.stats.season.streaks.currentStreak}</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}textcenter ranking-rank{$isLastV}">{$tsPlayer.stats.season.countMatches}</td>
                <td class="{if $tsPlayer.id == $playerHash.id}bold {/if}textcenter {if $pointsTip} tipsy-tt{/if} last-h{$isLastV}{$tsDiff}" title="{if '' !== $pointsTip}{$tsPlayer.tsPoints} {$pointsTip}{/if}">{$tsPlayer.tsPointsState}{$tsPlayer.tsPoints} {$tsArrow}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
	{else}
        <div style="margin-top: 75px;" class="textcenter">Es steht kein Ranking zur Verfügung</div>
	{/if}
</div>

<div class="grid4 content-first content-shadow">
    <h4 class="headline"><span>Siege / Niederlagen</span></h4>
    <div id="matches-stats"></div>
</div>

<div class="grid8 content-shadow" style="width: 602px; height: 188px;">
    <h4 class="headline"><span>Gegnerstatistik</span></h4>

    {$fearedPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedPlayer.playerId)}
    {$buddyPlayer = player('getHashForPlayerId', $playerHash.stats.season.buddyPlayer.playerId)}
    {$fearedTeamPlayer = player('getHashForPlayerId', $playerHash.stats.season.fearedTeamPlayer.playerId)}
    {$victimPlayer = player('getHashForPlayerId', $playerHash.stats.season.victimPlayer.playerId)}

    <div class="textcenter fl enemy-stats">
        {if $fearedPlayer}
            <a class="bold tipsy-tt" title="{$fearedPlayer.firstname} {$fearedPlayer.lastname}" href="/player/{$fearedPlayer.uid}">
                <img src="{$fearedPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angstgegner</div>
            {if $playerHash.stats.season.fearedPlayer.matchCount}
                {$playerHash.stats.season.fearedPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $victimPlayer}
            <a class="bold tipsy-tt" title="{$victimPlayer.firstname} {$victimPlayer.lastname}" href="/player/{$victimPlayer.uid}">
                <img src="{$victimPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Lieblingsgegner</div>
            {if $playerHash.stats.season.victimPlayer.matchCount}
                {$playerHash.stats.season.victimPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
        {if $buddyPlayer}
            <a class="bold tipsy-tt tipsy-n" title="{$buddyPlayer.firstname} {$buddyPlayer.lastname}" href="/player/{$buddyPlayer.uid}">
                <img src="{$buddyPlayer.avatar}" alt="" class="avatar big" />
            </a>
            {else}
            <div class="avatar big unknown">?</div>
        {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Kicker-Kumpel</div>
            {if $playerHash.stats.season.buddyPlayer.matchCount}
                {$playerHash.stats.season.buddyPlayer.matchCount} Spiele
                {else}
                ? Spiele
            {/if}
        </div>
    </div>
    <div class="textcenter fl enemy-stats">
    {if $fearedTeamPlayer}
        <a class="bold tipsy-tt" title="{$fearedTeamPlayer.firstname} {$fearedTeamPlayer.lastname}" href="/player/{$fearedTeamPlayer.uid}">
            <img src="{$fearedTeamPlayer.avatar}" alt="" class="avatar big" />
        </a>
        {else}
        <div class="avatar big unknown">?</div>
    {/if}
        <div class="em paddingt10">
            <div class="bold marginb5">Angst-Mitspieler</div>
        {if $playerHash.stats.season.fearedTeamPlayer.matchCount}
            {$playerHash.stats.season.fearedTeamPlayer.matchCount} Spiele
            {else}
            ? Spiele
        {/if}
        </div>
    </div>
</div>

<div class="grid6 content-first content-shadow">
    <h4 class="headline"><span>Monster Packs / Packs / Spiele</span></h4>
    <div id="matches-packs"></div>
</div>

<div class="grid6 content-shadow" style="height: 188px;">
    <h4 class="headline"><span>Letzte 3 Spiele</span></h4>
    {if count($recentMatches) > 0}
    <div id="sidebar-latest-matches">
        {foreach $recentMatches match name="sbRecentMatches"}
        <div{if $dwoo.foreach.sbRecentMatches.last} class="last"{/if}>
            <div class="team-container fl">
                {if $match.type == '2on2'}
                    <div>
                        <span class="position teamRed">T</span>
                        <a href="/player/{$match.players.left.goal.uid}">
                            {if $match.players.left.goal.id == $playerHash.id}
                                <strong>{$match.players.left.goal.firstname} {$match.players.left.goal.lastname}</strong>
                            {else}
                                {$match.players.left.goal.firstname} {$match.players.left.goal.lastname}
                            {/if}
                        </a>
                    </div>
                    <div style="margin-top: 10px;">
                        <span class="position teamRed">S</span>
                        <a href="/player/{$match.players.left.offense.uid}">
                            {if $match.players.left.offense.id == $playerHash.id}
                                <strong>{$match.players.left.offense.firstname} {$match.players.left.offense.lastname}</strong>
                            {else}
                                {$match.players.left.offense.firstname} {$match.players.left.offense.lastname}
                            {/if}
                        </a>
                    </div>
                {else}
                    <span class="position teamRed">A</span>
                    <a href="/player/{$match.players.left.all.uid}">
                        {$match.players.left.all.firstname} {$match.players.left.all.lastname}
                    </a>
                {/if}
            </div>
            <div class="textcenter fl" style="margin-top: 5px;">
                <span class="score" style="color: #666666; font-size: 30px;">{$match.score.left}:{$match.score.right}</span>
                <br />
                <!--<span class="dateInfo">{$match.dateSaved}</span>-->
            </div>
            <div class="textright fl team-container">
                {if $match.type == '2on2'}
                    <div>
                        <a href="/player/{$match.players.right.offense.uid}">
                            {if $match.players.right.offense.id == $playerHash.id}
                                <strong>{$match.players.right.offense.firstname} {$match.players.right.offense.lastname}</strong>
                            {else}
                                {$match.players.right.offense.firstname} {$match.players.right.offense.lastname}
                            {/if}
                        </a>
                        <span class="position teamYellow">S</span>
                    </div>
                    <div style="margin-top: 10px;">
                        <a href="/player/{$match.players.right.goal.uid}">
                            {if $match.players.right.goal.id == $playerHash.id}
                                <strong>{$match.players.right.goal.firstname} {$match.players.right.goal.lastname}</strong>
                            {else}
                                {$match.players.right.goal.firstname} {$match.players.right.goal.lastname}
                            {/if}
                        </a>
                        <span class="position teamYellow">T</span>
                    </div>
                {else}
                    <span class="position teamYellow">A</span>
                    <a href="/player/{$match.players.right.all.uid}">
                        {$match.players.right.all.firstname} {$match.players.right.all.lastname}
                    </a>
                {/if}
            </div>
            <div class="clear"></div>
        </div>
        {/foreach}
        </div>
    {else}
        <div style="margin-top: 80px;" class="textcenter">Du hast noch kein Spiel bestritten</div>
    {/if}
</div>

<script type="text/javascript">
    var pointTrendSeasonMain = {$playerHash.stats.season.pointTrend};
    
    $(function() {
        new Highcharts.Chart({
            chart: {
                renderTo: 'pointTrend',
                defaultSeriesType: 'line',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                gridLineColor: '#ccc',
                lineColor: '#ccc',
                labels: {
                    enabled: false
                },
                title: {
                    text: 'Punktetrend der letzten 15 Spiele',
                    style: {
                        'color': '#3C3E45',
                        'font-weight': 'normal'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                },
                gridLineColor: '#ccc',
                lineColor: '#3C3E45'
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.y + ' Punkte</strong>';
                },
                borderColor: '#ccc'
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                    name: 'Punkteverlauf',
                    data: pointTrendSeasonMain,
                    color: '#3C3E45'
                }]
        });

        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-stats',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Siege', 'Niederlagen'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.season.countWins}, {$playerHash.stats.season.countLosses}],
                color: '#3C3E45'
            }]
        });

        var matchSum = {math("$playerHash.stats.season.countMatches-$playerHash.stats.season.packCount-$playerHash.stats.season.monsterPackCount")};
        new Highcharts.Chart({
            chart: {
                renderTo: 'matches-packs',
                defaultSeriesType: 'bar',
                backgroundColor: '#fff',
                height: 163
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['MonsterPacks', 'Packs', 'Matches'],
                        gridLineColor: '#ccc',
                        lineColor: '#ccc',
                        labels: {
                            enabled: false
                        },
                        title: {
                            enabled: false  
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null
                        },
                        gridLineColor: '#ccc',
                        lineColor: '#3C3E45'
                    },


            tooltip: {
                formatter: function() {
                    return ''+
                        this.y +' Spiele';
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                    enabled: true
                    }
                }
            },
            legend: {
                enabled: false 
            },
            credits: {
                enabled: false
            },
            series: [{
                name: null,
                data: [{$playerHash.stats.season.monsterPackCount}, {$playerHash.stats.season.packCount}, matchSum],
                color: '#3C3E45'
            }]
        });
    });
</script>