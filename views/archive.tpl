{if $error}
<div id="content" class="content-first content-shadow">
	<h4 class="headline">
		<span>Saison-Archiv</span>
	</h4>
	Es stehen keine Daten zur Verfügung.
</div>

{else}

	{if $function == 'season'}
	<div id="content" class="content-first content-shadow">
		<h4 class="headline">
			<span>Saison-Archiv</span>
			<span class="headline-info fr">Die Rangliste der Saison {$season.monthName}/{$season.year} ({$season.start} bis {$season.end})</span>
		</h4>
		<table id="ranking-archive" class="ranking ranking-big">
			<thead>
				<tr>
					<th class="textcenter" style="width: 40px;">Platz</th>
					<th>Name</th>
					<th class="textcenter">Ratio</th>
					<th class="textcenter">Spiele</th>
					<th class="textcenter tipsy-tt" title="Gewonnene Spiele">Gew.</th>
					<th class="textcenter tipsy-tt" title="Verlorene Spiele">Verl.</th>
					<th class="textcenter">Tore</th>
					<th class="textcenter tipsy-tt" title="Differenz Tore - Gegentore">Diff.</th>
					<th class="textcenter tipsy-tt" title="Bester Win Streak">+ Streak</th>
					<th class="textcenter tipsy-tt" title="Längster Loss Streak">- Streak</th>
					<th class="textcenter tipsy-tt" title="Anzahl Packs">Packs</th>
					<th class="textcenter tipsy-tt" title="Anzahl Monster Packs">MPacks.</th>
					<th class="textcenter last-h">Punkte</th>
				</tr>
			</thead>
			<tbody>
				{foreach $players rank player name="archiveRanking"}
					{if $player.stats.season.streaks.bestWinStreak > 0}
						{$streakWin=' posTrend'}
					{else}
						{$streakWin=''}
					{/if}

					{if $player.stats.season.streaks.bestLossStreak > 0}
						{$streakLoss=' negTrend'}
					{else}
						{$streakLoss=''}
					{/if}

					{$isLastV = ''}
					{if $dwoo.foreach.archiveRanking.last}
						{$isLastV=' last-v'}
					{/if}

					<tr class="{cycle values=array('','odd')}">
						<td class="big textcenter ranking-rank{$isLastV}">{$rank + 1}.</td>
						<td class="{$isLastV}">
							<span class="pgStatus{if $player.checkedIn} checkedIn{else} checkedOut{/if}"></span>
							<img src="{$player.avatar}" alt="" class="avatar small fl" />
							<span class="ranking-player-name"><a href="/player/{$player.uid}">{$player.firstname} {$player.lastname}</a></span>
						</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.matchWinRatio}%</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.countMatches}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.countWins}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.countLosses}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.playerGoals}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.playerGoals-$player.stats.season.enemyGoals}</td>
						<td class="textcenter{$isLastV}{$streakWin}">+{$player.stats.season.streaks.bestWinStreak}</td>
						<td class="textcenter{$isLastV}{$streakLoss}">-{$player.stats.season.streaks.bestLossStreak}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.packCount}</td>
						<td class="textcenter{$isLastV}">{$player.stats.season.monsterPackCount}</td>
						<td class="textcenter bold last-h{$isLastV}">{$player.stats.season.points}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
		<div class="margint15 textright">
			<button class="btn-redirect" data-href="/archive"><i class="icon-arrow-left"></i> Zurück zur Übersicht</button>
		</div>
	</div>

	{else}

		{if $seasons|count > 0}
			{foreach reverse($seasons, true) sYear seasonYear name="seasonYears"}
			<div id="content" class="content-first content-shadow">
				<div class="textcenter posRel">

					<div class="special-headline content-toggle" style="margin-top: 0;">
						{if $sYear == $currentYear}
						<span>Aktuelle Saison ({$sYear})</span>
						{else}
						<span>Vergangene Saison ({$sYear})</span>
						{/if}
					</div>
					<div class="hide margint10">

						{foreach $seasonYear season}
						<div class="season-tile">
							<div>
								<div class="season-title">{$season.monthName} {$season.year}</div>
							</div>
							<ul class="ranks">
								{foreach $season.players player name="seasonTop3"}
									{if $dwoo.foreach.seasonTop3.index === 0}
										{$trophyType='gold'}
									{elseif $dwoo.foreach.seasonTop3.index === 1}
										{$trophyType='silver'}
									{else}
										{$trophyType='bronze'}
									{/if}
									<li>
										<img src="/static/img/icons/trophies/trophy_{$trophyType}.png" alt="" class="fl" style="padding: 7px 5px;" />
										<span class="fl" style="padding: 7px 5px;"><strong>{$player.firstname} {$player.lastname}</strong> <span class="italic">({$player.stats.season.points} Pkt.)</span></span><br class="clear" />
									</li>
								{/foreach}
							</ul>
							<button class="btn-redirect" data-href="/archive/season/{$season.year}/{$season.month}">
								<i class="icon-trophy"></i> Details zur Saison {$season.monthName} {$season.year}
							</button>
						</div>
						{/foreach}
						<div class="clear"></div>
					</div>
				</div>
			</div>
			{/foreach}

		{else}

			<div id="content" class="content-first content-shadow">
				<h4 class="headline">
					<span>Saison-Archiv</span>
				</h4>
				Es wurden leider noch keine Saisons archiviert.
			</div>
		{/if}

	{/if}
{/if}

</div>

<script>
	$(function() {
		setTimeout(function() {
			$('div.content-toggle:first').click();
		}, 600);
	});
</script>