<div id="sidebar" class="content-first grid3">
    <div class="content-shadow">
        <h4 class="headline"><span>Index</span></h4>
        <ul class="box-navigation-simple">
            <li><a href="/info"{if !$function} class="active"{/if}>Über PackGyver</a></li>
            <li><a href="/info/rules"{if $function == 'rules'} class="active"{/if}>Regeln</a></li>
            <li><a href="/info/points"{if $function == 'points'} class="active"{/if}>Punkteübersicht</a></li>

			{if player('isAdmin')}
				<li><a href="/admin" class="red bold">Admin</a></li>
			{/if}
        </ul>
    </div>
</div>
<div id="content" class="grid9 content-shadow">

    {if $function == 'rules'}
    <h4 class="headline"><span>Regeln</span></h4>
    <ul id="rules">
        <li>
            <div class="paragraph">§</div>
            <div class="paragraph-text">
                Die Spielsituation darf nicht manipuliert werden. Pusten oder ein Anheben des Tisches sind unzulässig.
            </div>
            <div class="clear"></div>
        </li>
	<li>
            <div class="paragraph">§</div>
            <div class="paragraph-text">
                Es darf nicht gekurbelt werden. Sollte ein Spieler kurbeln, so wird
das Spiel unterbrochen und der Ball neu eingeworfen. Der kurbelnde
Spieler darf bis zum nächsten Tor seines Teams oder des gegnerischen
Teams nur mit der linken Hand spielen. Sollte er in dieser Zeit wieder kurbeln, so darf er seine Spieler einmal positionieren und muss sich dann vom Tisch entfernen,
bis ein Tor gefallen ist.
            </div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="paragraph">§</div>
            <div class="paragraph-text">
                Der Ball darf nicht mit übermäßigem Gewalteinfluss eingeworfen werden. Ein so erzeiltes Tor ist ungültig 
                und darf nicht gezählt werden. Der Ball darf jedoch in Richtung der 5er-Reihe angedreht werden.
                <br /><br />
                <div class="marginb5">Das Liegenbleiben des Balles wird  wie folgt geregelt:</div>
                <ul>
                    <li>Situation: Der Ball liegt zwischen Tor und Stürmerreihe - Hierbei wird der Ball in die Ecke positioniert.</li>
                    <li>Situation: Der Ball liegt zwischen den beiden Stürmerreihen -  Der Ball wird neu eingeworfen.</li>
                </ul>
            </div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="paragraph">§</div>
            <div class="paragraph-text">
                Die Ballwahl muss vor dem Spielbeginn vollzogen werden (Mehrheitswahl). Ein Auswechseln des Balls während des Spiels ist nicht zulässig.
            </div>
            <div class="clear"></div>
        </li>
        <li class="last">
            <div class="paragraph">§</div>
            <div class="paragraph-text">
                Der CheckIn/CheckOut Prozess muss strikt eingehalten werden, da dieser die Spielerauswahl im Matchmaking steuert. Abwesende Spieler müssen sich selbständig auschecken.
            </div>
            <div class="clear"></div>
        </li>
    </ul>
    {elseif $function == 'points'}
    <h4 class="headline"><span>Punkteregelung</span></h4>
    <div id="pointScheme"></div>
    <script type="text/javascript">
        $(function() {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'pointScheme',
                    defaultSeriesType: 'line',
                    backgroundColor: '#fff',
                    height: 500
                },
                title: {
                    text: null
                },
                xAxis: {
                    gridLineColor: '#ccc',
                    lineColor: '#ccc',
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: 'Punktevergabe',
                        style: {
                            'color': '#3C3E45',
                            'font-weight': 'bold'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    gridLineColor: '#ccc',
                    lineColor: '#3C3E45'
                },
                tooltip: {
                    formatter: function() {
                        return '<strong>' + this.y + ' Punkte</strong>';
                    },
                    borderColor: '#ccc'
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                series: [{
                        name: 'Punkte-Schema',
                        data: [60, 24, 22, 21, 19, 15, 14, 13, 12, 11, -1, -2, -3, -4, -5, -9, -11, -12, -14, -40],
                        color: '#3C3E45'
                    }]
            });
        });
    </script>

    {else}

    <h4 class="headline"><span>Über PackGyver</span></h4>
    <p class="textcenter">
        <img src="/static/img/logo.png" alt="PackGyver" /><br />
        <strong>Aktuelle Version:</strong> v0.6
    </p>
    {/if}
</div>