<div id="content" class="grid9 content-shadow">
	<h4 class="headline"><span>Archiv</span></h4>
	{if $storedSeasons|count > 0}
	<ul>
		{foreach $storedSeasons sYear seasonYear}
		<li class="marginr20 marginb20 fl">
			<div class="bold">Saison {$sYear}</div>
			{foreach $seasonYear season}
			<div class="padding5">- Saison {$season.monthName}/{$season.year}</div>
			{/foreach}
		</li>
		{/foreach}
	</ul>
	{else}
		Es wurden noch keine Saisons archiviert.
	{/if}
	<div class="clear margint20"></div>
	<h4 class="headline"><span>Saison archivieren</span></h4>
	<div class="margint20">
		<form id="store-season" action="" method="post">
            <div class="marginb5">
                <label for="month">Monat ab</label>
                <input type="number" id="month" name="fromMonth" min="1" max="12" required />
            </div>
            <div class="marginb5">
                <label for="year">Jahr ab</label>
                <input type="number" id="year" name="fromYear" value="" min="2011" max="2050" required />
            </div>
            <div class="marginb5">
                <label for="month">Monat bis</label>
                <input type="number" id="month" name="toMonth" min="1" max="12" />
            </div>
            <div class="marginb5">
                <label for="year">Jahr bis</label>
                <input type="number" id="year" name="toYear" value="" min="2011" max="2050" />
            </div>
			<button type="submit" name="store"><i class="icon-trophy"></i> Saison archivieren</button>
		</form>
	</div>
</div>