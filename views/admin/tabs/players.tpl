<div id="content" class="grid9 content-shadow">

{if $action == 'edit' || $action == 'create'}

	{if $action == 'edit'}
	<h4 class="headline"><span>Spieler bearbeiten</span></h4>
	{else}
	<h4 class="headline"><span>Spieler anlegen</span></h4>
	{/if}

	{if $isSent}
		{if $success}
			<div class="alert alert-success">
				<a class="close">×</a>
				<strong>Erfolg!</strong> Der Spieler wurde erfolgreich gespeichert.
			</div>
		{else}
			<div class="alert alert-error">
				<a class="close">×</a>
				<strong>Fehler!</strong> Spieler konnte nicht gespeichert werden.
			</div>
		{/if}
	{/if}

	<form action="{if $action == 'edit'}/admin/players/edit/{$player.id}/save{else}/admin/players/create/0/save{/if}" method="post" autocomplete="off">
		<div class="marginb5">
			<label for="firstname">Vorname</label>
			<input type="text" id="firstname" name="firstname" value="{$player.firstname}" />
		</div>
		<div class="marginb5">
			<label for="lastname">Nachname</label>
			<input type="text" id="lastname" name="lastname" value="{$player.lastname}" />
		</div>
		<div class="marginb5">
			<label for="email">Email</label>
			<input type="text" id="email" name="email" value="{$player.email}" />
		</div>
		<div class="marginb5">
			<label for="password">Passwort</label>
			<input type="password" id="password" name="password" placeholder="**********" />
		</div>
		<div class="marginb5">
			<label for="gtalk">Google Talk</label>
			<input type="text" id="gtalk" name="gtalk" value="{$player.gtalk}" />
		</div>
		<div class="marginb5">
			<label for="ckActive">Aktiv</label>
			<input type="checkbox" id="ckActive" name="active"{if $player.active} checked="checked"{/if} />
		</div>
		<div class="marginb5">
			<label for="ckCheckedIn">CheckedIn</label>
			<input type="checkbox" id="ckCheckedIn" name="checkedIn"{if $player.checkedIn} checked="checked"{/if} />
		</div>
		<div class="marginb5">
			<label for="ckDeleted">Gelöscht</label>
			<input type="checkbox" id="ckDeleted" name="deleted"{if $player.deleted} checked="checked"{/if} />
		</div>
		<div class="marginb5">
			<label for="type">Typ</label>
			<input type="radio" id="typeUser" name="type" value="0"{if $player.type == 0 || !$player.type} checked="checked"{/if} /> <label for="typeUser" class="clean">User</label>
			<input type="radio" id="typeAdmin" name="type" value="1"{if $player.type == 1} checked="checked"{/if} /> <label for="typeAdmin" class="clean">Admin</label>
		</div>
		<div class="margint15">
			<input type="submit" name="save" value="Speichern" />
		</div>
	</form>

{else}

	<h4 class="headline">
		<span>Aktive Spieler</span>
		<span class="fr btn-group">
		<button name="checkout" class="btn-request" data-href="/admin/ajax/changePlayerStatus/0"><i class="icon-eye-close"></i>Alle auschecken</button>
			<button name="checkin" class="btn-request" data-href="/admin/ajax/changePlayerStatus/1"><i class="icon-eye-open"></i>Alle einchecken</button>
			<button class="btn-redirect" data-href="/admin/players/create"><i class="icon-plus"></i>Spieler anlegen</button>
		</span>
	</h4>

	<table class="ranking marginb25">
		<thead>
			<tr>
				<th>Name</th>
				<th>Letzte Aktivität</th>
				<th>Checked</th>
				<th>Aktiv</th>
				<th>Aktion</th>
			</tr>
		</thead>
		<tbody>
		{foreach $players player}
			<tr>
				<td>
					<img src="{$player.avatar}" class="avatar small fl" />
					<span class="ranking-player-name"><a href="/player/{$player.uid}">{$player.firstname} {$player.lastname}</a></span>
				</td>
				<td class="textcenter">{$player.lastActivity}</td>
				<td class="textcenter">
					<input type="checkbox" class="player-check" playerId="{$player.id}" name="activePlayer_{$player.id}"{if $player.checkedIn} checked="checked"{/if} />
				</td>
				<td class="textcenter">
					<input type="checkbox" class="player-active" playerId="{$player.id}" name="activePlayer_{$player.id}"{if $player.active} checked="checked"{/if} />
				</td>
				<td class="textcenter">
					<button class="btn-redirect" data-href="/admin/players/edit/{$player.id}"><i class="icon-edit"></i> Editieren</button>
				</td>
			</tr>
		{/foreach}
		</tbody>
	</table>

	<h4 class="headline"><span>Gelöschte Spieler</span></h4>
	<table class="ranking">
		<thead>
		<tr>
			<th>Name</th>
			<th>Letzte Aktivität</th>
			<th>Aktion</th>
		</tr>
		</thead>
		<tbody>
		{foreach $playersDeleted dPlayer}
		<tr>
			<td>
			<img src="{$dPlayer.avatar}" class="avatar small fl" />
			<span class="ranking-player-name">{$dPlayer.firstname} {$dPlayer.lastname}</span>
			</td>
			<td class="textcenter">{$dPlayer.lastActivity}</td>
			<td class="textcenter">
			<button class="btn-redirect" data-href="/admin/players/edit/{$dPlayer.id}"><i class="icon-edit"></i> Editieren</button>
			</td>
		</tr>
		{/foreach}
		</tbody>
	</table>

{/if}

</div>