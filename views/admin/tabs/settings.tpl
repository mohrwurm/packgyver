<div id="content" class="grid9 content-shadow">

	<h4 class="headline"><span>Einstellungen</span></h4>
	<div style="width: 460px; float: left;">
		{foreach $settings settingGroup groupSettings name="settingsList"}
		<table{if !$dwoo.foreach.settingsList.last} class="marginb15"{/if}>
			<thead>
				<tr>
					<td colspan="2" class="bold"><h3>{$settingGroup}</h3></td>
				</tr>
			</thead>
			<tbody>
				{foreach $groupSettings setting}
				<tr>
					<td style="width: 180px; padding: 5px 0 0 10px;">{$setting.key}</td>
					<td id="{$setting.id}" class="jEditable">{$setting.value}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
		{/foreach}
	</div>

</div>