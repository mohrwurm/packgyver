<div id="content" class="grid9 content-shadow">

{if $function == 'create' || $function == 'edit'}

    {if $isEdit}
    <h4 class="headline"><span>Notification an {$notification.player.firstname} {$notification.player.lastname} bearbeiten</span></h4>
    {else}
    <h4 class="headline"><span>Notification erstellen</span></h4>
    {/if}

    {if $isSent}
	{if $success}
	    <div class="alert alert-success">
		<a class="close">×</a>
		<strong>Erfolg!</strong> Die Notification wurde erfolgreich gespeichert.
	    </div>
	{else}
	    <div class="alert alert-error">
		<a class="close">×</a>
		<strong>Fehler!</strong> Die Notification konnte nicht gespeichert werden.
	    </div>
	{/if}
    {/if}

    <form action="/admin/notifications/{if $isEdit}edit/{$notification.id}{else}create/{/if}/save" method="post">

	{if !$isEdit}
	<div class="marginb5">
	    <label for="title">Empfänger</label>
	    <select data-placeholder="Bitte auswählen" id="recipients" name="recipients[]" multiple="multiple" style="width: 234px;">
		{foreach $players player}
		    {if $isEdit}
			{if $player.id == $notification.player.id}
			    <option value="{$player.id}" selected="selected">{$player.firstname} {$player.lastname}</option>
			{else}
			    <option value="{$player.id}">{$player.firstname} {$player.lastname}</option>
			{/if}
		    {else}
			<option value="{$player.id}">{$player.firstname} {$player.lastname}</option>
		    {/if}
		{/foreach}
	    </select>
	</div>
	{/if}

	<div class="marginb5">
	    <label for="title">Titel</label>
	    <input type="text" id="title" name="title"{if $isEdit} value="{$notification.title}"{/if} />
	</div>
	<div class="marginb5">
	    <label for="message">Nachricht</label>
	    <textarea id="message" name="message">{if $isEdit}{$notification.message}{/if}</textarea>
	</div>
	<div class="margint15">
	    <label for="isHTML">HTML?</label>
	    <input type="checkbox" id="isHTML" name="isHTML" />
	</div>
	<div class="margint15">
	    <input type="submit" name="save" value="Speichern" />
	</div>
    </form>

{else}

    <h4 class="headline">
	<span>Notifications</span>
	<span class="fr btn-group">
	    <button id="createMatch" class="btn-redirect" data-href="/admin/notifications/create"><i class="icon-plus"></i>Notification anlegen</button>
	</span>
    </h4>

    {if $notifications|count > 0}
    <table class="ranking">
        <thead>
            <tr>
                <th class="textcenter">Spieler</th>
                <th class="textcenter">Title</th>
                <th class="textcenter">Nachricht</th>
                <th class="textcenter">Datum</th>
                <th class="textcenter">Aktionen</th>
            </tr>
        </thead>
        <tbody>
        {foreach $notifications notification}
        <tr class="{cycle values=array('','odd')}">
            <td class="textcenter">{$notification.player.firstname} {$notification.player.lastname}</td>
            <td class="textcenter">{$notification.title}</td>
            <td class="textcenter">
		<button class="show-content">Nachricht</button>
		<div class="hide">
		    <h1>Meldung</h1>
		    <div class="fb-notification-list-content">
			<div class="notification-entry">
			    <div>
				<div class="marginb5">
				    <span class="bold">
					<span class="negTrend dispose-notification" title="Meldung ausblenden">X</span>
					{$notification.title}
				    </span>
				    <span class="fr date-info">({$notification.dateSent})</span>
				</div>
				<div class="message">
				    {$notification.message}
				</div>
			    </div>
			</div>
			<div class="fb-controls textright">
			    <button class="fb-close-trigger"><i class="icon-remove"></i>Schließen</button>
			</div>
		    </div>
		</div>
	    </td>
            <td class="textcenter">{$notification.dateSent}</td>
            <td class="textcenter">
                <button class="btn-redirect" data-href="/admin/notifications/edit/{$notification.id}"><i class="icon-edit"></i> Editieren</button>
                <button class="delete-notification" data-href="/admin/notifications/delete/{$notification.id}"><i class="icon-remove"></i> Löschen</button>
            </td>
        </tr>
        {/foreach}
        </tbody>
    </table>
    {else}
        Keine Notifications verfügbar
    {/if}

{/if}

</div>