<div id="content" class="grid9 content-shadow">

{if $function == 'edit' || $function == 'add'}

	{if $function == 'edit'}
    <h4 class="headline"><span>Spiel bearbeiten</span></h4>
	{else}
    <h4 class="headline"><span>Spiel anlegen</span></h4>
	{/if}

    {if $isSent}
        {if $success}
            <div class="alert alert-success">
                <a class="close">×</a>
                <strong>Erfolg!</strong> Das Spiel wurde erfolgreich gespeichert.
            </div>
        {else}
            <div class="alert alert-error">
                <a class="close">×</a>
                <strong>Fehler!</strong> Das Spiel konnte nicht gespeichert werden.
            </div>
        {/if}
    {/if}

    <form action="/admin/matches/{if $function == 'edit'}edit/{$match.id}{else}add{/if}/save" method="post">
        <div class="fl textright" style="width: 280px;">

			{if $type == '1on1'}
            <span class="position teamRed">ALLE</span>
            <select name="teamRedGoal">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.left.single.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>

			{else}

            <span class="position teamRed">TOR</span>
            <select name="teamRedGoal">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.left.goal.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>
            <br />
            <span class="position teamRed">STURM</span>
            <select name="teamRedOffense">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.left.offense.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>
			{/if}
        </div>
        <div id="adm-match-edit-score" class="fl textcenter" style="width: 150px;">
            <input type="number" class="score" maxlength="2" min="0" max="10" name="scoreRed" value="{if null !== $match.score.left}{$match.score.left}{else}0{/if}" />
            <span class="bold">:</span>
            <input type="number" class="score" maxlength="2" min="0" max="10" name="scoreYellow" value="{if null !== $match.score.right}{$match.score.right}{else}0{/if}" />
        </div>
        <div class="fl" style="width: 280px;">

			{if $type == '1on1'}

			<select name="teamYellowGoal">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.right.single.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>
            <span class="position teamYellow">ALLE</span>

			{else}
            
            <select name="teamYellowOffense">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.right.offense.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>
            <span class="position teamYellow">STURM</span>
            <br />
            <select name="teamYellowGoal">
                {foreach $players player}
                <option value="{$player.id}"{if $match.players.right.goal.id == $player.id} selected="selected"{/if}>{$player.firstname} {$player.lastname}</option>
                {/foreach}
            </select>
            <span class="position teamYellow">TOR</span>

			{/if}
        </div>
        <div class="clear"></div>

		{if $edit}
        <div class="margint15 textcenter">
			<input type="checkbox"{if $match.deleted} checked="checked"{/if} id="markDeleted" name="markDeleted" /> <label for="markDeleted" class="clean">Als gelöscht markieren</label><br />
			<input type="checkbox" id="deletePermanent" name="deletePermanent" /> <label for="deletePermanent" class="clean">Endgültig löschen</label>
		</div>
		{/if}

        <div class="margint15 textcenter">
            <input type="hidden" id="isRandom" name="isRandom" value="1" />
            <input type="hidden" id="isSent" name="isSent" value="1" />
            <button type="submit" name="{if $function == 'edit'}editMatch{else}addMatch{/if}"><i class="icon-ok"></i> Speichern</button>
        </div>
    </form>
{else}

    <h4 class="headline">
        <span>Spiele</span>
        <span class="fr btn-group">
            <button id="createMatch" class="btn-redirect" data-href="/admin/matches/add"><i class="icon-plus"></i>Neues Spiel anlegen</button>
        </span>
    </h4>

    {if $matches|count > 0}
    <table id="adm-match-list" class="ranking">
        <thead>
            <tr>
                <th class="textcenter">ID</th>
                <th class="textcenter"><span class="position teamRed">Team ROT</span></th>
                <th class="textcenter">VS</th>
                <th class="textcenter"><span class="position teamYellow">Team GELB</span></th>
                <th class="textcenter">Aktion</th>
            </tr>
        </thead>
        <tbody>
        {foreach $matches match name="upcomingMatches"}
        <tr class="{cycle values=array('','odd')}">
            <td class="textcenter">{$match.id}{if $match.deleted}<br /><span class="negTrend">(Gelöscht)</span>{/if}</td>
            <td class="textcenter">
                <div class="team-container team-red">
                    {foreach $match.players.left posRed teamRedPlayer}
                        <a href="/player/{$teamRedPlayer.uid}" class="disInlBlock marginr20">
                            <span class="fr player-information">
                                {$teamRedPlayer.firstname}<br />{$teamRedPlayer.lastname}<br />
                                {if $posRed == 'goal'}
                                <span class="position teamRed">T</span>
                                {elseif $posRed == 'offense'}
                                <span class="position teamRed">S</span>
                                {else}
                                <span class="position teamRed">A</span>
                                {/if}
                            </span>
                        </a>
                    {/foreach}
                </div>
            </td>
            <td class="textcenter">
                <div class="versusContainer">
                    <div class="dateInfo">{$match.dateSaved}</div>
                    <div class="score">{$match.score.left} : {$match.score.right}</div>
                </div>
            </td>
            <td class="textcenter">
                <div class="team-container team-yellow">
                    {foreach $match.players.right posYellow teamYellowPlayer}
                        <a href="/player/{$teamYellowPlayer.uid}" class="disInlBlock marginl20">
                            <span class="fl player-information">
                                {$teamYellowPlayer.firstname}<br />{$teamYellowPlayer.lastname}<br />
                                {if $posYellow == 'goal'}
                                <span class="position teamYellow">T</span>
                                {elseif $posYellow == 'offense'}
                                <span class="position teamYellow">S</span>
                                {else}
                                <span class="position teamYellow">A</span>
                                {/if}
                            </span>
                        </a>
                    {/foreach}
                </div>
            </td>
            <td class="textcenter">
                <button class="btn-redirect i-standalone margin0" data-href="/admin/matches/edit/{$match.id}"><i class="icon-edit"></i></button>
                <button class="match-info i-standalone margin0" data-href="#match-info-{$match.id}"><i class="icon-info-sign"></i></button>
				<div id="match-info-{$match.id}" class="hide">
					<div class="margin20">
					<ul>
						<li class="paddingb5">
							<span class="bold">Erstellt von:</span> {$match.createdByPlayer.firstname} {$match.createdByPlayer.lastname}
						</li>
						<li class="paddingb20">
							<span class="bold">Erstellt am:</span> {$match.dateCreated}
						</li>
						<li class="paddingb5">
							<span class="bold">Gespeichert von:</span> {$match.savedByPlayer.firstname} {$match.savedByPlayer.lastname}
						</li>
						<li>
							<span class="bold">Gespeichert am:</span> {$match.dateSaved}
						</li>
					</ul>
					</div>
				</div>
            </td>
        </tr>
        {/foreach}
        </tbody>
    </table>
    {else}
        Keine Spiele verfügbar
    {/if}
{/if}

</div>