<div id="content" class="grid9">
    <div class="content-shadow">
        <h4 class="headline">
            <span>Logs (Letzte 3 Tage)</span>
        </h4>
        <ul style="line-height: 17px;">
            {foreach $logs logId log}
                {if $log.level}
                    <li{if $log.level == 'ERROR'} class="red"{/if}>
                        <strong>[{$log.date->format('d.m.Y H:i:s')}]</strong> {$log.level} - {truncate($log.message, 55, '...', true)}
                        <a href="" class="toggle bold" target=".logDetail_{$logId}" rel="logDetail_{$logId}">(Details)</a>
                        <div class="logDetail_{$logId} hide pg-color" style="word-wrap: break-word;">
                            <div class="padding20">
                                {$log.message}
                                <div class="margint15 bold">Context:</div>
                                {if $log.context}
                                    <ul>
                                        {foreach $log.context name value}
                                            <li>{$name}: {$value}</li>
                                        {/foreach}
                                    </ul>
                                {else}-{/if}
                            </div>
                        </div>
                    </li>
                {/if}
            {else}
                Keine Logs verf&uuml;gbar.
            {/foreach}
        </ul>
    </div>
</div>