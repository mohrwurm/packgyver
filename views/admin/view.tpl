{if $error}

<div class="content-first content-shadow">
    <h4 class="headline"><span>Fehler</span></h4>
    {$error}
</div>

{else}

<div id="sidebar" class="content-first grid3">
    <div class="content-shadow">
        <h4 class="headline"><span>Menü</span></h4>
        <ul class="box-navigation-simple">
            <li><a href="/admin"{if !$function} class="active"{/if}>Übersicht</a></li>
            <li><a href="/admin/players"{if $function == 'players'} class="active"{/if}>Spieler</a></li>
            <li><a href="/admin/matches"{if $function == 'matches'} class="active"{/if}>Spiele</a></li>
            <li><a href="/admin/notifications"{if $function == 'notifications'} class="active"{/if}>Notifications</a></li>
            <li><a href="/admin/awards"{if $function == 'awards' && !$section} class="active"{/if}>Awards</a></li>

            {if $function == 'awards'}
            <li class="sub">
                <ul>
                    <li><a href="/admin/awards/detect"{if $section == 'detect'} class="active"{/if}>Detector</a></li>
                </ul>
            </li>
            {/if}

            <li><a href="/admin/cache"{if $function == 'cache'} class="active"{/if}>Cache</a></li>
            <li><a href="/admin/settings"{if $function == 'settings'} class="active"{/if}>Einstellungen</a></li>
            <li><a href="/admin/archive"{if $function == 'archive'} class="active"{/if}>Archiv</a></li>
            <li><a href="/admin/logs"{if $function == 'logs'} class="active"{/if}>Logs</a></li>
        </ul>
    </div>
</div>

{$content}

{/if}