{if !$pgWSService.enabled}

push service is offline.

{elseif !$match}

no match available

{else}

<!DOCTYPE html>
<html>
    <head>
        <title>PackGyver - Enterprise Edition [BETA]</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

		{include '../inc/statics.tpl'}

		<script>
			var matchId = {$match.id};
			var locked = false;

			$(function() {
				setScore('left', {$match.score.left});
				setScore('right', {$match.score.right});

				checkMods();

				$('div.api-sb-score-v').click(function(e) {
					e.preventDefault();

					var score = $(this);
					var type = (score.attr('id').indexOf('-left') !== -1) ? 'left' : 'right';
					var oldValue = parseInt(score.text());
					var newValue = oldValue + 1;

					if (newValue <= 10 && newValue >= 0) {
						if (locked === false) {
							score.text(newValue);
							apiScoreCall((type === 'left') ? 'red' : 'yellow', '+');

							if (newValue > 0) {
								$('.api-sb-bottombar-mod.' + type).css('visibility', 'visible');
							}
							if (newValue === 10) {
								locked = true;
							}
						}
					}
				});
				$('.api-sb-bottombar-mod').click(function() {
					var type = ($(this).hasClass('left')) ? 'left' : 'right';
					var score = $('#api-sb-score-' + type);
					var oldValue = parseInt(score.text());
					var newValue = oldValue - 1;

					if (newValue <= 10 && newValue >= 0) {
						if (locked === false) {
							score.text(newValue);
							apiScoreCall((type === 'left') ? 'red' : 'yellow', '-');

							if (newValue === 0) {
								$('.api-sb-bottombar-mod.' + type).css('visibility', 'hidden');
							}
						}
					}
				});

				function getScore(team) {
					return parseInt($('div#api-sb-score-' + team).text());
				}

				function setScore(team, score) {
					if (locked === false) {
						$('div#api-sb-score-' + team).text(score);
					}
				}

				function checkMods() {
					var mods = ['left', 'right'];
					mods.forEach(function(team) {
						if (getScore(team) > 0) {
							$('.api-sb-bottombar-mod.' + team).css('visibility', 'visible');
						}
					});
				}

				function apiScoreCall(team, type) {
					var params = {
						matchId: matchId,
						type: type,
						team: team
					};
					$.post('/play/changeResult', params, function(data) {
						console.log(data);
						if (false === data.success) {
							alert('ERROR!');
						}
					}, 'json');
				}
			});
		</script>

		<style type="text/css">
			body {
				background: url("/static/img/mobile/api/led_bg_left.png") no-repeat fixed 0 42px, url("/static/img/mobile/api/led_bg_right.png") no-repeat fixed 100% 80px, url("/static/img/mobile/api/bg.png") repeat fixed 50% 50%,black
			}
			.unselectable {
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			#api-sb-bottombar {
				position: absolute;
				bottom: 0px;
				width: 100%;
			}
			#api-sb .sb-bar {
				height: 80px;
				background: #3f3f3f;
				background: -moz-linear-gradient(top, #3f3f3f 0%, #232323 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3f3f3f), color-stop(100%,#232323));
				background: -webkit-linear-gradient(top, #3f3f3f 0%,#232323 100%);
				background: -o-linear-gradient(top, #3f3f3f 0%,#232323 100%);
				background: -ms-linear-gradient(top, #3f3f3f 0%,#232323 100%);
				background: linear-gradient(top, #3f3f3f 0%,#232323 100%);
			}
			.api-sb-score {
				font-size: 250px;
				color: #eee;
				float: left;
			}
			#api-sb-vs {
				float: left;
				font-size: 400px;
				text-align: center;
				width: 10%;
			}
			.api-sb-score-v {
				cursor: pointer;
				text-align: center;
				font-size: 400px;
				width: 45%;
			}
			.api-sb-topbar-team {
				width: 45%;
				float: left;
				color: #eee;
				font-size: 60px;
				text-align: center;
				padding-top: 10px;
			}
			#api-sb-topbar-logo {
				width: 10%;
				float: left;
				text-align: center;
			}
			#api-sb-topbar-logo > img {
				padding-top: 4px;
			}
			.api-sb-bottombar-mod {
				font-size: 60px;
				color: #eee;
				visibility: hidden;
			}
			#api-sb-bottombar-left,
			#api-sb-bottombar-right {
				width: 35%;
				float: left;
				text-align: center;
			}
			#api-sb-bottombar-div {
				width: 30%;
				float: left;
				font-size: 35px;
				text-align: center;
				padding-top: 10px;
				color: #fff;
			}
			#api-sb .api-sb-avatars img {
				margin-top: 10px;
			}

			@media screen and (width: 1920px) {
				body {
					background: url("/static/img/mobile/api/led_bg_left.png") no-repeat fixed 0 37px, url("/static/img/mobile/api/led_bg_right.png") no-repeat fixed 100% 37px, url("http://app.digitalfoosball.com/images/backgrounds/bg_bodybase.png") repeat fixed 50% 50%,black;
				}
				#api-sb .sb-bar {
					height: 40px;
				}
				#api-sb .api-sb-topbar-team {
					font-size: 30px;
					padding-top: 5px;
				}
				#api-sb #api-sb-topbar-logo > img {
					width: 30px;
					height: 30px;
					padding-top: 5px;
				}
				#api-sb .api-sb-score-v {
					font-size: 200px;
				}
				#api-sb #api-sb-vs {
					font-size: 180px;
				}
				#api-sb #api-sb-bottombar-div {
					font-size: 20px;
				}
				#api-sb .api-sb-bottombar-mod {
					font-size: 30px;
				}
				#api-sb .api-sb-avatars img {
					width: 25px;
					height: 30px;
					margin-top: 3px;
				}
			}

			@media screen and (max-width: 800px) {
				body {
					background: url("/static/img/mobile/api/led_bg_left.png") no-repeat fixed 0 37px, url("/static/img/mobile/api/led_bg_right.png") no-repeat fixed 100% 37px, url("http://app.digitalfoosball.com/images/backgrounds/bg_bodybase.png") repeat fixed 50% 50%,black;
				}
				#api-sb .sb-bar {
					height: 40px;
				}
				#api-sb .api-sb-topbar-team {
					font-size: 30px;
					padding-top: 5px;
				}
				#api-sb #api-sb-topbar-logo > img {
					width: 30px;
					height: 30px;
					padding-top: 5px;
				}
				#api-sb .api-sb-score-v {
					font-size: 200px;
				}
				#api-sb #api-sb-vs {
					font-size: 180px;
				}
				#api-sb #api-sb-bottombar-div {
					font-size: 20px;
				}
				#api-sb .api-sb-bottombar-mod {
					font-size: 30px;
				}
				#api-sb .api-sb-avatars img {
					width: 25px;
					height: 30px;
					margin-top: 3px;
				}
			}
		</style>
    </head>
    <body class="unselectable">
		<div id="api-sb">
			<div id="api-sb-topbar" class="sb-bar">
				<div class="api-sb-topbar-team">ROT</div>
				<div id="api-sb-topbar-logo">
					<img src="/static/img/app/pg_launcher_icon_72x72.png" alt="PackGyver" />
				</div>
				<div class="api-sb-topbar-team">GELB</div>
			</div>
			<div style="clear: both;"></div>
			<div id="api-sb-scorecenter">
				<div id="api-sb-score-left" class="api-sb-score api-sb-score-v" onselect="return false;">0</div>
				<div id="api-sb-vs" class="api-sb-score">:</div>
				<div id="api-sb-score-right" class="api-sb-score api-sb-score-v" onselect="return false;">0</div>
				<div style="clear: both;"></div>
			</div>
			<div id="api-sb-bottombar" class="sb-bar">
				<div id="api-sb-bottombar-left" class="textcenter api-sb-avatars" data-score-id="api-sb-score-left">
					<img class="avatar sm" src="{$match.players.left.goal.avatar}" alt="" />
					<img class="avatar sm" src="{$match.players.left.offense.avatar}" alt="" />
				</div>
				<div id="api-sb-bottombar-div">
					<span class="api-sb-bottombar-mod left" data-score-id="api-sb-score-left">-1</span>
					<span class="api-sb-bottombar-mod right" data-score-id="api-sb-score-right">-1</span>
				</div>
				<div id="api-sb-bottombar-right" class="textcenter api-sb-avatars" data-score-id="api-sb-score-right">
					<img class="avatar sm" src="{$match.players.right.goal.avatar}" alt="" />
					<img class="avatar sm" src="{$match.players.right.offense.avatar}" alt="" />
				</div>
			</div>
		</div>
    </body>
</html>

{/if}