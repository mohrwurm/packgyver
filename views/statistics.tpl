<div class="special-headline">
    <span>Statistiken der aktuellen Saison</span>
</div>

<div class="content-first grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Spiele</span></h4>
    <span class="statistic-big">{$stats.season.matchCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Spiele &#216; Tag</span></h4>
    <span class="statistic-big">{$stats.season.matchesPerDay}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Monster Packs</span></h4>
    <span class="statistic-big">{$stats.season.monsterPackCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Packs</span></h4>
    <span class="statistic-big">{$stats.season.packCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Siege Rot</span></h4>
    <span class="statistic-big">{$stats.season.redTeamWins}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Siege Gelb</span></h4>
    <span class="statistic-big">{$stats.season.yellowTeamWins}</span>
</div>
<div class="grid4 content-first content-shadow">
    <h4 class="headline"><span>Monster Packs / Packs</span></h4>
    <div id="match-packs-season">
	{if !$stats.season.chartsEnabled}
	    Es wurden noch keine Spiele in dieser Saison gespielt
	{/if}
    </div>
</div>
<div class="grid4 content-shadow" style="height: 225px;">
    <h4 class="headline"><span>PackGyver</span></h4>
    <img src="/static/img/logo.png" alt="Logo" width="256" style="margin-top: 60px;" />
</div>
<div class="grid4 content-shadow">
    <h4 class="headline"><span>Siege Rot / Gelb</span></h4>
    <div id="team-victories-season">
	{if !$stats.season.chartsEnabled}
	    Es wurden noch keine Spiele in dieser Saison gespielt
	{/if}
    </div>
</div>

<div class="clear"></div>

<div class="special-headline" style="margin-top: 40px;">
    <span>Statistiken der gesamten Spielzeit</span>
</div>

<div class="content-first grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Spiele</span></h4>
    <span class="statistic-big">{$stats.alltime.matchCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Spiele &#216; Tag</span></h4>
    <span class="statistic-big">{$stats.alltime.matchesPerDay}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Monster Packs</span></h4>
    <span class="statistic-big">{$stats.alltime.monsterPackCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Packs</span></h4>
    <span class="statistic-big">{$stats.alltime.packCount}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Siege Rot</span></h4>
    <span class="statistic-big">{$stats.alltime.redTeamWins}</span>
</div>
<div class="grid2 content-shadow textcenter" style="padding: 20px 10px; width: 124px;">
    <h4 class="headline clean" style="font-size: 15px;"><span>Siege Gelb</span></h4>
    <span class="statistic-big">{$stats.alltime.yellowTeamWins}</span>
</div>
<div class="grid4 content-first content-shadow">
    <h4 class="headline"><span>Monster Packs / Packs</span></h4>
    <div id="match-packs-alltime">
	{if !$stats.alltime.chartsEnabled}
	    Es wurden noch keine Spiele in dieser Saison gespielt
	{/if}
    </div>
</div>
<div class="grid4 content-shadow" style="height: 225px;">
    <h4 class="headline"><span>PackGyver</span></h4>
    <img src="/static/img/logo.png" alt="Logo" width="256" style="margin-top: 60px;" />
</div>
<div class="grid4 content-shadow">
    <h4 class="headline"><span>Siege Rot / Gelb</span></h4>
    <div id="team-victories-alltime">
	{if !$stats.alltime.chartsEnabled}
	    Es wurden noch keine Spiele in dieser Saison gespielt
	{/if}
    </div>
</div>

{$matchCountSeason = math("$stats.season.matchCount-$stats.season.packCount-$stats.season.monsterPackCount")}
{$matchCountAlltime = math("$stats.alltime.matchCount-$stats.alltime.packCount-$stats.alltime.monsterPackCount")}
<script type="text/javascript">
    $(function() {

    {if $stats.season.chartsEnabled}

	// season
        new Highcharts.Chart({
            chart: {
                renderTo: 'team-victories-season',
                plotBorderWidth: null,
                backgroundColor: '#fff',
                height: 200,
                width: 280
            },
            title: {
                text: null
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        size: '60%',
                        connectorPadding: -10
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                    type: 'pie',
                    name: 'MatchStats',
                    data: [
                        {
                            name: 'Wins Red',    
                            y: {round($stats.season.redTeamWins/$stats.season.matchCount*100, 1)},
                            color: '#9b2426'
                        },
                        {
                            name: 'Wins Yellow',    
                            y: {round($stats.season.yellowTeamWins/$stats.season.matchCount*100, 1)},
                            color: '#d6ca04'
                        }
                    ]
                }]
        });
        new Highcharts.Chart({
            chart: {
                renderTo: 'match-packs-season',
                plotBorderWidth: null,
                backgroundColor: '#fff',
                height: 200,
                width: 280,
                margin: [0, 0, 0, 0]
            },
            colors: [
                '#3C3E45', '#CCC', '#999'
            ],
            title: {
                text: null
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                    type: 'pie',
                    name: 'MatchStats',
                    data: [
                        {
                            name: 'Matches',    
                            y: {round($matchCountSeason/$stats.season.matchCount*100, 1)}
                        },
                        {
                            name: 'Packs',    
                            y: {round($stats.season.packCount/$stats.season.matchCount*100, 1)}
                        },
                        {
                            name: 'Monster Packs',    
                            y: {round($stats.season.monsterPackCount/$stats.season.matchCount*100, 1)}
                        }
                    ]
                }]
        });

	{/if}

	{if $stats.alltime.chartsEnabled}

	// alltime
        new Highcharts.Chart({
            chart: {
                renderTo: 'team-victories-alltime',
                plotBorderWidth: null,
                backgroundColor: '#fff',
                height: 200,
                width: 280
            },
            title: {
                text: null
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        size: '60%',
                        connectorPadding: -10
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                    type: 'pie',
                    name: 'MatchStats',
                    data: [
                        {
                            name: 'Wins Red',    
                            y: {round($stats.alltime.redTeamWins/$stats.alltime.matchCount*100, 1)},
                            color: '#9b2426'
                        },
                        {
                            name: 'Wins Yellow',    
                            y: {round($stats.alltime.yellowTeamWins/$stats.alltime.matchCount*100, 1)},
                            color: '#d6ca04'
                        }
                    ]
                }]
        });
        new Highcharts.Chart({
            chart: {
                renderTo: 'match-packs-alltime',
                plotBorderWidth: null,
                backgroundColor: '#fff',
                height: 200,
                width: 280,
                margin: [0, 0, 0, 0]
            },
            colors: [
                '#3C3E45', '#CCC', '#999'
            ],
            title: {
                text: null
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                    type: 'pie',
                    name: 'MatchStats',
                    data: [
                        {
                            name: 'Matches',    
                            y: {round($matchCountAlltime/$stats.alltime.matchCount*100, 1)}
                        },
                        {
                            name: 'Packs',    
                            y: {round($stats.alltime.packCount/$stats.alltime.matchCount*100, 1)}
                        },
                        {
                            name: 'Monster Packs',    
                            y: {round($stats.alltime.monsterPackCount/$stats.alltime.matchCount*100, 1)}
                        }
                    ]
                }]
        });

	{/if}

    });
</script>