{$notifications = player('getNotificationsHash')}
<div>
    <h1>Deine Meldungen</h1>
    <div class="fb-notification-list-content">
        {if $notifications|count > 0}
            {foreach $notifications notification}
                {include 'notificationEntry.tpl'}
            {/foreach}
            <div class="margin20 hide none-available">Du hast derzeit keine weiteren Meldungen.</div>
        {else}
            <div class="margin20">Du hast derzeit keine Meldungen.</div>
        {/if}
    </div>
    <div class="fb-controls shadow textright">
        <button class="fb-close-trigger"><i class="icon-remove"></i>Schließen</button>
    </div>
</div>