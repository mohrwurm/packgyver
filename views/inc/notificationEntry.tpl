<div class="notification-entry notificationId-{$notification.id} {if $dwoo.foreach.default.last} last{/if}" notificationId="{$notification.id}">
	<div>
		<div class="marginb5">
			<span class="bold">
				{$notification.title}
			</span>
			<span class="fr date-info">({$notification.dateSent})</span>
		</div>
		<div class="message">
			{$notification.message}
		</div>
	</div>
</div>