<h4 class="headline"><span>Zuletzt verliehen</span></h4>

{$awardsGiven = widget('getAwardsGiven', array(5))}

{if $awardsGiven|count > 0}
<ul class="widget-awards-given">
    {foreach $awardsGiven hash}
    <li{if $dwoo.foreach.default.last} class="last"{/if}>
        <img src="{$hash.award.logo}" class="avatar small fl tipsy-tt" title="{$hash.award.name}" alt="award" />
        <span class="fl">
            <a href="/player/{$hash.player.uid}">
		{$hash.player.firstname} {$hash.player.lastname}
	    </a>
	    <div style="font-size: 10px;"><time class="timeago" title="{$hash.award.date}" datetime="{$hash.award.cdate}"></time></div>
        </span>
    </li>
    {/foreach}
</ul>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}