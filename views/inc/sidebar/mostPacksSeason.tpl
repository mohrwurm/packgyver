<h4 class="headline"><span class="tipsy-tt" title="Pack Ranking der aktuellen Saison">Packs (S)</span></h4>

{$mostSeasonPacks = widget('getSaisonMostPacks', array(3))}

{if $mostSeasonPacks|count > 0}
<table class="ranking sidebar-player-ranking">
    <thead>
        <tr>
            <th class="textcenter bold">#</th>
            <th class="bold">Name</th>
            <th class="bold last-h tipsy-tt" title="Packs">P.</th>
        </tr>
    </thead>
    <tbody>
        {foreach $mostSeasonPacks rank player}
            {$isLastV=''}
            {if $dwoo.foreach.default.last}
                {$isLastV=' last-v'}
            {/if}
            <tr class="{cycle values=array('','odd')}">
                <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
                <td class="{$isLastV}">
                    <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                    <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
                </td>
                <td class="last-h textcenter {$isLastV}">{$player.packCount}</td>
            </tr>
        {/foreach}
    </tbody>
</table>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}