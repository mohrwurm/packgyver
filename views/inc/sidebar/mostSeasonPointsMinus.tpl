{$isLastV=''}
<h4 class="headline"><span>Niedrigste Punktzahl in einer Saison</span></h4>

{$mostSeasonPointsPlus = widget('getMostSeasonPointsMinus', array(3))}
{if $mostSeasonPointsPlus|count > 0}
    <table class="ranking sidebar-player-ranking">
        <thead>
            <tr>
                <th class="textcenter bold">#</th>
                <th class="bold">Name</th>
                <th class="bold textcenter">Saison</th>
                <th class="bold textcenter">Spiele</th>
                <th class="bold textcenter last-h">Pkt.</th>
            </tr>
        </thead>
        <tbody>
            {foreach $mostSeasonPointsPlus rank player}
                {if $dwoo.foreach.default.last}
                    {$isLastV=' last-v'}
                {/if}
                <tr class="{cycle values=array('','odd')}">
                    <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
                    <td class="{$isLastV}">
                        <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                        <span class="ranking-player-name">
							<a href="/player/{$player.player.uid}">{$player.player.shortName}</a>
						</span>
                    </td>
                    <td class="textcenter{$isLastV}">
						<a href="/archive/season/{$player.season.year}/{$player.season.month}">
                            {$player.season.monthNameShort}{if $player.season.monthName|strlen > 3}.{/if}/{$player.season.year}
                        </a>
                    </td>
                    <td class="textcenter{$isLastV}">{$player.matchCount}</td>
                    <td class="textcenter last-h{$isLastV}">{$player.points}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else}
    <span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}