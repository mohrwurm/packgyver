<h4 class="headline"><span class="tipsy-tt" title="Alltime Match Ranking">Meiste Spiele (A)</span></h4>

{$alltimeMostMatches = widget('getAlltimeMostMatches', array(3))}

{if $alltimeMostMatches|count > 0}
<table class="ranking sidebar-player-ranking">
    <thead>
        <tr>
            <th class="textcenter bold">#</th>
            <th class="bold">Name</th>
            <th class="bold last-h">Sp.</th>
        </tr>
    </thead>
    <tbody>
        {foreach $alltimeMostMatches rank player}
            {if $dwoo.foreach.default.last}
                {$isLastV=' last-v'}
            {/if}
            <tr class="{cycle values=array('','odd')}">
                <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
                <td class="{$isLastV}">
                    <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                    <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
                </td>
                <td class="textcenter last-h{$isLastV}">{$player.countMatches}</td>
            </tr>
        {/foreach}
    </tbody>
</table>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}