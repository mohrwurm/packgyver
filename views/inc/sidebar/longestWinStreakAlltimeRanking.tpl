{$isLastV=''}
<h4 class="headline"><span class="tipsy-tt" title="Win Streaks Alltime">Win Streaks (A)</span></h4>

{$alltimeLongestWinStreak = widget('getAlltimeLongestWinStreak', array(3))}
{if $alltimeLongestWinStreak|count > 0}
    <table class="ranking sidebar-player-ranking">
        <thead>
            <tr>
                <th class="textcenter bold">#</th>
                <th class="bold">Name</th>
                <th class="bold textcenter last-h">St.</th>
            </tr>
        </thead>
        <tbody>
            {foreach $alltimeLongestWinStreak rank player}
                {if $dwoo.foreach.default.last}
                    {$isLastV=' last-v'}
                {/if}
                <tr class="{cycle values=array('','odd')}">
                    <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
                    <td class="{$isLastV}">
                        <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                        <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
                    </td>
                    <td class="textcenter last-h{$isLastV}"><span class="posTrend">+{$player.bestWinStreak}</span></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{else}
    <span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}