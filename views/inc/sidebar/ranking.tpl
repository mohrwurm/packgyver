<h4 class="headline"><span>Tabelle</span></h4>
<table class="ranking sidebar-player-ranking">
    <thead>
        <tr>
            <th class="textcenter bold">P</th>
            <th class="bold last-h">Name</th>
            {*<th class="textright bold" style="width: 40px;">Pkt.</th>*}
        </tr>
    </thead>
    <tbody>
        {foreach widget('getRankingData') player}
        {if $dwoo.foreach.default.last}
            {$isLastV=' last-v'}
        {/if}
        <tr class="{cycle values=array('','odd')}">
            <td class="textcenter ranking-rank{$isLastV}">{$dwoo.foreach.default.index + 1}.</td>
            <td class="last-h{$isLastV}">
                <img src="{$player.avatar}" alt="" class="avatar small fl" />
                <span class="ranking-player-name"><a href="/player/{$player.uid}">{$player.firstname} {$player.lastname}</a></span>
            </td>
            {*<td class="textcenter bold{$isLastV}">{$player.stats.season.points}</td>*}
        </tr>
        {/foreach}
    </tbody>
</table>