<h4 class="headline"><span class="tipsy-tt" title="Alltime MonsterPack Ranking">MonsterPacks verteilt (A)</span></h4>

{$alltimeMostMonsterPacks = widget('getAlltimeMostMonsterPacks', array(3))}

{if $alltimeMostMonsterPacks|count > 0}
<table class="ranking sidebar-player-ranking">
    <thead>
        <tr>
            <th class="textcenter">Pl.</th>
            <th class="bold">Name</th>
            <th class="textcenter bold last-h">MPs</th>
        </tr>
    </thead>
    <tbody>
        {foreach $alltimeMostMonsterPacks rank player name="mostMonsterPacks"}
            {if $dwoo.foreach.mostMonsterPacks.last}
                {$isLastV=' last-v'}
            {/if}
            <tr class="{cycle values=array('','odd')}">
                <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
                <td class="{$isLastV}">
                    <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                    <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
                </td>
                <td class="textcenter last-h{$isLastV}">{$player.monsterPackCount}</td>
            </tr>
        {/foreach}
    </tbody>
</table>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}