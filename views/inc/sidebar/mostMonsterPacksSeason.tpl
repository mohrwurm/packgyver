<h4 class="headline"><span class="tipsy-tt" title="MonsterPack Ranking der aktuellen Saison">MonsterPacks (S)</span></h4>

{$mostSeasonMonsterPacks = widget('getSaisonMostMonsterPacks', array(0))}

{if $mostSeasonMonsterPacks|count > 0}
<table class="ranking sidebar-player-ranking">
    <thead>
        <tr>
            <th class="textcenter bold">#</th>
            <th class="bold last-h">Name</th>
            <th class="textcenter last-h tipsy-tt" title="Monster Packs">M.</th>
        </tr>
    </thead>
    <tbody>
        {foreach $mostSeasonMonsterPacks rank player}
        {$isLastV=''}

        {if $dwoo.foreach.default.last}
            {$isLastV=' last-v'}
        {/if}
        <tr class="{cycle values=array('','odd')}">
            <td class="big textcenter ranking-rank{$isLastV}">{$rank+1}.</td>
            <td class="{$isLastV}">
                <img src="{$player.player.avatar}" alt="" class="avatar small fl" />
                <span class="ranking-player-name"><a href="/player/{$player.player.uid}">{$player.player.shortName}</a></span>
            </td>
            <td class="textcenter last-h{$isLastV}">{$player.monsterPackCount}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
{else}
<span class="no-data">Es stehen keine Daten zur Verfügung</span>
{/if}