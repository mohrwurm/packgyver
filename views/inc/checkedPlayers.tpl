{$players = player('getCheckedPlayers')}
<div>
    <h1>Eingecheckte Spieler</h1>
    <div class="fb-player-list-content">
        <div class="fb-player-list-content-item personal{if $pgUser.checkedIn == 0} hide{/if}">
            <img src="{$pgUser.avatar}" alt="" class="avatar medium fl player-avatar-chg" />
            <span class="fl">
                <a href="/player/{$pgUser.uid}">{$pgUser.firstname} {$pgUser.lastname}</a>
            </span>
        </div>
        {foreach $players player name="checkedPlayerList"}
		<div class="fb-player-list-content-item{if $dwoo.foreach.checkedPlayerList.last} last{/if}">
			<img src="{$player.avatar}" alt="" class="avatar medium fl" />
			<span class="fl">
			<a href="/player/{$player.uid}">{$player.firstname} {$player.lastname}</a>
			</span>
		</div>
        {/foreach}
        <div id="checked-players-none" class="margin10 {if $players|count > 0 || $pgUser.checkedIn == 1}hide{/if}">
            Keine Spieler eingecheckt
        </div>
    </div>
    <div class="fb-controls shadow textright">
        <button class="fb-close-trigger">Schließen</button>
    </div>
</div>

<div id="checked-players-tpl" class="hide">
    <div>
        <div class="fb-player-list-content-item">
            <img src="/static/upload/avatar/dummy.png" alt="" class="avatar medium fl">
            <span class="fl">
                <a href="#">Dummy</a>
            </span>
        </div>
    </div>
</div>