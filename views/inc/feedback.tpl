<div id="pg-feedback-open">
    <h1>Feedback senden</h1>
    <div id="pg-feedback">
        <p>Du hast eine Idee oder einen Bug gefunden? Dann wähle eine Kategorie aus und beschreibe dein Anliegen bitte genau.</p>
        <form action="" id="pg-feedback-form" method="post" autocomplete="off">
            <div class="marginb5">
                <ul class="pg-feedback-options">
                    <li class="selected">Vorschlag</li>
                    <li>Bug</li>
                    <li>Beschwerde</li>
                    <li>Anderes</li>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="margint20 margint10">
                <label for="message" class="bold marginb5">Nachricht</label>
                <textarea id="message" name="message" style="" required="required" placeholder="Bitte gib deine Nachricht hier ein"></textarea>
            </div>
            <input type="hidden" name="type" value="Vorschlag" />
        </form>
    </div>
    <div class="fb-controls textright">
        <button class="fb-close-trigger">Abbrechen</button>
        <button class="pg-feedback-send"><i class="icon-thumbs-up"></i> Senden</button>
    </div>
</div>