<div id="matchBox">
    <div style="width: 370px; margin: 17px 0 7px;" id="fb-matchup" data-match-type="2on2">
	<div class="team-red fl" style="width: 125px;">
	    <div class="bold textcenter marginb10">
		<div>
		    <span class="position teamRed disInlBlock">TOR</span>
		</div>
		<img class="avatar sm team-red-goal-ava" style="margin: 0 10px 6px 10px;" src="/static/upload/avatar/dummy.png" alt="avatar">
		<div class="team-red-goal"></div>
	    </div>
	    <div class="bold textcenter marginb10">
		<div>
		    <span class="position teamRed disInlBlock">STURM</span>
		</div>
		<img class="avatar sm team-red-offense-ava" style="margin: 0 10px 6px 10px;" src="/static/upload/avatar/dummy.png" alt="avatar">
		<div class="team-red-offense"></div>
	    </div>
	</div>
	<div class="fl textcenter" style="margin-top: 32px;width:120px;">
	    <div>
		<span class="bold">Erstellt von:</span><br />
		<span class="disInlBlock padding5">
		    <span class="match-created-by" style="white-space: nowrap;"></span>
		    <span style="font-size: 10px;" class="match-created-date disInlBlock padding5"></span>
		</span>
	    </div>
	    <div style="font-size: 68px; font-weight: bold; color: #ddd;">VS</div>
	    <div class="margint10">
		<button class="btn-redirect" data-href="/play">Zur Übersicht</button>
	    </div>
	</div>
	<div class="team-yellow fr" style="width: 125px;">
	    <div class="bold textcenter marginb10">
		<div>
		    <span class="position teamYellow disInlBlock">STURM</span>
		</div>
		<img class="avatar sm team-yellow-offense-ava" style="margin: 0 10px 6px 10px;" src="/static/upload/avatar/dummy.png" alt="avatar">
		<div class="team-yellow-offense"></div>
	    </div>
	    <div class="bold textcenter marginb10">
		<div>
		    <span class="position teamYellow disInlBlock">TOR</span>
		</div>
		<img class="avatar sm team-yellow-goal-ava" style="margin: 0 10px 6px 10px;" src="/static/upload/avatar/dummy.png" alt="avatar">
		<div class="team-yellow-goal"></div>
	    </div>
	</div>
    </div>
    <div class="clear"></div>
</div>