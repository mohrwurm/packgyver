<div id="wrapper" class="dumbVisitor">
    <div id="pg-special-panel" class="content-shadow">
        <div class="textcenter marginb20">
            <img src="/static/img/logo.png" alt="logo" />
        </div>
        <div class="marginb20">
            <strong>Kein gültiger Browser</strong>
        </div>
        Dein Browser "{$browser.name}" in der Version "{$browser.version}" wird von der PackGyver Desktop-Seite nicht unterstützt.
        <!-- {$browser.userAgent} -->
        <div class="margint25">
            <a href="http://www.mozilla.org/de/firefox/" class="tipsy-tt marginr10" title="Mozilla Firefox">
                <img src="/static/img/iesucker/firefox.png" alt="Firefox" width="80" />
            </a>
            <a href="http://www.google.com/chrome" class="tipsy-tt marginr10" title="Google Chrome">
                <img src="/static/img/iesucker/chrome.png" alt="Chrome" width="80" />
            </a>
            <a href="http://www.opera.com/" class="tipsy-tt marginr10" title="Opera">
                <img src="/static/img/iesucker/opera.png" alt="Opera" width="80" />
            </a>
            <a href="http://www.apple.com/de/safari/" class="tipsy-tt marginr10" title="Apple Safari">
                <img src="/static/img/iesucker/safari.png" alt="Safari" width="80" />
            </a>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('html, body').addClass('pg-special');
        centerPanel();
    });

    $(window).resize(function() {
        centerPanel();
    });
</script>