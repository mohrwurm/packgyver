<div id="match-overview">
    <div class="content-first content-shadow paddingb0 posRel">

	<div class="hide" style="position: absolute; left: -100px;">
	    <a id="webkitNotificationDemo" href="">NOTIFY</a>
	</div>

        <h4 class="headline">
            <span>Aktuelles Spiel</span>
            <span class="fr btn-group">
                <button id="mm-add-match" class="btn-redirect" data-href="/play/create"><i class="icon-plus"></i>Neues Spiel anlegen</button>
            </span>
        </h4>

        {if $currentMatch}
            {$cMatch = $currentMatch.0}
            <div id="currentMatch" class="margint20 type-{$cMatch.type}">
                <div class="team-container team-red">
                {foreach $cMatch.players.left posRed teamRedPlayer}
                    <div class="playerContainer clear margint5 marginb10">
                        <a href="/player/{$teamRedPlayer.uid}">
                            <img class="fl avatar big" src="{$teamRedPlayer.avatar}" alt="avatar" />
                        </a>
                        <div class="playerCredentials">
                            <a href="/player/{$teamRedPlayer.uid}">
                                {$teamRedPlayer.firstname}<br />
                                {$teamRedPlayer.lastname}
                            </a>
                            <div>
                                {if $posRed == 'goal'}
                                <img src="/static/img/matchmaking/position_goal.png" width="75" height="50" />
                                <!--<span class="position teamRed">TOR</span>-->
                                {elseif $posRed == 'offense'}
                                <img src="/static/img/matchmaking/position_offense.png" width="60" height="50" />
                                <!--<span class="position teamRed">STURM</span>-->
                                {else}
                                <span class="position teamRed">ALLE</span>
                                {/if}
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                {/foreach}
                </div>

                <div class="detailTable">
                    <div id="scoreboard" class="margin15">
                        <div class="textcenter m-info">{$cMatch.dateCreated}</div>
                        <form method="post" action="" autocomplete="off">
                            <div id="range-red" team="red" class="score-range"></div>
                            <div class="score-red fl scorecard"></div>
                            <div class="fl" style="width: 16px; margin: 40px 15px;">
                                <div style="width: 16px; line-height: 0px;">
                                    <span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span>
                                </div>
                                <div style="width: 16px; line-height: 0px; margin-top: 40px;">
                                    <span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span><span class="sc-d" style="opacity: 1;"></span>
                                </div>
                            </div>
                            <div class="score-yellow fl scorecard"></div>
                            <div id="range-yellow" class="score-range" team="yellow"></div>
                            <input type="hidden" name="teamScoreYellow" class="teamScoreYellow" value="{$cMatch.score.right}" />
                            <input type="hidden" name="teamScoreRed" class="teamScoreRed" value="{$cMatch.score.left}" />
                            <input type="hidden" name="matchId" class="matchId" value="{$cMatch.id}" />
                        </form>
                    </div>
                    <div id="current-match-ops" class="textcenter">
                        <div class="cm-ops-control-back">
                            <button class="save-cm"><i class="icon-ok"></i>Speichern</button>
                            <button class="cancel-cm"><i class="icon-remove"></i>Abbrechen</button>
                        </div>
                        {if $cMatch.hasPlayerParticipated}
                        <div class="cm-ops-control-front">
                            <button class="enter-score-cm marginr0" matchId="{$cMatch.id}"><i class="icon-pencil"></i>Ergebnis eintragen</button>
                            <button class="match-info" data-href="#matchInfo-{$cMatch.id}" style="margin-left: -6px;min-width: 35px;"><i class="icon-info-sign padding0"></i></button>
                            <button class="delete-cm" matchId="{$cMatch.id}" style="margin-left: -12px;"><i class="icon-remove padding0"></i></button>
                        </div>
                        {else}
			<button class="match-info" data-href="#matchInfo-{$cMatch.id}"><i class="icon-info-sign"></i>Informationen</button>
                        {/if}
			<div id="matchInfo-{$cMatch.id}" class="hide">
			    <div class="margin20">
				<ul>
				    <li class="paddingb5">
					<span class="bold">Erstellt von:</span> {$cMatch.createdByPlayer.firstname} {$cMatch.createdByPlayer.lastname}
				    </li>
				    <li>
					<span class="bold">Erstellt am:</span> {$cMatch.dateCreated}
				    </li>
				</ul>
			    </div>
			</div>
                    </div>
                </div>
                <div class="team-container team-yellow">
                {foreach $cMatch.players.right posYellow teamYellowPlayer}
                    <div class="playerContainer clear margint5 marginb10">
                        <a href="/player/{$teamYellowPlayer.uid}">
                            <img class="fr avatar big" src="{$teamYellowPlayer.avatar}" alt="avatar" />
                        </a>
                        <div class="playerCredentials">
                            <a href="/player/{$teamYellowPlayer.uid}">
                                {$teamYellowPlayer.firstname}<br />
                                {$teamYellowPlayer.lastname}
                            </a>
                            <div>
                                {if $posYellow == 'goal'}
                                <img src="/static/img/matchmaking/position_goal.png" width="75" height="50" />
                                <!--<span class="position teamYellow">TOR</span>-->
                                {elseif $posYellow == 'offense'}
                                <img src="/static/img/matchmaking/position_offense.png" width="60" height="50" />
                                <!--<span class="position teamYellow">STURM</span>-->
                                {else}
                                <span class="position teamYellow">ALLE</span>
                                {/if}
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                {/foreach}
                </div>
            </div>
            <div class="clear"></div>
        {else}
            <div class="alert alert-block textcenter">
                <strong>{$matchQueueLimit} Plätze frei!</strong> Derzeit werden keine Spiele bestritten. Du kannst eins generieren und sofort spielen!
            </div>
        {/if}
        <div class="clear"></div>
    </div>

    {if $currentMatch}
    <div class="content-first content-shadow">
        <h4 class="headline">
            <span>Spiele in der Warteschlange</span>
            <span class="fr headline-info">
                {if ($matchQueueLimit-$matchCount) > 1}
                    Es können noch {$matchQueueLimit-$matchCount} Spiele angelegt werden.
                {elseif ($matchQueueLimit-$matchCount) == 1}
                    Es kann noch 1 Spiel angelegt werden.
                {else}
                    <span class="limit-reached">Die maximale Anzahl an Spielen ({$matchQueueLimit}) ist erreicht</span>
                {/if}
            </span>
        </h4>
        {if $matches}
            {foreach $matches match name="upcomingMatches"}
            <div class="posRel upcomingMatchItem{if $dwoo.foreach.upcomingMatches.last} last{/if}">
                <div class="team-container team-red">
                    {foreach $match.players.left posRed teamRedPlayer}
                        <a href="/player/{$teamRedPlayer.uid}" class="bold disInlBlock marginr20">
                            <img src="{$teamRedPlayer.avatar}" class="avatar sm fr" />
                            <span class="fr player-information">
                                {$teamRedPlayer.firstname}<br />{$teamRedPlayer.lastname}<br />
                                {if $posRed == 'goal'}
                                <span class="position teamRed">Tor</span>
                                {elseif $posRed == 'offense'}
                                <span class="position teamRed">Sturm</span>
                                {else}
                                <span class="position teamRed">Alle</span>
                                {/if}
                            </span>
                        </a>
                    {/foreach}
                </div>
                <div class="versusContainer">
                    <span class="dateInfo">{$match.dateCreated}</span>
                    {if $match.hasPlayerParticipated}
                    <div class="margint5">
                        <button class="enter-score marginr0" matchId="{$match.id}"><i class="icon-pencil"></i>Eintragen</button>
			<button class="match-info" data-href="#matchInfo-{$match.id}" style="margin-left: -6px;min-width: 35px;"><i class="icon-info-sign padding0"></i></button>
                        <button class="delete-cm" matchId="{$match.id}" style="margin-left: -12px;"><i class="icon-remove padding0"></i></button>
                    </div>
                    {else}
		    <div class="score match-info cursorPointer" data-href="#matchInfo-{$match.id}">vs</div>
		    {*<button class="match-info" data-href="#matchInfo-{$match.id}"><i class="icon-info-sign"></i>Informationen</button>*}
                    {/if}

		    <div id="matchInfo-{$match.id}" class="hide">
			<div class="margin20">
			    <ul>
				<li class="paddingb5">
				    <span class="bold">Erstellt von:</span> {$match.createdByPlayer.firstname} {$match.createdByPlayer.lastname}
				</li>
				<li>
				    <span class="bold">Erstellt am:</span> {$match.dateCreated}
				</li>
			    </ul>
			</div>
		    </div>
                </div>
                <div class="team-container team-yellow">
                    {foreach $match.players.right posYellow teamYellowPlayer}
                        <a href="/player/{$teamYellowPlayer.uid}" class="bold disInlBlock marginl20">
                            <img src="{$teamYellowPlayer.avatar}" class="avatar sm fl" />
                            <span class="fl player-information">
                                {$teamYellowPlayer.firstname}<br />{$teamYellowPlayer.lastname}<br />
                                {if $posYellow == 'goal'}
                                <span class="position teamYellow">Tor</span>
                                {elseif $posYellow == 'offense'}
                                <span class="position teamYellow">Sturm</span>
                                {else}
                                <span class="position teamYellow">Alle</span>
                                {/if}
                            </span>
                        </a>
                    {/foreach}
                </div>
            <div class="clear"></div>
            </div>
            {/foreach}
        {else}
            Derzeit sind keine weiteren Spiele in der Warteschlange!
        {/if}
    </div>
    {/if}

    <div class="hide">
        <div id="match-score">
            <h1>Ergebnis eintragen</h1>
            <div class="fb-player-list-content">
                <div class="margin15 textcenter posRel">
                    <div class="save-info marginb10 bold">Bitte gib das Ergebnis an.</div>

                    <div class="team-dot team-red"></div>
                    <div class="team-dot team-yellow"></div>

                    <input type="number" class="score-input teamScoreRed" min="0" max="10" required="required" />
                    <span class="score-delimiter">:</span>
                    <input type="number" class="score-input teamScoreYellow" min="0" max="10" required="required" />
                    <input type="hidden" name="matchId" class="matchId" />
                </div>
            </div>
            <div class="fb-controls textright">
                <button class="fb-close-trigger"><i class="icon-remove"></i>Schließen</button>
                <button class="mm-save-score ico"><i class="icon-arrow-right"></i>Speichern</button>
            </div>
        </div>
    </div>
</div>