<div id="content" class="content-first grid8 content-shadow">
    <h4 class="headline">
        <span>Tabelle</span>
        <span class="headline-info fr">Saison {$season.monthName}/{$season.year} ({$season.start} bis {$season.end})</span>
    </h4>
    <table id="ranking" class="ranking ranking-big">
        <thead>
            <tr>
                <th class="textcenter" style="width: 40px;">Platz</th>
                <th>Name</th>
                <th class="textcenter">Ratio</th>
                <th class="textcenter">Spiele</th>
                <th class="textcenter tipsy-tt" title="Gewonnene Spiele">Gew.</th>
                <th class="textcenter tipsy-tt" title="Verlorene Spiele">Verl.</th>
                {*<th class="textcenter">Tore</th>
                <th class="textcenter">Diff.</th>*}
                <th class="textcenter tipsy-tt" title="Win/Loss Streak">Streak</th>
                <th class="textcenter last-h">Punkte</th>
            </tr>
        </thead>
        <tbody>

	    {$inactivePlayers = 0}

            {foreach $players rank player}
                {if player('isStreakPositive', $player.stats.season.streaks.currentStreak)}
                    {$streak=' posTrend'}
                {elseif 0 != $player.stats.season.streaks.currentStreak}
                    {$streak=' negTrend'}
                {else}
                    {$streak=''}
                {/if}

                {if $dwoo.foreach.default.last}
                    {$isLastV=' last-v'}
                {/if}

                {if $player.active == 0}
                    {$inactivePlayers = $inactivePlayers+1}
                {/if}

                <tr class="{cycle values=array('','odd')}{if !$player.active} player-inactive hide{/if}">
                    <td class="big textcenter ranking-rank{$isLastV}">{$rank + 1}.</td>
                    <td class="{$isLastV}">
                        <span class="pgStatus{if $player.checkedIn} checkedIn{else} checkedOut{/if}"></span>
                        <img src="{$player.avatar}" alt="" class="avatar small fl" />
                        <span class="ranking-player-name">
							<a href="/player/{$player.uid}"{if $player.active == 0} style="color: darkred;"{/if}>{$player.firstname} {$player.lastname}</a>
						</span>
                    </td>
                    <td class="textcenter{$isLastV}">{$player.stats.season.matchWinRatio}%</td>
                    <td class="textcenter{$isLastV}">{$player.stats.season.countMatches}</td>
                    <td class="textcenter{$isLastV}">{$player.stats.season.countWins}</td>
                    <td class="textcenter{$isLastV}">{$player.stats.season.countLosses}</td>
                    {*<td class="textcenter{$isLastV}">{$player.stats.season.playerGoals}</td>
                    <td class="textcenter{$isLastV}">{$player.stats.season.playerGoals-$player.stats.season.enemyGoals}</td>*}
                    <td class="textcenter{$isLastV}{$streak}">{$player.stats.season.streaks.currentStreak}</td>
                    <td class="textcenter bold last-h{$isLastV}">{$player.stats.season.points}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>

    {if $inactivePlayers > 0}
    <div class="margint15 textright">
        <button id="toggle-hidden-players" class="hide-players"><i class="icon-eye-open"></i> Inaktive Spieler <span class="val">anzeigen</span></button>
    </div>
    {/if}

</div>
<div id="sidebar" class="grid4">
    <div class="content-shadow">
        {include 'inc/sidebar/longestWinStreakSeasonRanking.tpl'}
    </div>
    <div class="content-shadow">
        {include 'inc/sidebar/longestLossStreakSeasonRanking.tpl'}
    </div>
    <div class="content-shadow">
        {include 'inc/sidebar/mostPacksSeason.tpl'}
    </div>
    <div class="content-shadow">
        {include 'inc/sidebar/mostMonsterPacksSeason.tpl'}
    </div>
</div>