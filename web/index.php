<?php

require_once __DIR__ . '/../app/init.inc.php';

$session = Session::instance();

Router::instance()->handle()->execute();

$session->writeClose();
