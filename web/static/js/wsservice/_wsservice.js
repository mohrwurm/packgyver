var WSService = {
	isMatchPage: function() {
		return 1 === $('div#matchmaking-site').length;
	},
	showMatchCreationMsg: function (match) {
		var btn = getButton('Schließen', 'fb-close-trigger'),
		html = $('div#matchBox').clone(),
		breakNames = function(firstname, lastname) {
			return firstname + '<br />' + lastname;
		},
		prepAva = function(src) {
			return src; // + '?s=' + $.packGyver.utils.generateRandomNumber(1000, 10000);
		}

		$('div.team-red-goal', html).html(breakNames(match.players.left.goal.firstname, match.players.left.goal.lastname));
		$('img.team-red-goal-ava', html).attr('src', prepAva(match.players.left.goal.avatar));

		$('div.team-red-offense', html).html(breakNames(match.players.left.offense.firstname, match.players.left.offense.lastname));
		$('img.team-red-offense-ava', html).attr('src', prepAva(match.players.left.offense.avatar));

		$('div.team-yellow-goal', html).html(breakNames(match.players.right.goal.firstname, match.players.right.goal.lastname));
		$('img.team-yellow-goal-ava', html).attr('src', prepAva(match.players.right.goal.avatar));

		$('div.team-yellow-offense', html).html(breakNames(match.players.right.offense.firstname, match.players.right.offense.lastname));
		$('img.team-yellow-offense-ava', html).attr('src', prepAva(match.players.right.offense.avatar));

		$('span.match-created-by', html).html(match.createdByPlayer.firstname + ' ' + match.createdByPlayer.lastname);
		$('span.match-created-date', html).text('(' + match.dateCreated + ')');

		$.packGyver.site.notify('Ein Spiel wurde generiert', html.html(), btn);
	},
	eventCheckedPlayersChange: function (data) {
		$('span#topnav-player-checked').text(data.count);
		$('div#checkedPlayers div.fb-player-list-content div.fb-player-list-content-item').remove();

		var isPlayerActive = false;

		if (data.count !== 0) {
			var tpl = $('div#checkedPlayers div#checked-players-tpl > div');
			var i = 0;

			$.each(data.players, function(id, player) {
				if (player.id === $.packGyver.player.id) {
					isPlayerActive = true;
				}

				var oTpl = tpl.clone();
				var link = oTpl.find('span.fl > a');
				var avatar = oTpl.find('img');

				link.attr('href', '/player/' + player.uid);
				link.text(player.firstname + ' ' + player.lastname);

				avatar.attr('src', player.avatar);
				i++;

				if (i === data.count) {
					$('div.fb-player-list-content-item', oTpl).addClass('last');
				}

				$('div#checkedPlayers div.fb-player-list-content').append(oTpl.html());
			});
		}

		if (isPlayerActive) {
			$('.player-check-c').removeClass('checkedOut').addClass('checkedIn').text($.packGyver.players.status.checkedIn);
		} else {
			$('.player-check-c').removeClass('checkedIn').addClass('checkedOut').text($.packGyver.players.status.checkedOut);
		}
	},
	eventMatchScoreChange: function (data) {
		var scoreboard = $('div#scoreboard');

		if (scoreboard.length === 1) {
			updateScoreSlider(data.team, data.score);
			if (data.score == 10) {
				setTimeout(function() {
					refreshMatchOverview();
				}, 3000);
			}
		}
	},
	eventMatchOverviewCreate: function (data) {
		if (data.triggeredBy !== $.packGyver.player.id) {
			if (WSService.isMatchPage()) {
				refreshMatchOverview();
			} else {
				if ($.inArray($.packGyver.player.id, data.match.playerIds) !== -1) {
					WSService.showMatchCreationMsg(data.match);
					return;
				}
			}
		}
	},
	eventMatchOverviewDelete: function (data) {
		if (data.triggeredBy !== $.packGyver.player.id && WSService.isMatchPage()) {
			refreshMatchOverview();
		}
	},
	eventMatchOverviewSave: function (data) {
		if (data.triggeredBy !== $.packGyver.player.id && WSService.isMatchPage()) {
			refreshMatchOverview();
		}
	},
	eventNotificationReceived: function (data) {
		$.packGyver.players.notifications.pullable = true;
		$.packGyver.players.notifications.updateNumb(data.count);
	}
};