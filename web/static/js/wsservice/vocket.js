var uri = 'ws://' + pgWSServiceOptions.host + ':' + pgWSServiceOptions.port,
    prvChannel = 'player-prv-' + $.packGyver.player.id;
console.log(uri);

ab.connect(uri,

    // WAMP session was established
    function (session) {

        console.log('Connected!');

        session.subscribe(prvChannel,
            function (topic, payload) {
                console.log(topic, payload.data);
            });

        session.subscribe('checked-players',
            function (topic, payload) {

                // change event
                WSService.eventCheckedPlayersChange(payload.data);
            });

        session.subscribe('notification/create',
            function (topic, payload) {

                WSService.eventNotificationReceived(payload.data);
            });

        session.subscribe('match-score',
            function (topic, payload) {

                WSService.eventMatchScoreChange(payload.data);
            });

        session.subscribe('match-overview',
            function (topic, payload) {

                switch (payload.event) {
                    case 'create':
                        WSService.eventMatchOverviewCreate(payload.data);
                        break;
                    case 'delete':
                        WSService.eventMatchOverviewDelete(payload.data);
                        break;
                    case 'save':
                        WSService.eventMatchOverviewSave(payload.data);
                        break;
                }
            });
    },

    // WAMP session is gone
    function (code, reason) {
        console.log('err-js: ' + reason);
        alert('WS Connection Error');
    }
);
