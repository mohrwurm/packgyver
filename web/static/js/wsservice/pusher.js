var pusher = new Pusher(pgWSServiceOptions.apiKey),
	prvChannel = 'player-prv-' + $.packGyver.player.id;

var personalChannel = pusher.subscribe(prvChannel);
personalChannel.bind('notification/create', function(data) {
	WSService.eventNotificationReceived(data);
});

var checkedPlayersChannel = pusher.subscribe('checked-players');
checkedPlayersChannel.bind('change', function(data) {
	WSService.eventCheckedPlayersChange(data);
});

var matchScore = pusher.subscribe('match-score');
matchScore.bind('change', function(data) {
	WSService.eventMatchScoreChange(data);
});

var matchOverview = pusher.subscribe('match-overview');
matchOverview.bind('create', function(data) {
	WSService.eventMatchOverviewCreate(data);
});
matchOverview.bind('delete', function(data) {
	WSService.eventMatchOverviewDelete(data);
});
matchOverview.bind('save', function(data) {
	WSService.eventMatchOverviewSave(data);
});