var prvChannel = 'player-prv-' + $.packGyver.player.id;

Beacon.connect(pgWSServiceOptions.apiKey, [prvChannel, 'checked-players', 'match-score', 'match-overview'], {
	log: pgWSServiceOptions.log
});

Beacon.listen(function (event) {
	switch (event.channel) {
		case 'checked-players':
			/* ATM only 'change' event */
			WSService.eventCheckedPlayersChange(event.data);
			break;
		case 'match-score':
			/* ATM only 'change' event */
			WSService.eventMatchScoreChange(event.data);
			break;
		case 'match-overview':
			switch (event.name) {
				case 'create':
					WSService.eventMatchOverviewCreate(event.data);
					break;
				case 'delete':
					WSService.eventMatchOverviewCreate(event.data);
					break;
				case 'save':
					WSService.eventMatchOverviewCreate(event.data);
					break;
			}
			break;
		case prvChannel:
			switch (event.name) {
				case 'notification/create':
					WSService.eventNotificationReceived(event.data);
					break;
			}
			break;
	}
});