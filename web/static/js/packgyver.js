(function($) {
	$.packGyver = {
		player: null,
		site: {
			browser: {
				version: (navigator.userAgent.toLowerCase().match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
				chrome: /chrome/.test( navigator.userAgent.toLowerCase() ),
				safari: /webkit/.test( navigator.userAgent.toLowerCase() ) && !/chrome/.test( navigator.userAgent.toLowerCase() ),
				opera: /opera/.test( navigator.userAgent.toLowerCase() ),
				msie: /msie/.test( navigator.userAgent.toLowerCase() ) && !/opera/.test( navigator.userAgent.toLowerCase() ),
				mozilla: /mozilla/.test( navigator.userAgent.toLowerCase() ) && !/(compatible|webkit)/.test( navigator.userAgent.toLowerCase() )
			},
			notify: function(title, content, controls, isModal) {
				isModal = (true !== isModal) ? false : true;
				var html = ['<div id="pg-fb-open">',
				'<h1 class="fb-title">' + title + '</h1>',
				'<div id="fb-content">' + content + '</div>',
				'<div class="fb-controls textright">' + controls + '</div>',
				'</div>'].join('');
				$.facebox(html, {
					modal: isModal
				});
			},
			url: null,
			pageLoader: function(show) {
				var loader = $('#page-loader');
				if (true === show) {
					loader.fadeIn(500);
				} else {
					loader.fadeOut(500);
				}
			}
		},
		players: {
			check: function(type, playerId) {
				$.packGyver.site.pageLoader(true);
				$.post('/players/status/check/' + type, {
					playerId: playerId
				}, function() {
					$.packGyver.site.pageLoader(false);
				});
			},
			status: {
				checkedIn: 'Eingecheckt',
				checkedOut: 'Ausgecheckt'
			},
			notifications: {
				pullable: false,
				hasNotifications: false,
				noneAvailable: '<div class="margin20">Du hast derzeit keine Meldungen.</div>',
				removeDOMNotifications: function () {
					$.packGyver.players.notifications.hasNotifications = false;
					$('div.notification-entry', $('div#player-notifications')).remove();
					$('div.fb-notification-list-content', $('div#player-notifications')).html($.packGyver.players.notifications.noneAvailable);
				},
				clear: function () {
					$.post('/notifications/clear');
				},
				checkDOMNotifications: function () {
					if ($('div.notification-entry', $('div#player-notifications')).length > 0) {
						$.packGyver.players.notifications.hasNotifications = true;
						$.packGyver.players.notifications.pulsateNumb();
					}
				},
				pull: function () {
					$.packGyver.site.pageLoader(true);

					$.post('/notifications/pull', {
						cleanUp: true
					}, function (data) {
						var content = '';

						$.packGyver.site.pageLoader(false);
						$.packGyver.players.notifications.pullable = false;

						if (data.newCount > 0) {
							$.packGyver.players.notifications.hasNotifications = true;
							content = data.notifications;
						} else {
							content = $.packGyver.players.notifications.noneAvailable;
							$.packGyver.players.notifications.reset();
						}

						$('div.fb-notification-list-content', $('div#player-notifications')).html(content);
						$.packGyver.players.notifications.openBox(false);
					});
				},
				openBox: function (clear) {
					$.facebox($('div#player-notifications').html());

					if ($.packGyver.players.notifications.hasNotifications) {
						if (true === clear) {
							$.packGyver.players.notifications.clear();
						}

						$.packGyver.players.notifications.reset();
					}
				},
				reset: function () {
					$.packGyver.players.notifications.removeDOMNotifications();
					$.packGyver.players.notifications.updateNumb(0);
					$.packGyver.players.notifications.pullable = false;
				},
				updateNumb: function (count) {
					if (count >= 0) {
						var numb = $('span#player-notifications-count'),
						oldValue = parseInt(numb.text());

						if (count === 0) {
							numb.removeClass('new', 1100);
							numb.effect('pulsate', {
								times: 1
							}, 700);
						} else if (count > oldValue) {
							numb.addClass('new', 300);
							numb.effect('pulsate', {
								times: 2
							}, 700);
						}

						numb.text(count);
					}
				},
				pulsateNumb: function () {
					$('span#player-notifications-count').effect('pulsate', {
						times: 2
					}, 700);
				}
			}
		},
		utils: {
			getBrowser: function() {
				var browserName = 'Unknown';
 
				if ($.packGyver.site.browser.chrome) {
					browserName = 'Google Chrome';
				} else if ($.packGyver.site.browser.safari) {
					browserName = 'Safari';
				} else if ($.packGyver.site.browser.opera) {
					browserName = 'Opera';
				} else if ($.packGyver.site.browser.mozilla) {
					browserName = 'Mozilla Firefox';
				} else if ($.packGyver.site.browser.msie) {
					browserName = 'Internet Explorer';
				}
    
				return browserName + ' (v' + $.packGyver.site.browser.version + ')';
			},
			generateRandomNumber: function(min, max) {
				return Math.floor(Math.random() * (max - min + 1)) + min;
			}
		}
	}
})(jQuery);