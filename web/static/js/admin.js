/* 
    Document   : admin.js
    Created on : 19.11.2011, 13:37:00
    Description:
        PackGyver Admin
 */

$(function() {

	/* delete award/playerAward */
	$('button.deleteAward, button.deletePlayerAward').click(function() {
		var button = $(this);
		var mode = null;
		var awardId = parseInt(button.attr('awardId'));

		if (button.hasClass('deleteAward')) {
			mode = 'award';
		} else if (button.hasClass('deletePlayerAward')) {
			mode = 'playerAward';
		}

		if (!isNaN(awardId) && null !== mode) {
			if (confirm('Soll dieser Award wirklich gelöscht werden?')) {
				$.post('/admin/awards/delete/' + mode + '/' + awardId, null, function(resp) {
					if (resp.success === true) {
						alert('success');
						window.location = '/awards';
					} else {
						var closeBtn = getButton('Schließen', 'fb-close-trigger fb-delete-award fb-award-' + mode);
						$.packGyver.site.notify('Award löschen', '<div class="margin20">' + resp.error + '</div>', closeBtn);
					}
				}, 'json');
			}
		}
	});

	/* admin delete match */
	$('button#delete-match').click(function(e) {
		if (!confirm('Soll das Spiel wirklich gelöscht werden?')) {
			e.preventDefault();
		}
	});

	/* admin delete notification */
	$('button.delete-notification').click(function(e) {
		if (confirm('Soll die Notification wirklich gelöscht werden?')) {
			window.location = $(this).attr('data-href');
		}
	});

	/* admin clear cache */
	$('.clearCacheBtn').click(function() {
		var type = $(this).attr('name');

		if (typeof type !== 'undefined') {
			$.packGyver.site.pageLoader(true);
			$.post('/admin/ajax/clearCache/' + type, null, function() {
				$.packGyver.site.pageLoader(false);
			});
		}
	});

	/* set player active/inactive */
	$('.player-active').change(function() {
		var playerId = (undefined !== $(this).attr('playerId')) ? $(this).attr('playerId') : null;
		var type = $(this).is(':checked') ? 'in' : 'out';
		$.packGyver.site.pageLoader(true);
		$.post('/players/status/active/' + type, {
			playerId: playerId
		}, function() {
			$.packGyver.site.pageLoader(false);
		});
	});

	/* store season */
	$('form#store-season').submit(function(e) {
		e.preventDefault();
		$.packGyver.site.pageLoader(true);

		$.post('/admin/archive/store', $(this).serialize(), function(res) {
			var btn = getButton('Schließen', 'fb-close-trigger mm-trigger-refresh');
			$.packGyver.site.pageLoader(false);
			$.packGyver.site.notify('Saison archivieren', '<div class="margin20">' + res.message + '</div>', btn);
		}, 'json');
	});

	$('button.show-content').click(function() {
		$.facebox($(this).nextAll('div.hide').html());
	});

	/* edit settings */
	$('.jEditable').editable('/admin/ajax/changeSetting', {
		type      : 'textarea',
		cancel    : 'Abbrechen',
		submit    : 'Speichern'
	});

	/* HOTKEYS */

	$(document).bind('keydown', 'ctrl+a', function (e){
		window.location = '/admin';
		return false;
	});

	/* /HOTKEYS */

});