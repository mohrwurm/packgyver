(function() {
    $.fn.timeago.defaults.lang = {
	units: {
	    second: "Sekunde",
	    seconds: "Sekunden",
	    minute: "Minute",
	    minutes: "Minuten",
	    hour: "Stunde",
	    hours: "Stunden",
	    day: "Tag",
	    days: "Tagen",
	    month: "Monat",
	    months: "Monaten",
	    year: "Jahr",
	    years: "Jahre"
	},
	prefixes: {
	    lt: "weniger als",
	    about: "ca.",
	    over: "über",
	    almost: "fast",
	    ago: "vor "
	},
	suffix: "" // ago
    };

}).call(this);