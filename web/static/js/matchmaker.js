(function($) {
	$.matchMaker = {
		type: null,
		typeName: null,
		player: null,
		participate: false,
		fairShuffle: false,
		poolPlayers: [],
		matchPlayers: [],
		excludePlayers: [],
		notifications: {
			currentMatch: 0,
			nextMatch: 1
		},

		_error: function(err) {
			var errMsg = null;

			switch (err) {
				case 'notCheckedIn':
					errMsg = 'Du willst mitspielen, bist aber selbst nicht eingecheckt.';
					break;
				case 'notEnoughPlayers':
					errMsg = 'Es wurden nicht genügend Spieler für den ' + this.typeName + ' Spieltyp ausgewählt.';
					break;
				case 'invalidType':
					errMsg = 'Kein gültiger Spieltyp ausgewählt';
					break;
				default:
					if (!err) {
						err = 'UnknownError';
					}
					errMsg = err + ' - Das hast du ja super hinbekommen...';
			}

			throw errMsg;
		},

		appendPoolPlayer: function(index, player) {
			this.poolPlayers[index] = player;
		},

		getPlayer: function(playerId) {
			return this.poolPlayers[playerId];
		},

		_existsPlayer: function(playerId) {
			return typeof this.poolPlayers[playerId] !== undefined;
		},

		_registerPlayerForMatch: function(playerId) {
			this.matchPlayers.push(this.getPlayer(playerId));
		},

		_excludePlayerForMatch: function(playerId) {
			this.excludePlayers.push(this.getPlayer(playerId));
		},

		changePlayerPoolStati: function(stat) {
			$('#mm-choose-player ul li').each(function(i, element) {
				var elem = $(element);

				if (stat === 'out') {
					if (!elem.hasClass('deactive')) {
						elem.addClass('deactive');
						$('div.mm-player div.player-mm-status > button').addClass('deactive');
					}
				} else {
					if (elem.hasClass('deactive')) {
						elem.removeClass('deactive');
						$('div.mm-player div.player-mm-status > button').removeClass('deactive');
					}
				}
			});
		},

		updatePlayerCount: function() {
			var cip = $('div#alternative-pp ul li:not(.deactive)').length;
			$('div#mm-chosen-player-msg span.val').text(cip);
		},

		_fetchPlayersFromPool: function() {
			var mm = this;

			$('div#mm-choose-player ul li').each(function() {
				var playerId = parseInt($('div.mm-player', $(this)).attr('playerId'));

				if (!isNaN(playerId) && mm._existsPlayer(playerId)) {
					if (!$(this).hasClass('deactive')) {
						mm._registerPlayerForMatch(playerId);
					} else {
						mm._excludePlayerForMatch(playerId);
					}
				}
			});
		},

		_fetchType: function() {
			this.type = parseInt($('div#matchmaking div#pregame input[name=matchType]:checked').val());
		},

		_isValidType: function() {
			return (this.type === 2 || this.type === 4);
		},

		_fetch: function() {
			this._fetchType();
			this.typeName = (this.type === 2) ? '1on1' : '2on2';
			this._fetchPlayersFromPool();
			this.participate = $('input#mm-player-participate').is(':checked');
			this.fairShuffle = $('input#mm-fair-shuffle').is(':checked');
		},

		_getTeamSpot: function(team, pos) {
			return $('div#team-' + team + '-' + pos, $('#mm-setup-rdy'));
		},

		_fillTeamSpot: function(team, pos, player) {
			var spot = this._getTeamSpot(team, pos);
			var pSpot = $('div.team-spot-player', spot);
			pSpot.attr('playerId', player.id);
			pSpot.find('div.tsp-main img.avatar').attr('src', player.avatar + '?s');
			pSpot.find('div.tsp-main div.tsp-player-name span.firstname').text(player.firstname);
			pSpot.find('div.tsp-main div.tsp-player-name span.lastname').text(player.lastname);
			pSpot.find('div.tsp-addon div.tsp-addon-points span.val').text(player.stats.season.points);
			pSpot.find('div.tsp-addon div.tsp-addon-streak').text(player.stats.season.streaks.currentStreak);
			pSpot.find('div.tsp-addon-position').text(spot.attr('position' + this.typeName));

			if (this.type === 2) {
				spot.find('div.panel div.front > img').attr('src', '/static/img/matchmaking/position_all.png');
			}
		},

		generate: function() {
			this._fetch();

			if (this._isValidType()) {
				$('div#matchmaking').addClass((this.type === 2) ? 'type-1on1' : 'type-2on2');

				if (true === this.participate && null === this.player) {
					this._error('notCheckedIn');
				}

				if ((this.type === 2 && this.matchPlayers.length >= 2)
					|| this.type === 4 && this.matchPlayers.length >= 4) {

					var incPlayers = [],
					i = 0;
					for (i; i < this.matchPlayers.length; i++) {
						incPlayers.push(this.matchPlayers[i].id);
					}

					var excPlayers = [],
					ix = 0;
					for (ix; ix < this.excludePlayers.length; ix++) {
						excPlayers.push(this.excludePlayers[ix].id);
					}

					var params = {
						type: $.matchMaker.type,
						participate: $.matchMaker.participate,
						fairShuffle: $.matchMaker.fairShuffle,
						matchPlayers: incPlayers.join(','),
						excludePlayers: excPlayers.join(',')
					};

					$.packGyver.site.pageLoader(true);
					$.post('/play/wizard', {
						data: $.base64.encode(JSON.stringify(params))
					}, function(res) {
						if (true === res.success) {
							if (res.type === 0) {
								$.matchMaker._fillTeamSpot('red', 'goal', res.teams.red.single);
								$.matchMaker._fillTeamSpot('yellow', 'goal', res.teams.yellow.single);
								$.matchMaker._getTeamSpot('yellow', 'offense').remove();
								$.matchMaker._getTeamSpot('red', 'offense').remove();
							} else {
								$.matchMaker._fillTeamSpot('red', 'goal', res.teams.red.goal);
								$.matchMaker._fillTeamSpot('red', 'offense', res.teams.red.offense);
								$.matchMaker._fillTeamSpot('yellow', 'goal', res.teams.yellow.goal);
								$.matchMaker._fillTeamSpot('yellow', 'offense', res.teams.yellow.offense);
							}

							$('div#scoreboard input.matchId').val(res.matchId);

							if (true === res.notify) {
								$.matchMaker.notifyPlayers(res.matchId, $.matchMaker.notifications.currentMatch);
							}

							$.packGyver.site.pageLoader(false);
							$.matchMaker.start();
						} else {
							var cBtn = getButton('Schließen', 'mm-cancel');
							$.packGyver.site.notify('Spiel generieren', '<div class="margin20">' + res.error + '</div>', cBtn);
						}
					}, 'json');

					return;
				} else {
					this._error('notEnoughPlayers');
				}
			} else {
				this._error('invalidType');
			}
			this._error('MatchCreationError');
		},

		start: function() {
			$('div#pregame').fadeOut(777, function() {
				$('div#mm-setup-rdy').fadeIn(777, function() {
					$('div#matchmaking-table').animate({
						opacity: 1
					}, 1000);
					setTimeout(function() {
						var els = shuffle($('div.team-container div.panel'), 2);
						els.each(function(i) {
							var $o = $(this);
							setTimeout(function() {
								$o.addClass('flip');
							}, 300 * i);
						});
					}, 1337);
				});
			});
		},

		notifyPlayers: function(matchId, type) {
			if (type === 0 || type === 1) {
				$.post('/play/notify/' + type + '/' + matchId);
			}
		},

		saveMatch: function(matchId, scoreRed, scoreYellow, isOverview) {
			var validScores = this.validateScore(scoreRed, scoreYellow);

			if (null === validScores) {
				var postData = {
					scoresRed: scoreRed,
					scoresYellow: scoreYellow
				}

				$.packGyver.site.pageLoader(true);
				$.post('/play/detail/' + matchId + '/save', postData, function(sR) {
					$.packGyver.site.pageLoader(false);
					var result = null;
					var btn = getButton('Schließen', 'fb-close-trigger');

					if (false === sR.success) {
						if (true === sR.matchAlreadySaved) {
							result = 'Das Spiel wurde bereits von ' + sR.matchSavedBy + ' gespeichert!';
						} else {
							result = 'Ein Fehler ist aufgetreten';
						}
					} else {
						if (false === isOverview) {
							btn = '<button class="btn-redirect" data-href="/play">Zur Übersicht</button>';
						}

						if (sR.matchId > 0 && true === sR.notify) {
							$.matchMaker.notifyPlayers(sR.matchId, $.matchMaker.notifications.nextMatch);
						}

						result = 'Das Spiel wurde erfolgreich gespeichert';
					}

					$.packGyver.site.notify('Spiel speichern', '<div class="margin20">' + result + '</div>', btn);

					if (true === sR.success && true === isOverview) {
						refreshMatchOverview();
					}
				}, 'json');
			} else {
				var cBtn = getButton('Schließen', 'fb-close-trigger');
				$.packGyver.site.notify('Spiel speichern', '<div class="margin20">' + validScores + '</div>', cBtn);
			}
		},

		deleteMatch: function(matchId) {
			if (!isNaN(matchId) && matchId > 0) {
				$.packGyver.site.pageLoader(true);
				$.post('/play/detail/' + matchId + '/delete', null, function(res) {
					$.packGyver.site.pageLoader(false);
					var cBtn = getButton('Schließen', 'fb-close-trigger');
					if (res.success) {
						refreshMatchOverview();
					//$.packGyver.site.notify('Spiel löschen', '<div class="margin20">Das Spiel wurde erfolgreich gelöscht!</div>', cBtn);
					} else {
						$.packGyver.site.notify('Spiel löschen', '<div class="margin20">' + res.error + '</div>', cBtn);
					}
				}, 'json');
			}
		},

		validateScore: function(scoreRed, scoreYellow) {
			var error = null;
			var intRegex = /^\d{1,2}$/;

			if(!intRegex.test(scoreRed) || !intRegex.test(scoreYellow)) {
				error = 'Es sind nur Zahlen erlaubt!';
			} else if(scoreRed === scoreYellow) {
				error = 'Es gibt kein Unentschieden!';
			} else if(scoreRed !== 10 && scoreYellow !== 10) {
				error = 'Ein Team muss 10 Tore haben!';
			} else if(scoreRed > 10 || scoreRed < 0 || scoreYellow > 10 || scoreYellow < 0) {
				error = 'Ergebnisse müssen zwischen 0 und 10 sein!';
			}

			if (null !== error) {
				error = 'Ungültiges Ergebnis. ' + error;
			}

			return error;
		}
	}
})(jQuery);