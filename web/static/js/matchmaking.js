/*
    Document   : matchmaking.js
    Created on : 01.02.2012, 13:37:00
    Description:
        Matchmaking
 */
var scoreSlider;
var sliderOptions = {
	orientation: "vertical",
	range: "min",
	min: 0,
	max: 10,
	value: 0,
	slide: function(e, ui) {
		updateScoreSliderByEvent(e, ui);
	}
};

$(function() {
	initScoreSlider();

	/* refresh match overview page */
    $(document).on('click', 'button#mm-refresh, button.mm-trigger-refresh', function() {
		refreshMatchOverview();
	});

	/* enter score view for matches in the queue */
    $(document).on('click', 'button.enter-score', function() {
		var matchId = parseInt($(this).attr('matchId'));
		$.facebox($('#match-score').clone());
		$('#facebox #match-score input.score-input').val('');
		$('#facebox #match-score input.matchId').val(matchId);
	});

	/* enter score event for queued matches */
    $(document).on('click', 'button.mm-save-score', function() {
		var matchId = parseInt($('#facebox #match-score input.matchId').val()),
		scoreRed = parseInt($('#facebox #match-score input.teamScoreRed').val()),
		scoreYellow = parseInt($('#facebox #match-score input.teamScoreYellow').val());

		if (!isNaN(matchId) && matchId > 0) {
			$.matchMaker.saveMatch(matchId, scoreRed, scoreYellow, true);
		}
	});

	/* fade in/out the score save/cancel elements */
    $(document).on('click', 'button.enter-score-cm', function() {
		$('div.cm-ops-control-front').fadeOut(300, function() {
			$('div.cm-ops-control-back, div.score-range').fadeIn(300, function() {
				$('div#scoreboard div#range-red a.ui-slider-handle').focus();
			});
		});
	});

	/* enter score event for the current match */
    $(document).on('click', 'button.save-cm', function() {
		var
		scoreRed = parseInt($('#scoreboard input.teamScoreRed').val()),
		scoreYellow = parseInt($('#scoreboard input.teamScoreYellow').val()),
		matchId = parseInt($('#scoreboard input.matchId').val()),
		isOverview = !$(this).hasClass('save-generated');

		if (!isNaN(matchId) && matchId > 0) {
			$.matchMaker.saveMatch(matchId, scoreRed, scoreYellow, isOverview);
		}
	});

	/* cancel saving a score */
    $(document).on('click', 'button.cancel-cm', function() {
		$('div.cm-ops-control-back, div.score-range').fadeOut(800, function() {
			scoreSlider.slider('value', 0);
			resetScores();
			$('div.cm-ops-control-front').fadeIn(300);
		});
	});

    $(document).on('click', 'button.delete-cm', function() {
		var button = $(this);
		var attrs = [
		{
			name: 'matchId',
			value: button.attr('matchId')
		}
		];
		var confirmButton = getButton('Löschen', 'delete-cm-confirm fb-close-trigger', attrs);
		var cancelButton = getButton('Abbrechen', 'fb-close-trigger', attrs);
		var btns = cancelButton + '' + confirmButton;
		$.packGyver.site.notify('Spiel löschen', '<div class="margin20">Möchtest Du das Spiel wirklich löschen?</div>', btns);
	});
    $(document).on('click', 'button.delete-cm-confirm', function() {
		var matchId = $(this).attr('matchId');
		$.matchMaker.deleteMatch(matchId);
	});

	/* MATCHMAKING - MATCHMAKER */
	$('a.mm-player-change-stati').click(function(e) {
		e.preventDefault();
		$.matchMaker.changePlayerPoolStati($(this).attr('status'));
		$.matchMaker.updatePlayerCount();
	});
	$('div#mm-setup-rdy div#table-content').fadeIn(600);

	/* custom scrollbar for player pool */
	$('div#pregame div#mm-choose-player').lionbars();

	/* player stati button */
	$('div#pregame div.player-mm-status > button').click(function() {
		$(this).toggleClass('deactive');
		$(this).parents('li:first').toggleClass('deactive');
		$.matchMaker.updatePlayerCount();
	});

	// prevent multiple clicks
	var mmGenerated = false;
	$('div#pregame button#mm-generate-match').click(function() {
		if (false === mmGenerated) {
			mmGenerated = true;

			try {
				$.matchMaker.generate();
			} catch (e) {
				var btn = getButton('Schließen', 'btn-redirect', [{
					name: 'data-href', 
					value: '/play'
				}]);
				$.packGyver.site.notify('Fehler beim Erstellen', '<div class="margin20"><span class="bold">Ein Fehler ist aufgetreten!</span><br />' + e + '</div>', btn);
			}
		}
	});
	$(document).on('click', 'button.mm-cancel', function() {
		window.location = '/play';
	});
});

/**
 * get the refreshed matchmaking overview content
 */
function refreshMatchOverview() {
	$.packGyver.site.pageLoader(true);
	$.post('/play/refresh', null, function(data) {
		if ($('div#match-overview', $('div#matchmaking-site')).length === 0) {
			$.packGyver.site.pageLoader(false);
			return;
		}
		$('div#match-overview', $('div#matchmaking-site')).fadeOut(300, function() {
			$('div#matchmaking-site').hide().html(data);
			initScoreSlider();
			$('div#matchmaking-site').fadeIn(300, function() {
				$.packGyver.site.pageLoader(false);
			});
		});
	});
}

/**
 * initialize the main score slider
 */
function initScoreSlider() {
	scoreSlider = $("#range-red, #range-yellow").slider(sliderOptions);

	if ($('.score-red, .score-yellow').length > 0) {
		var scoreDots = [],
		styleDefs = {
			zero: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 29, 30, 31, 32, 39, 40, 41, 42, 49, 50, 51, 52, 59, 60, 61, 62, 69, 70, 71, 72, 79, 80, 81, 82, 89, 90, 91, 92, 99, 100, 101, 102, 109, 110, 111, 112, 119, 120, 121, 122, 129, 130, 131, 132, 139, 140, 141, 142, 149, 150, 151, 152, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			one: [5, 6, 15, 16, 25, 26, 35, 36, 45, 46, 55, 56, 65, 66, 75, 76, 85, 86, 95, 96, 105, 106, 115, 116, 125, 126, 135, 136, 145, 146, 155, 156, 165, 166, 175, 176],
			two: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 29, 30, 39, 40, 49, 50, 59, 60, 69, 70, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 111, 112, 121, 122, 131, 132, 141, 142, 151, 152, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			three: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 29, 30, 39, 40, 49, 50, 59, 60, 69, 70, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 109, 110, 119, 120, 129, 130, 139, 140, 149, 150, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			four: [1, 2, 9, 10, 11, 12, 19, 20, 21, 22, 29, 30, 31, 32, 39, 40, 41, 42, 49, 50, 51, 52, 59, 60, 61, 62, 69, 70, 71, 72, 79, 70, 71, 72, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 109, 110, 119, 120, 129, 130, 139, 140, 149, 150, 159, 160, 169, 170, 179, 180],
			five: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 31, 32, 41, 42, 51, 52, 61, 62, 71, 72, 71, 72, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 109, 110, 119, 120, 129, 130, 139, 140, 149, 150, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			six: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 31, 32, 41, 42, 51, 52, 61, 62, 71, 72, 71, 72, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 109, 110, 111, 112, 119, 120, 121, 122, 129, 130, 131, 132, 139, 140, 141, 142, 149, 150, 151, 152, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			seven: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 29, 30, 39, 40, 49, 50, 59, 60, 69, 70, 79, 80, 89, 90, 99, 100, 109, 110, 119, 120, 129, 130, 139, 140, 149, 150, 159, 160, 169, 170, 179, 180],
			eight: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 29, 30, 31, 32, 39, 40, 41, 42, 49, 50, 51, 52, 59, 60, 61, 62, 69, 70, 71, 72, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 109, 110, 111, 112, 119, 120, 121, 122, 129, 130, 131, 132, 139, 140, 141, 142, 149, 150, 151, 152, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			nine: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 29, 30, 31, 32, 39, 40, 41, 42, 49, 50, 51, 52, 59, 60, 61, 62, 69, 70, 71, 72, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 109, 110, 119, 120, 129, 130, 139, 140, 149, 150, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180],
			ten: [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 29, 30, 31, 32, 34, 35, 39, 40, 41, 42, 44, 45, 49, 50, 51, 52, 54, 55, 59, 60, 61, 62, 64, 65, 69, 70, 71, 72, 74, 75, 79, 80, 81, 82, 84, 85, 89, 90, 91, 92, 94, 95, 99, 100, 101, 102, 104, 105, 109, 110, 111, 112, 114, 115, 119, 120, 121, 122, 124, 125, 129, 130, 131, 132, 134, 135, 139, 140, 141, 142, 144, 145, 149, 150, 151, 152, 154, 155, 159, 160, 161, 162, 164, 165, 166, 167, 168, 169, 170, 171, 172, 174, 175, 176, 177, 178, 179, 180]
		},
		style = document.styleSheets[document.styleSheets.length - 1];

		for (var i = 0; i < 180; ++i) {
			scoreDots.push('<span class="sc-d"></span>');
		}
		for (var def in styleDefs) {
			styleDefs[def].forEach(function (a, b) {
				style.insertRule("#scoreboard .scorecard." + def + " .sc-d:nth-child(" + a + ")" + "{opacity: 1;}", style.cssRules ? style.cssRules.length : 0);
			});
		}
		$('.score-red, .score-yellow').append(scoreDots.join(""));
		updateScoreSliderByInputs();
	}
}

/**
 * set team score in score slider
 * 
 * @param team string
 * @param val int
 */
function setScoreSliderValue(team, val) {
	var
	score = $('.score-' + team),
	regexp = /\s[zero|one|two|three|four|five|six|seven|eight|nine|ten]{1,}$/g,
	scores = {
		0: 'zero',
		1: 'one',
		2: 'two',
		3: 'three',
		4: 'four',
		5: 'five',
		6: 'six',
		7: 'seven',
		8: 'eight',
		9: 'nine',
		10: 'ten'
	};

	if (score.length > 0) {
		score.attr('class', score.attr('class').replace(regexp, '') + ' ' + scores[val]);
	}
}

/**
 * reset the main score slider
 */
function resetScores() {
	updateScoreSlider('red', 0);
	updateScoreSlider('yellow', 0);
	setScoreSliderValue('red', 0);
	setScoreSliderValue('yellow', 0);
}

/**
 * update the main score slider
 */
function updateScoreSliderByEvent(e, ui) {
	updateScoreSlider($(e.target).attr('team'), ui.value);
}

function updateScoreSliderByInputs() {
	updateScoreSlider('red', $('div#scoreboard input.teamScoreRed').val());
	updateScoreSlider('yellow', $('div#scoreboard input.teamScoreYellow').val());
}

function updateScoreSlider(team, score) {
	setScoreSliderValue(team, score);
	$("input.teamScore" + (team.charAt(0).toUpperCase() + team.slice(1))).val(score);
}

/**
 * 
 * @param arrs array
 * @param repeats int
 * @return array
 */
function shuffle(arrs, repeats){
	if(!repeats) {
		repeats = 1;
	}
    
	var i = 0;
	do {
		var s = [];
		while (arrs.length) {
			s.push(arrs.splice(Math.random() * arrs.length, 1)[0]);
		}
		while (s.length) {
			arrs.push(s.pop());
		}
		i++;
	} while (repeats > i);

	return arrs;
}