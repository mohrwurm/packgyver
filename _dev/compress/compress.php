<?php

# $ php compress.php css
# $ php compress.php js

error_reporting(E_ALL);

require __DIR__ . '/../../app/init.inc.php';

define('YUI', __DIR__ . DS . 'yuicompressor-2.4.7.jar');
define('TYPE_CSS', '.css');
define('TYPE_JS', '.js');

if (!file_exists(YUI)) {
	info('[ERROR] YUI Compressor not found!');
	exit;
}

$tcType = (isset($argv[1]) && $argv[1] == 'css') ? TYPE_CSS : TYPE_JS;

if (TYPE_JS === $tcType) {
	$ext = '.js';
} else {
	$ext = '.css';
}

$path = PG::getStaticDir();

if (is_dir($path)) {
	foreach (getCompressable($tcType) as $outputName => $jobs) {
		$content = null;
		$count = count($jobs);

		if (is_numeric($outputName)) {
			$outputName = $jobs[0];
		} else {
			if (isWindows()) {
				$outputName = replaceDS($outputName);
			}
		}

		info('#################### Processing: ' . $outputName . ' ####################');

		foreach ($jobs as $job) {
			if (isWindows()) {
				$job = replaceDS($job);
			}

			$file = $path . $job . $ext;

			if (file_exists($file)) {
				info('> [INFO] Reading file: ' . $file);
				$content .= file_get_contents($file);
			} else {
				info('> [WARNING] File not found: ' . $file);
			}
		}

		$tmpFile = null;

		if ($count > 1) {
			$tmpFile = $path . $outputName . $tcType;

			if (is_writable($path)) {
				info('> Creating file (uncompressed): ' . $tmpFile);
				file_put_contents($tmpFile, $content);
			} else {
				throw new ErrorException('cant write $tmpFile to: ' . $tmpFile);
			}
		}

		info('> Creating file (compressed): ' . $path . $outputName . '.min' . $tcType);

		if (file_exists($path . $outputName . '.min' . $tcType)) {
			unlink($path . $outputName . '.min' . $tcType);
		}

		$outputFile = $path . $outputName . '.min' . $tcType;

		if (is_writable($path)) {
			$exec = exec(escapeshellcmd('java -jar ' . YUI . ' ' . $path . $outputName . $tcType . ' -o ' . $outputFile));
		} else {
			throw new ErrorException('cant write minified version: ' . $outputFile);
		}

		if (null !== $tmpFile && file_exists($tmpFile)) {
			info('> Deleting temp file (uncompressed): ' . $tmpFile);
			unlink($tmpFile);
		}

		info('> Result: ' . $exec);
	}
}

/**
 *
 * @param int $type
 * @return array
 */
function getCompressable($type) {
	$staticCSS = array(
		'/css/_min/packgyver.all' => array(
			'/css/plugins/jquery-ui/smoothness/jquery-ui-1.11.1.custom',
			'/css/plugins/jquery.dataTables',
			'/css/plugins/font-awesome',
			'/css/plugins/tipsy',
			'/css/plugins/facebox',
			'/css/plugins/select2',
			'/css/plugins/picker/default',
			'/css/plugins/picker/default.date',
			'/css/styles',
			'/css/matchmaking',
			'/css/sidebar'
		)
	);

	$staticJS = array(
		'/js/_min/packgyver.all' => array(
			'/js/plugins/jquery-2.1.1',
			'/js/plugins/jquery-ui-1.11.1.custom',
			'/js/plugins/highcharts.src',
			'/js/plugins/jquery.dataTables',
			'/js/plugins/jquery.base64.min',
			'/js/plugins/jquery.tipsy',
			'/js/plugins/jquery.hotkeys',
			'/js/plugins/picker/picker',
			'/js/plugins/picker/picker.date',
			'/js/plugins/timeago/timeago',
			'/js/plugins/timeago/locales/timeago.de',
			'/js/plugins/facebox',
			'/js/plugins/select2',
			'/js/packgyver',
			'/js/script'
		),
		'/js/_min/packgyver.mm' => array(
			'/js/plugins/jquery.lionbars',
			'/js/matchmaker',
			'/js/matchmaking'
		)
	);

	if (TYPE_JS === $type) {
		return $staticJS;
	} else {
		return $staticCSS;
	}
}

/**
 * 
 * @return bool
 */
function isWindows() {
	return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
}

/**
 * 
 * @param string $path
 * @return string
 */
function replaceDS($path) {
	return str_replace('/', '\\', $path);
}

/**
 *
 * @param string $msg
 */
function info($msg) {
	echo $msg . PHP_EOL;
}
