<?php

/**
 * PackGyver - Season
 * 
 * @package season
 */
class Season {

	/**
	 *
	 * @var int
	 */
	private $month;

	/**
	 *
	 * @var int
	 */
	private $year;

	/**
	 *
	 * @var \PlayerCollection
	 */
	private $playerCollection;

	/**
	 * 
	 * @return int
	 */
	public function getMonth() {
		return $this->month;
	}

	/**
	 * 
	 * @param int $month
	 * @return \Season
	 */
	public function setMonth($month) {
		$this->month = $month;

		return $this;
	}

	/**
	 * 
	 * @return int
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * 
	 * @param int $year
	 * @return \Season
	 */
	public function setYear($year) {
		$this->year = $year;

		return $this;
	}

	/**
	 * 
	 * @param \PlayerCollection $players
	 * @return \Season
	 */
	public function setPlayerCollection(\PlayerCollection $players) {
		$this->playerCollection = $players;

		return $this;
	}

	/**
	 * 
	 * @return \PlayerCollection
	 */
	public function getPlayerCollection() {
		return $this->playerCollection;
	}

	/**
	 * 
	 * @return \PlayerCollection
	 */
	public function getTop3Players() {
		$i = 0;
		$players = new PlayerCollection();

		foreach ($this->getPlayerCollection() as $player) {
			if ($i > 2) {
				break;
			}

			$players->appendPlayer($player);
			$i++;
		}

		return $players;
	}

	public function toHashSimple() {
		$hash = array();

		$hash['month'] = $this->getMonth();
		$hash['monthName'] = DateUtil::getMonthNameForMonth($this->getMonth());
		$hash['monthNameShort'] = DateUtil::getShortMonthNameForMonth($this->getMonth());
		$hash['year'] = $this->getYear();

		return $hash;
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash($withStats = false) {
		return array_merge($this->toHashSimple(), array(
					'players' => $this->getPlayerCollection()->toHash($withStats)
				));
	}

}
