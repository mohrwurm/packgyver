<?php

/**
 * PackGyver - Season Collection
 * 
 * @package season
 */
class SeasonCollection implements IteratorAggregate, Countable {

	/**
	 *
	 * @var \ArrayObject
	 */
	private $seasons;

	/**
	 * 
	 * @param array $seasons
	 */
	public function __construct(array $seasons = array()) {
		$this->seasons = new ArrayObject($seasons);
	}

	/**
	 * 
	 * @return int
	 */
	public function count() {
		return $this->seasons->count();
	}

	/**
	 * 
	 * @return Iterator
	 */
	public function getIterator() {
		return $this->seasons->getIterator();
	}

	/**
	 * 
	 * @return \SeasonCollection
	 */
	public function getSeasons() {
		return $this->seasons;
	}

	/**
	 * 
	 * @param \Season $season
	 */
	public function appendSeason(\Season $season) {
		$this->seasons->append($season);
	}

	public function deleteSeason(\Season $season) {
		
	}

	public function existsSeason(\Season $season) {
		
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		foreach ($this as $season) {
			/* @var $season \Season */
			$hash[] = $season->toHash();
		}

		return $hash;
	}

}
