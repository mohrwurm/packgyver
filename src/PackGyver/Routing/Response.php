<?php

/**
 * PackGyver - Response
 */
class Response {

	/**
	 * @var HttpHeader
	 */
	private $httpHeader;

	/**
	 * @var mixed
	 */
	private $data;

	/**
	 * CTOR
	 *
	 * @param mixed $data
	 * @param HttpHeader $header
	 */
	public function __construct($data = null, HttpHeader $header = null) {
		$this->data = $data;
		$this->httpHeader = $header;
	}

	/**
	 * @param HttpHeader $header
	 */
	public function setHttpHeader(HttpHeader $header) {
		$this->httpHeader = $header;
	}

	/**
	 * @return HttpHeader
	 */
	public function getHttpHeader() {
		return $this->httpHeader;
	}

	/**
	 * @param mixed $data
	 */
	public function setData($data) {
		$this->data = $data;
	}

	/**
	 * @return mixed
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @return bool
	 */
	public function hasHttpHeader() {
		return null !== $this->getHttpHeader();
	}

	/**
	 * send response
	 */
	public function execute() {
		if (true === $this->hasHttpHeader()) {
			$this->getHttpHeader()->send();
		}
		echo $this->getData();
	}

	/**
	 * create response with mime
	 *
	 * @param mixed $data
	 * @param string $mimeType
	 * @param int $status
	 * @return Response
	 */
	public static function withMimeType($data, $mimeType = 'text/plain', $status = 200) {
		$header = new HttpHeader();
		$header->setMimeType($mimeType);
		$header->setStatusCode($status);

		return new Response($data, $header);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getData();
	}

}

?>