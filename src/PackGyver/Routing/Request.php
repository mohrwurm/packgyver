<?php

/**
 * PackGyver - Request
 */
class Request {

	const TYPE_POST = 0;
	const TYPE_GET = 1;

	/**
	 * @var request
	 */
	private static $instance;

	/**
	 * @return Request
	 */
	public static function getInstance() {
		if (null === self::$instance) {
			self::$instance = new Request();
		}
		return self::$instance;
	}

	/**
	 * @param string $index
	 * @param int $type
	 * @return string
	 */
	public function getParam($index, $type = self::TYPE_POST) {
		if ($type === self::TYPE_POST) {
			return isset($_POST[$index]) ? $_POST[$index] : null;
		} else {
			return isset($_GET[$index]) ? $_GET[$index] : null;
		}
	}

	/**
	 * @param string $index
	 * @param int $type
	 * @return int
	 */
	public function getParamInt($index, $type = self::TYPE_POST) {
		$val = $this->getParam($index, $type);
		return is_numeric($val) ? intval($val) : null;
	}

	/**
	 * @return bool
	 */
	public function isAjax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	}

	/**
	 * @return string
	 */
	public function getRemoteIp() {
		return $_SERVER['REMOTE_ADDR'];
	}

}
