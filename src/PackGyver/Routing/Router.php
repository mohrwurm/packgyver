<?php

/**
 * PackGyver - Route
 */
class Router {

	const ROUTES_FILE = 'routes.xml';
	const CONTROLLER_FUNCTION_PREFIX = 'action';
	const DEFAULT_CONTROLLER = 'Home';
	const DEFAULT_CONTROLLER_SUFFIX = 'Controller';
	const DEFAULT_CONTROLLER_FUNCTION = 'Default';

	/**
	 * @var Router
	 */
	private static $instance;

	/**
	 * @var Controller
	 */
	private $controller;

	/**
	 * @var string
	 */
	private $function;

	/**
	 * @var array
	 */
	private $parameters = array();

	/**
	 * @var string
	 */
	private $mapping;

	/**
	 * @return Router
	 */
	public static function instance() {
		if (null === self::$instance) {
			self::$instance = new Router();
		}
		return self::$instance;
	}

	/**
	 * @return string
	 */
	public function getController() {
		return $this->controller;
	}

	/**
	 * @return string
	 */
	public function getMapping() {
		return $this->mapping;
	}

	/**
	 * @return string
	 */
	public function getFunction() {
		return $this->function;
	}

	/**
	 * @return array
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * Get the Meta Title for the controller
	 * 
	 * @return string|null
	 */
	public function getPageTitle() {
		if ($this->getController() instanceof Controller) {
			return $this->getController()->getMetaTitle();
		}

		return null;
	}

	/**
	 * @return Response
	 * @throws PackGyverException
	 */
	public function handle() {
		$response = null;
		$urlPackets = $this->parseUrl();
		$this->mapping = $urlPackets['mapping'];
		$this->function = $urlPackets['function'];
		$this->parameters = $urlPackets['parameters'];

		$validator = new RequestValidator();
		$isValidRequest = $validator->isValid($this->getMapping());

		if (true === $isValidRequest) {
			if ($this->getMapping()) {
				$cntrl = ucfirst($this->getMapping());
			} else {
				$cntrl = self::DEFAULT_CONTROLLER;
			}

			$cntrl .= self::DEFAULT_CONTROLLER_SUFFIX;

			$controllerPath = $this->getControllersPath() . $cntrl . '.php';
			$function = null;
			$defaultFunction = self::CONTROLLER_FUNCTION_PREFIX . self::DEFAULT_CONTROLLER_FUNCTION;

			if (file_exists($controllerPath)) {
				$this->controller = new $cntrl();
				$this->controller->setMethod($this->getFunction());
				$this->controller->setParameters($this->getParameters());

				if ($this->getFunction()) {
					$function = self::CONTROLLER_FUNCTION_PREFIX . ucfirst($this->getFunction());
				}

				if (null !== $function && is_callable(array($this->controller, $function))) {
					$response = call_user_func(array($this->controller, $function));
				} else if (is_callable(array($this->controller, $defaultFunction))) {
					$response = call_user_func(array($this->controller, $defaultFunction));
				} else {
					throw new PackGyverException(get_class($this->controller) . ' does not provide a default handler...');
				}
			} else {
				$this->controller = new NotfoundController();
				$response = $this->controller->actionDefault();
			}
		} else {
			$defaultController = self::DEFAULT_CONTROLLER . self::DEFAULT_CONTROLLER_SUFFIX;
			$this->controller = new $defaultController;
			$this->function = self::CONTROLLER_FUNCTION_PREFIX . self::DEFAULT_CONTROLLER_FUNCTION;

			$response = $validator->getErrorContent();
			$singleResponse = DwooProxy::getInstance()->parseTemplate('singles/main', array(
				'pgMainContent' => $response
			));

			return new Response($singleResponse);
		}

		if (!$response instanceof Response) {
			$response = new Response($response);
		}

		if ($response->hasHttpHeader()) {
			return $response;
		}

		$mainContent = array(
			'pgMainContent' => $response->getData(),
			'pgError' => !$isValidRequest
		);

		$hSResponse = new Response(DwooProxy::getInstance()->parseTemplate('main', $mainContent));

		return $hSResponse;
	}

	/**
	 *
	 * @return array
	 */
	private function parseUrl() {
		if (StringUtil::beginsWith($_SERVER['SCRIPT_NAME'], $_SERVER['REQUEST_URI'])) {
			$url = substr($_SERVER['REQUEST_URI'], strlen($_SERVER['SCRIPT_NAME']));
		} else {
			$url = $_SERVER['REQUEST_URI'];
		}

		if (StringUtil::contains('?', $url)) {
			$url = substr($url, 0, strpos($url, '?'));
		}

		$url = array_slice(explode('/', $url), 1);

		return array(
			'mapping' => isset($url[0]) ? $url[0] : null,
			'function' => isset($url[1]) ? $url[1] : null,
			'parameters' => array_slice($url, 2),
		);
	}

	/**
	 * get path for the controllers
	 *
	 * @return string
	 */
	private function getControllersPath() {
		return SRC_PATH . 'PackGyver/Controller' . DS;
	}

}
