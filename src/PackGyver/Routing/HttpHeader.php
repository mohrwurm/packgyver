<?php

/**
 * PackGyver - HTTTP Header
 */
class HttpHeader {

	const MIMETYPE_JSON = 'application/json';
	const MIMETYPE_HTML = 'text/html';

	/**
	 * @var string
	 */
	private $mimeType;

	/**
	 * @var int
	 */
	private $statusCode;

	/**
	 * @var string
	 */
	private $lastModified;

	/**
	 * @var array
	 */
	public static $statusCodes = array(
		/* info */
		100 => 'Continue',
		101 => 'Switching Protocols',
		/* success */
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		207 => 'Multi-Status',
		/* redirect */
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		/* client error */
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		422 => 'Unprocessable Entity',
		423 => 'Locked',
		424 => 'Failed Dependency',
		/* server error */
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		507 => 'Insufficient Storage',
		509 => 'Bandwidth Limit Exceeded'
	);

	/**
	 * @param int $status
	 */
	public function __construct($status = null) {
		$this->statusCode = $status;
	}

	/**
	 * @param string $mimeType
	 */
	public function setMimeType($mimeType) {
		$this->mimeType = $mimeType;
	}

	/**
	 * @return string
	 */
	public function getMimeType() {
		return $this->mimeType;
	}

	/**
	 * @param int $sCode
	 */
	public function setStatusCode($sCode) {
		$this->statusCode = $sCode;
	}

	/**
	 * @return int
	 */
	public function getStatusCode() {
		return $this->statusCode;
	}

	/**
	 * @param string $lastModified
	 */
	public function setLastModified($lastModified) {
		$this->lastModified = $lastModified;
	}

	/**
	 * @return string
	 */
	public function getLastModified() {
		return $this->lastModified;
	}

	/**
	 * send headers if available
	 */
	public function send() {
		if (null !== $this->getMimeType()) {
			header('Content-type: ' . $this->getMimeType());
		}
		if (null !== $this->getStatusCode()) {
			header('HTTP/1.0 ' . $this->getStatusCode() . ' ' . self::$statusCodes[$this->getStatusCode()]);
		}
		if (null !== $this->getLastModified()) {
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $this->getLastModified()) . ' GMT', true);
		}
	}

}
