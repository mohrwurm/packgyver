<?php

/**
 * PackGyver - Request Validator
 */
class RequestValidator {

	const ERROR_TYPE_INVALID_BROWSER = 'browser';
	const ERROR_TYPE_NOT_LOGGED_IN = 'login';

	/**
	 * @var string
	 */
	private $error;

	/**
	 * @var string
	 */
	private $errorType;

	/**
	 * @return string
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * @param string $mapping
	 * @return bool
	 */
	public function isValid($mapping) {
		if ($mapping === 'cron' || $mapping == 'api') {
			return true;
		}

		if (false === BrowserUtil::isAcceptable()) {
			$this->errorType = self::ERROR_TYPE_INVALID_BROWSER;
			return false;
		}

		if (false === AuthenticationManager::getInstance()->isLoggedIn() && $mapping != 'authenticate') {
			$this->errorType = self::ERROR_TYPE_NOT_LOGGED_IN;
			$this->error = AuthenticationManager::getInstance()->getError();
			return false;
		}

		return true;
	}

	/**
	 * @return string
	 */
	public function getErrorContent() {
		switch ($this->errorType) {
			case self::ERROR_TYPE_INVALID_BROWSER:
				$data = array('browser' => BrowserUtil::getInformation());
				break;
			case self::ERROR_TYPE_NOT_LOGGED_IN;
			default:
				$data = array(
					'redirect' => rawurlencode($_SERVER['REQUEST_URI']),
					'data' => array('error' => $this->getError())
				);
		}

		return DwooProxy::getInstance()->parseTemplate('singles/' . $this->errorType, $data);
	}

}
