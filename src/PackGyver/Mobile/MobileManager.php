<?php

/**
 * PackGyver - Mobile Manager
 *
 * @package mobile
 */
class MobileManager {

	/**
	 *
	 * @return bool
	 */
	public static function isMobileDevice() {
		return 0 !== preg_match('/mobile/i', $_SERVER['HTTP_USER_AGENT']);
	}

}

?>