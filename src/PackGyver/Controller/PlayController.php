<?php

/**
 * PackGyver - Matchmaking Controller
 */
class PlayController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Play::Default';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Spielen');

		/*if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}*/

		$cache = $this->render('matchmaking/view', $this->getDefaultViewData(PG::getDB()));
		/*Cache::getCache()->set(self::CACHE_DEFAULT, $cache);*/

		return new Response($cache);
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionWizard() {
		$db = PG::getDB();
		$data = trim($_POST['data']);
		$result = array(
			'success' => true,
			'notify' => MatchManager::isNotificationEnabled()
		);

		if (!AuthenticationManager::getInstance()->getPlayer()->getActive()) {
			$result = array(
				'success' => false,
				'error' => 'Du bist derzeit inaktiv und kannst somit keine Spiele erstellen/spielen. Melde dich bei einem Admin, wenn du wieder aktiviert werden willst.'
			);
		} else if (!MatchManager::isMatchCreatable($db)) {
			$result = array(
				'success' => false,
				'error' => 'Derzeit ist die maximale Anzahl an Spielen in der Warteschlange ausgereizt. Bitte warte, bis Spiele gespeichert werden und somit Plätze frei werden.'
			);
		} else if (isset($data) && strlen($data) > 0) {
			$validBase64 = (0 !== preg_match('/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/', $data));

			if (true === $validBase64) {
				$data = json_decode(base64_decode($data));

				if (isset($data->type) && ($data->type === 4 || $data->type === 2)) {
					$incPlayers = array_unique(explode(',', $data->matchPlayers));
					$excPlayers = array_unique(explode(',', $data->excludePlayers));
					$validPlayers = array();

					foreach ($incPlayers as $incPlayer) {
						$player = PlayerEntity::findById($db, (int) $incPlayer);

						if ($player instanceof PlayerEntity) {
							$playerModel = new PlayerModel($player);

							if ($playerModel->isValidToPlay()) {
								$validPlayers[] = $playerModel;
							}
						}
					}

					if (count($validPlayers) >= $data->type) {
						$type = ($data->type === 4) ? MatchModel::TYPE_2ON2 : MatchModel::TYPE_1ON1;
						$validTypes = MatchModel::getActiveMatchTypes();

						if (isset($validTypes[$type])) {
							if (true === $data->fairShuffle) {
								//$shuffler = new MatchShuffleFair($type, $data->participate);
								$shuffler = new MatchShuffleRandom($type, $data->participate);
							} else {
								$shuffler = new MatchShuffleRandom($type, $data->participate);
							}

							foreach ($validPlayers as $validPlayer) {
								/* @var PlayerModel $validPlayer */
								$shuffler->appendPlayer($validPlayer);
							}

							$match = $shuffler->shuffle();
							$match->setType($type);
							$match->setDateCreated(DateUtil::formatForDB());
							$match->setRandom(true);
							$match->setCreatedByPlayer(new PlayerModel(AuthenticationManager::getInstance()->getPlayer()));
							$match->setSavedByPlayer(new PlayerModel(AuthenticationManager::getInstance()->getPlayer()));
							$match->setScoreRed(0);
							$match->setScoreYellow(0);

							if (true === $match->register($db)) {
								$result['success'] = true;
								$result['type'] = $match->getType();
								$result['matchId'] = $match->getMatchEntity()->getId();

								if ($match->getType() === MatchModel::TYPE_1ON1) {
									$result['teams']['red']['single'] = $match->getRedSinglePlayer()->toHash(true);
									$result['teams']['yellow']['single'] = $match->getYellowSinglePlayer()->toHash(true);
								} else {
									$result['teams']['red']['goal'] = $match->getRedGoalPlayer()->toHash(true);
									$result['teams']['yellow']['goal'] = $match->getYellowGoalPlayer()->toHash(true);
									$result['teams']['red']['offense'] = $match->getRedOffensePlayer()->toHash(true);
									$result['teams']['yellow']['offense'] = $match->getYellowOffensePlayer()->toHash(true);
								}

								$wsservice = WebSocketServiceManager::getInstance();

								if ($wsservice->isEnabled()) {
									$wsservice->getDataDispatcher()->triggerCreateMatchEvent(AuthenticationManager::getInstance()->getPlayer(), $match);
								}

								Cache::getCache()->delete(self::CACHE_DEFAULT);

								Logger::get()->info('CreateMatch', array(
									'MatchId' => $match->getMatchEntity()->getId(),
									'Excluded' => implode(', ', $excPlayers)
								));
							} else {
								$result['success'] = false;
								$result['error'] = 'Ein Fehler ist aufgetreten!';
							}
						} else {
							$result['success'] = false;
							$result['error'] = 'Ungültiger Spieltyp!';
						}
					} else {
						$result['success'] = false;
						$result['error'] = 'Es sind nicht genügend Spieler eingecheckt!';
					}
				} else {
					$result['success'] = false;
					$result['error'] = 'Ungültiger Spieltyp!';
				}
			} else {
				$result['success'] = false;
				$result['error'] = 'Die übermittelten Daten sind ungültig!';
			}
		}

		return Response::withMimeType(json_encode($result));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionNotify() {
		if ($this->getRequest()->isAjax()) {
			$db = PG::getDB();
			$type = (int) $this->getParameter(0);
			$match = MatchEntity::findById($db, (int) $this->getParameter(1));

			if ($match instanceof MatchEntity) {
				if ($this->hasPermission($db, $match)) {
					$matchModel = new MatchModel($match);
					$matchModel->notifyPlayers($type);
				}
			}

			return Response::withMimeType(null);
		}
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionDetail() {
		$db = PG::getDB();
		$match = MatchEntity::findById($db, (int) $this->getParameter(0));

		if ($match instanceof MatchEntity) {
			if ($this->getParameter(1) == 'save' && $this->getRequest()->isAjax()) {
				return Response::withMimeType($this->saveScore($db, $match));
			} else if ($this->getParameter(1) == 'delete' && $this->getRequest()->isAjax()) {
				$result = array();

				if ($this->hasPermission($db, $match)) {
					$match->setDeleted(true)
							->setDateSaved(DateUtil::formatForDB())
							->updateToDatabase($db);

					$matchModel = new MatchModel($match);
					$matchModel->recalculatePlayerStats();

					$wsservice = WebSocketServiceManager::getInstance();

					if ($wsservice->isEnabled()) {
						$wsservice->getDataDispatcher()->triggerDeleteMatchEvent(AuthenticationManager::getInstance()->getPlayer());
					}

					Cache::getCache()->delete(self::CACHE_DEFAULT);

					$result['success'] = true;
				} else {
					$result['success'] = false;
					$result['error'] = 'Du hast nicht an diesem Spiel teilgenommen und kannst es daher auch nicht bearbeiten!';
				}

				return Response::withMimeType(json_encode($result));
			}
		}

		$this->redirect('/play');
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionCreate() {
		$this->setMetaTitle('Spiel erstellen');

		$db = PG::getDB();
		$data = array();

		$data['function'] = 'createMatch';

		if (!AuthenticationManager::getInstance()->getPlayer()->getActive()) {
			$data['error'] = 'Du bist derzeit inaktiv und kannst somit keine Spiele erstellen/spielen. Melde dich bei einem Admin, wenn du wieder aktiviert werden möchtest.';
		} else {
			$players = PlayerModel::getPlayersAsHash($db, true, true, false, true);
			$matchSettings = SettingsManager::getGroup('match');
			$maxMatchesInQueue = $matchSettings->getValue('matchQueueLimit', null);

			if (null === $maxMatchesInQueue) {
				$matchSettings->setValue('matchQueueLimit', 3);
				$maxMatchesInQueue = 3;
			}

			$activeTypes = MatchModel::getActiveMatchTypes();

			if (!isset($activeTypes[MatchModel::TYPE_1ON1]) && !isset($activeTypes[MatchModel::TYPE_2ON2])) {
				$data['error'] = 'Derzeit können keine Spiele erstellt werden.';
			} else {
				$activeTypes = MatchModel::getActiveMatchTypes();

				if (isset($activeTypes[MatchModel::TYPE_1ON1])) {
					$playerLimit = 2;
				} else if (isset($activeTypes[MatchModel::TYPE_2ON2])) {
					$playerLimit = 4;
				}

				if (MatchManager::countUnsavedMatches($db) >= $maxMatchesInQueue) {
					$data['error'] = 'Die Anzahl an maximalen Spielen in der Warteschlange ist ausgeschöpft.';
				} else if (count($players) < $playerLimit) {
					$data['error'] = 'Es müssen mindestens 4 Spieler eingecheckt sein, um ein Spiel zu starten!';
				}

				$data['players'] = $players;
				$data['is1on1Active'] = isset($activeTypes[MatchModel::TYPE_1ON1]);
				$data['is2on2Active'] = isset($activeTypes[MatchModel::TYPE_2ON2]);
			}
		}

		return new Response($this->render('matchmaking/view', $data));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionRefresh() {
		return Response::withMimeType($this->render('matchmaking/matchOverview', $this->getDefaultViewData(PG::getDB())));
	}

	/**
	 * check if player has match administration rights
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 * @return bool
	 */
	private function hasPermission(PDO $db, MatchEntity $match) {
		return PlayerModel::isPlayerAdmin() || PlayerMatchManager::hasPlayerParticipatedInMatch($db, $match);
	}

	/**
	 * save match score
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 */
	private function saveScore(PDO $db, MatchEntity $match) {
		$success = false;
		$matchAlreadySaved = false;
		$matchSavedBy = null;
		$isMatchUnsaved = MatchManager::isMatchUnsaved($match);
		$hasPermission = $this->hasPermission($db, $match);

		if (true === $hasPermission) {
			if (true === $isMatchUnsaved) {
				if (is_numeric($_POST['scoresRed']) && is_numeric($_POST['scoresYellow'])) {
					$resultRed = (int) $_POST['scoresRed'];
					$resultYellow = (int) $_POST['scoresYellow'];

					if (MatchManager::isValidScore($resultRed, $resultYellow)) {
						$player = AuthenticationManager::getInstance()->getPlayer();
						$playerModel = new PlayerModel($player);
						$matchModel = new MatchModel($match);
						$matchModel->setSavedByPlayer($playerModel);
						$matchModel->setScoreRed($resultRed);
						$matchModel->setScoreYellow($resultYellow);
						$matchModel->setDateSaved(DateUtil::formatForDB());
						$matchModel->updateScore($db);

						$success = true;

						$wsservice = WebSocketServiceManager::getInstance();

						if ($wsservice->isEnabled()) {
							$wsservice->getDataDispatcher()->triggerSaveMatchEvent(AuthenticationManager::getInstance()->getPlayer());
						}
					}
				}
			} else {
				$player = PlayerEntity::findById($db, $match->getSavedByPlayerId());
				if ($player instanceof PlayerEntity) {
					$matchSavedBy = $player->getFirstname() . ' ' . $player->getLastname();
				}
				$matchAlreadySaved = true;
			}
		} else {
			return json_encode(array(
						'success' => false
					));
		}

		if (true === $success) {
			Cache::getCache()->delete(self::CACHE_DEFAULT);
			Cache::getCache()->delete(RankingController::CACHE_DEFAULT);
			Cache::getCache()->delete(StatsController::CACHE_DEFAULT);

			$matchModel = new MatchModel($match);

			foreach ($matchModel->getPlayers() as $matchPlayer) {
				/* @var PlayerModel $matchPlayer */
				$matchPlayer->getStats(PlayerStatistics::CACHETYPE_SEASON)->recalculate();
				CacheManager::clearPlayerCache($matchPlayer->getEntity());
			}

			$matchModel->attach(new WinStreakObserver());
			$matchModel->notify();
		}

		return json_encode(array(
					'success' => $success,
					'matchAlreadySaved' => $matchAlreadySaved,
					'matchSavedBy' => $matchSavedBy,
					'notify' => MatchManager::isNotificationEnabled(),
					'matchId' => $match->getId()
				));
	}

	/**
	 *
	 * @return array
	 */
	public function actionChangeResult() {
		$result = array(
			'success' => false
		);

		if (isset($_POST['matchId']) && is_numeric($_POST['matchId'])) {
			$db = PG::getDB();
			$matchEntity = MatchEntity::findById($db, (int) $_POST['matchId']);

			if ($matchEntity instanceof MatchEntity) {
				if (MatchManager::isMatchCurrentMatch($db, $matchEntity)) {
					$team = ($_POST['team'] == 'red') ? 'red' : 'yellow';
					$type = ($_POST['type'] == '+') ? '+' : '-';
					$score = 0;

					$valid = false;

					if ($type == '+') {
						if ($team == 'red') {
							$valid = ($matchEntity->getResultTeamRed() < 10);
						} else {
							$valid = ($matchEntity->getResultTeamYellow() < 10);
						}
					} else {
						if ($team == 'red') {
							$valid = ($matchEntity->getResultTeamRed() > 0);
						} else {
							$valid = ($matchEntity->getResultTeamYellow() > 0);
						}
					}

					if (true === $valid) {
						if ($team == 'red') {
							if ($type == '+') {
								$matchEntity->setResultTeamRed($matchEntity->getResultTeamRed() + 1);
							} else {
								$matchEntity->setResultTeamRed($matchEntity->getResultTeamRed() - 1);
							}
							$score = $matchEntity->getResultTeamRed();
						} else {
							if ($type == '+') {
								$matchEntity->setResultTeamYellow($matchEntity->getResultTeamYellow() + 1);
							} else {
								$matchEntity->setResultTeamYellow($matchEntity->getResultTeamYellow() - 1);
							}
							$score = $matchEntity->getResultTeamYellow();
						}

						if ($score == 10) {
							$matchEntity->setDateSaved(DateUtil::formatForDB())
									->setSavedByPlayerId(AuthenticationManager::getInstance()->getPlayer()->getId());
						}

						$matchEntity->updateToDatabase($db);
						$result['success'] = true;

						if ($score == 10) {
							$matchModel = new MatchModel($matchEntity);
							foreach ($matchModel->getPlayers() as $matchPlayer) {
								/* @var $matchPlayer PlayerModel */
								$matchPlayer->getStats(PlayerStatistics::CACHETYPE_SEASON)->recalculate();
							}

							$matchModel->attach(new WinStreakObserver());
							$matchModel->notify();
						}

						$wsservice = WebSocketServiceManager::getInstance();

						if ($wsservice->isEnabled()) {
							$wsservice->getDataDispatcher()->triggerMatchScoreEvent(AuthenticationManager::getInstance()->getPlayer(), $matchEntity, $team, $score);
						}

						Cache::getCache()->delete(self::CACHE_DEFAULT);
					}
				}
			}
		}

		return Response::withMimeType(json_encode($result), 'application/json');
	}

	/**
	 * get data for default view
	 *
	 * @param PDO $db
	 * @return array
	 */
	private function getDefaultViewData(PDO $db) {
		$data = array();

		if (MatchManager::existsUnsavedMatch($db)) {
			$matches = MatchManager::getUnsavedMatches($db);
			$matchHashes = array();

			foreach ($matches as $mKey => $match) {
				$matchModel = new MatchModel($match);
				$matchHashes[$mKey] = $matchModel->toHash();
				$matchHashes[$mKey]['hasPlayerParticipated'] = $this->hasPermission($db, $match);
			}

			$matchCount = count($matchHashes);

			$data = array(
				'function' => 'showUnsaved',
				'currentMatch' => array_splice($matchHashes, 0, 1),
				'matches' => $matchHashes
			);
		} else {
			$data = array(
				'function' => 'showUnsaved',
				'currentMatch' => null,
				'matches' => null
			);
		}

		$activeTypes = MatchModel::getActiveMatchTypes();

		if (isset($activeTypes[MatchModel::TYPE_1ON1])) {
			$playerLimit = 2;
		} else if (isset($activeTypes[MatchModel::TYPE_2ON2])) {
			$playerLimit = 4;
		}

		$data['matchQueueLimit'] = (int) SettingsManager::getGroup('match')->getValue('matchQueueLimit');
		$data['matchCount'] = $matchCount;

		return $data;
	}

}
