<?php

/**
 * PackGyver - Home Controller
 */
class HomeController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Home::';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Dashboard');

		$cacheKey = self::CACHE_DEFAULT . AuthenticationManager::getInstance()->getPlayer()->getId();

		if (Cache::getCache()->exists($cacheKey)) {
			return new Response(Cache::getCache()->get($cacheKey));
		}

		$cache = $this->render('home', $this->getData());
		Cache::getCache()->set($cacheKey, $cache);

		return new Response($cache);
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionMe() {
		$this->setMetaTitle('Dashboard');

		$cacheKey = self::CACHE_DEFAULT . AuthenticationManager::getInstance()->getPlayer()->getId();

		if (Cache::getCache()->exists($cacheKey)) {
			return new Response(Cache::getCache()->get($cacheKey));
		}

		$data = $this->getData();
		$data['function'] = 'me';

		$cache = $this->render('home', $data);
		Cache::getCache()->set($cacheKey, $cache);

		return new Response($cache);
	}

	/**
	 *
	 * @return array
	 */
	private function getData() {
		$db = PG::getDB();
		$data = array();
		$player = AuthenticationManager::getInstance()->getPlayer();
		$ranking = new RankingManager(PlayerCollection::create(true, null, false));
		$playerModel = new PlayerModel($player);
		$playerHash = $playerModel->toHash(true);

		$data['recentMatches'] = MatchManager::getRecentMatchesHashesForPlayer($db, $player, 3);
		$data['tableSequence'] = null;

		$playerMatchesSeason = $playerModel->getStats(PlayerStatistics::CACHETYPE_SEASON)->getCountMatches();

		if ($playerMatchesSeason >= 0 && $playerModel->isActive() && !$playerModel->isDeleted()) {
			$data['tableSequence'] = $ranking->getRankingSequence($playerModel)->toHash();
		}

		$pointTrend = $playerHash['stats']['season']['pointTrend'];
		if (count($pointTrend) > 15) {
			$pointTrend = array_slice($pointTrend, count($pointTrend) - 15);
		}
		$playerHash['stats']['season']['pointTrend'] = json_encode($pointTrend);

		$data['playerHash'] = $playerHash;
		$data['seasonRank'] = $ranking->getRankForPlayerId($player->getId());
		$data['excludeEnemyRandom'] = rand(0, 3);

		return $data;
	}

}
