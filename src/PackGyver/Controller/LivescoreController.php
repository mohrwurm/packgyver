<?php

/**
 * PackGyver - Live Score
 */
class LivescoreController extends Controller {

	/**
	 * 
	 * @return \Response
	 */
	public function actionDefault() {
		$db = PG::getDB();
		$data = array();
		$match = MatchManager::getUnsavedMatches($db, 1);

		if ($match[0] instanceof MatchEntity) {
			$matchModel = new MatchModel($match[0]);
			$data['match'] = $matchModel->toHash();
		}

		return Response::withMimeType($this->render('livescore/live', $data), 'text/html');
	}

}
