<?php

/**
 * PackGyver - Player Controller
 */
class PlayerController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Player::%d::%s';
	const SECTION_SETUP = 'setup';
	const SECTION_SEASONS = 'seasons';
	const SECTION_STATS = 'stats';
	const SECTION_MATCHES = 'matches';
	const SECTION_AWARDS = 'awards';
	const SECTION_COMPARE = 'compare';
	const SECTION_PROFILE = 'profile';

	/**
	 * 
	 * @return Response
	 */
	public function actionDefault() {
		if ($this->getMethod()) {
			$data = array();
			$data['function'] = 'profile';

			$db = PG::getDB();
			$player = PlayerModel::getPlayerByUrlIdentifier($db, htmlspecialchars($this->getMethod()));

			if ($player instanceof PlayerEntity) {
				$playerModel = new PlayerModel($player);
				$data['playerHash'] = $playerModel->toHash(true);

				$this->setMetaTitle($playerModel->getFirstnamePersonal() . ' Profil');

				$section = trim($this->getParameter(0));
				$cacheKey = sprintf(self::CACHE_DEFAULT, $player->getId(), $section);

				switch ($section) {
					case self::SECTION_SETUP:
						//sth
						break;
					case self::SECTION_SEASONS:

						if (Cache::getCache()->exists($cacheKey)) {
							$data = array_merge($data, unserialize(Cache::getCache()->get($cacheKey)));
						} else {
							$seasonsHash = SeasonArchive::getSeasonsForPlayer($playerModel, true);
							$seasons = array();

							foreach ($seasonsHash as $year => $season) {
								$seasons[$year] = array();

								foreach ($season as $key => $seas) {
									$archive = new SeasonArchive();
									$seasonPlayers = $archive->getPlayerCollectionForSeason($seas['month'], $seas['year']);
									$seasonPlayer = $seasonPlayers->getPlayerById($player->getId());

									if ($seasonPlayer instanceof PlayerModel) {
										$seasons[$year][$key]['year'] = $seas['year'];
										$seasons[$year][$key]['month'] = $seas['month'];
										$seasons[$year][$key]['monthName'] = $seas['monthName'];

										$rankingManager = new RankingManager($seasonPlayers);

										$seasons[$year][$key]['players'] = $rankingManager->getRankingSequence($seasonPlayers->getPlayerById($player->getId()))->toHash();
									}
								}
							}

							$data['seasons'] = $seasons;

							if (is_numeric($this->getParameter(1)) && is_numeric($this->getParameter(2))) {
								$seasonArchive = new SeasonArchive();
								$month = (int) $this->getParameter(2);
								$year = (int) $this->getParameter(1);

								$tplData = array();
								$seasonStats = $seasonArchive->getSeasonForPlayer($playerModel, $month, $year);

								if ($seasonStats instanceof PlayerStatistics) {
									$tplData['playerHash']['stats']['season'] = $seasonStats->toHash();

									$data['season'] = SeasonManager::getSeasonDates($month, $year);
									$data['seasonStats'] = $this->render('profile/tabs/statsSeason', $tplData);
								} else {
									$data['seasonStats'] = null;
								}
							}

							Cache::getCache()->set($cacheKey, serialize($data));
						}

						break;
					case self::SECTION_STATS:
						if ($this->getParameter(1) == 'alltime') {
							$data['statsType'] = 'alltime';
						} else {
							$data['statsType'] = 'season';
						}
						break;
					case self::SECTION_MATCHES:
						if (Cache::getCache()->exists($cacheKey)) {
							$data['recentMatches'] = unserialize($cacheKey);
						} else {
							$data['recentMatches'] = MatchManager::getRecentMatchesHashesForPlayer($db, $player, 6);
							Cache::getCache()->set($cacheKey, serialize($data['recentMatches']));
						}

						break;
					case self::SECTION_AWARDS:
						if (Cache::getCache()->exists($cacheKey)) {
							$data['playerAwards'] = unserialize($cacheKey);
						} else {
							$playerAwards = AwardManager::getAwardsForPlayer(PG::getDB(), $player, true);

							if (count($playerAwards) > 2) {
								shuffle($playerAwards);
							}
							$data['playerAwards'] = $playerAwards;
						}

						break;
					case self::SECTION_COMPARE:
						$type = ($this->getParameter(1) == 'alltime' ? 'alltime' : 'season');
						$data['type'] = $type;
						$toComparePlayer = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());
						$toComparePlayerHash = null;

						if ($toComparePlayer->getId() == $playerModel->getId()) {
							if ($type == 'alltime') {
								$data['error'] = 'Was willst du denn bitte in der Alltime-Statistik vergleichen? Bist du dooooooof?';
							} else {
								$seasonArchive = new SeasonArchive();
								$playerSeasons = $seasonArchive->getSeasonsForPlayer($toComparePlayer);

								$playerSeasonHash = array();

								foreach ($playerSeasons as $playerSeason) {
									$playerSeasonHash[$playerSeason['year']][] = $playerSeason;
								}

								$data['playerSeasons'] = $playerSeasonHash;
								$data['selfCompare'] = true;

								if (count($playerSeasons) > 0) {
									$year = (int) $this->getParameter(1);
									$month = (int) $this->getParameter(2);
									$playerStats = $seasonArchive->getSeasonForPlayer($toComparePlayer, $month, $year);

									if ($playerStats instanceof PlayerStatistics) {
										$toComparePlayer->setStats($playerStats, PlayerStatistics::CACHETYPE_SEASON);
										$toComparePlayerHash = $toComparePlayer->toHash(true);

										$data['seasonMonth'] = DateUtil::getMonthNameForMonth($month);
										$data['seasonYear'] = $year;
									} else {
										$sai = count($playerSeasons) - 1;
										$seasonArchive->getSeasonForPlayer($toComparePlayer, $playerSeasons[$sai]['month'], $playerSeasons[$sai]['year']);
										$playerStats = $seasonArchive->getSeasonForPlayer($toComparePlayer, $playerSeasons[$sai]['month'], $playerSeasons[$sai]['year']);

										if ($playerStats instanceof PlayerStatistics) {
											$toComparePlayer->setStats($playerStats, PlayerStatistics::CACHETYPE_SEASON);
											$toComparePlayerHash = $toComparePlayer->toHash(true);

											$data['seasonMonth'] = DateUtil::getMonthNameForMonth($playerSeasons[$sai]['month']);
											$data['seasonYear'] = $playerSeasons[$sai]['year'];
										}
									}
								}
							}
						} else {
							$toComparePlayerHash = $toComparePlayer->toHash(true);
						}

						if (null !== $toComparePlayerHash) {
							if ($type == 'season') {
								$pointTrend = $toComparePlayerHash['stats']['season']['pointTrend'];

								if (count($pointTrend) > 15) {
									$pointTrend = array_slice($pointTrend, count($pointTrend) - 15);
								}
								$toComparePlayerHash['stats']['season']['pointTrend'] = json_encode($pointTrend);

								$pointTrend = $data['playerHash']['stats']['season']['pointTrend'];

								if (count($pointTrend) > 15) {
									$pointTrend = array_slice($pointTrend, count($pointTrend) - 15);
								}
								$data['playerHash']['stats']['season']['pointTrend'] = json_encode($pointTrend);
							}

							$data['toComparePlayer'] = $toComparePlayerHash;
						} else {
							if (!$data['error']) {
								$data['error'] = 'Keine Saison verfügbar.';
							}
						}

						break;
					default:
						$section = self::SECTION_PROFILE;
						$cacheKey = sprintf(self::CACHE_DEFAULT, $player->getId(), $section);

						if (Cache::getCache()->exists($cacheKey)) {
							$data = array_merge($data, unserialize(Cache::getCache()->get($cacheKey)));
						} else {
							$pointTrend = $data['playerHash']['stats']['season']['pointTrend'];

							if (count($pointTrend) > 15) {
								$pointTrend = array_slice($pointTrend, count($pointTrend) - 15);
							}
							$data['playerHash']['stats']['season']['pointTrend'] = json_encode($pointTrend);

							$playerAwards = AwardManager::getAwardsForPlayer(PG::getDB(), $player, true);

							if (count($playerAwards) > 2) {
								shuffle($playerAwards);
							}

							$data['playerAwards'] = $playerAwards;
							$data['recentMatches'] = MatchManager::getRecentMatchesHashesForPlayer($db, $player, 3);
							$data['tableSequence'] = null;

							$playerMatchesSeason = $playerModel->getStats(PlayerStatistics::CACHETYPE_SEASON)->getCountMatches();

							if ($playerMatchesSeason >= 0 && $playerModel->isActive() && !$playerModel->isDeleted()) {
								$ranking = new RankingManager(PlayerCollection::create(true, null, false));
								$data['seasonRank'] = $ranking->getRankForPlayerId($player->getId());
								$data['tableSequence'] = $ranking->getRankingSequence($playerModel)->toHash();
							}

							$gamesPerDay = 0;
							$dates = SeasonManager::getCurrentSeasonDates();
							$matchCount = $playerModel->getStats(PlayerStatistics::CACHETYPE_SEASON)->getCountMatches();

							if ($matchCount > 0) {
								$gamesPerDay = $matchCount / round(DateUtil::getWorkingDays($dates['_start'], DateUtil::format()));
							}

							$data['gamesPerDay'] = round($gamesPerDay, 1);

							Cache::getCache()->set($cacheKey, serialize($data));
						}
				}

				$data['section'] = $section;
				$data['isAuthorized'] = PlayerModel::isPlayerAuthorized($player->getId());

				$dateRegistered = DateUtil::formatFromDB($player->getDateRegistered(), true);

				$data['registeredSince'] = DateUtil::getMonthNameForMonth(date('n', $dateRegistered)) . ' \'' . substr(date('Y', $dateRegistered), 2);
			}

			return new Response($this->render('players', $data));
		} else {
			$this->redirect('/ranking');
		}
	}

}
