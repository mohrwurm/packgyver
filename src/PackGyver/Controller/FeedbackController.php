<?php

/**
 * PackGyver - Feedback Controller
 */
class FeedbackController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		return $this->actionSave();
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionSave() {
		$result = array('success' => false);
		$message = (isset($_POST['message']) && trim($_POST['message']) != '') ? htmlspecialchars("\n\n" . trim($_POST['message'])) : null;
		$type = (isset($_POST['type']) && trim($_POST['type'])) ? htmlspecialchars(trim($_POST['type'])) : null;
		$browser = (isset($_POST['browser']) && trim($_POST['browser'])) ? htmlspecialchars(trim($_POST['browser'])) : null;

		$player = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());

		if (false && null !== $message && null !== $type) {
			mail(
					'xxx@mail.com', 'PackGyver - Feedback', $player->getFirstname()
					. ' ' .
					$player->getLastname()
					. ' - '
					. $type
					. ' - ' . $message
					. ' - '
					. $browser);

			$result['success'] = true;
		}

		return Response::withMimeType(json_encode($result));
	}

}
