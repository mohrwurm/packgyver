<?php

/**
 * PackGyver - Statistics Controller
 */
class StatsController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Stats::Default';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Gesamtstatistiken');

		if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}

		$db = PG::getDB();
		$stats = array();

		$stats['season'] = StatisticsManager::getStatsSet($db, date('n'), date('Y'));
		$stats['season']['chartsEnabled'] = ($stats['season']['matchCount'] > 0);

		$stats['alltime'] = StatisticsManager::getStatsSet($db);
		$stats['alltime']['since'] = DateUtil::format(strtotime(PG::getRecordStart()), PG::DEFAULT_DATEFORMAT);
		$stats['alltime']['chartsEnabled'] = ($stats['alltime']['matchCount'] > 0);

		$cache = $this->render('statistics', array('stats' => $stats));
		Cache::getCache()->set(self::CACHE_DEFAULT, $cache);

		return new Response($cache);
	}

}
