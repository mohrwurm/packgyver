<?php

/**
 * PackGyver - Cron Controller
 */
class CronController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$data = array(
			'success' => false
		);

		return Response::withMimeType(json_encode($data), 'application/json');
	}

	/**
	 * 
	 * @return \Response
	 */
	public function actionPlayers() {
		$data = array(
			'success' => false
		);

		if ($this->isCronActive()) {
			if ($this->getParameter(0) == 'check') {
				if ('out' === $this->getParameter(1)) {
					$sql = 'UPDATE `player` SET checkedIn = 0';
					PG::getDB()->query($sql);
					$data['success'] = true;
				}
			}
		}

		return Response::withMimeType(json_encode($data), 'application/json');
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionStatistics() {
		$data = array(
			'success' => false
		);

		if ($this->isCronActive()) {
			if ($this->getParameter(0) == 'rebuild') {
				if ($this->getParameter(1) == 'alltime') {
					AdminManager::clearCache(AdminManager::CACHE_TYPE_PLAYERSTATS_ALLTIME);

					foreach (PlayerModel::getPlayers(PG::getDB(), null, null, false) as $player) {
						$model = new PlayerModel($player);
						$model->getStats(PlayerStatistics::CACHETYPE_ALLTIME)->recalculate();
					}

					$data['success'] = true;
				}
			}
		}

		return Response::withMimeType(json_encode($data), 'application/json');
	}

	/**
	 * check if crons are enabled in settings
	 *
	 * @return bool
	 */
	private function isCronActive() {
		$group = SettingsManager::getGroup('cron');
		$value = $group->getValue('isCronEnabled');

		if (null === $value) {
			$group->setValue('isCronEnabled', 1);
			$value = 1;
		}

		return 0 !== $value;
	}

}
