<?php

/**
 * PackGyver - Players Controller
 */
class PlayersController extends Controller {

	/**
	 * 
	 * @return Response
	 */
	public function actionDefault() {
		$this->redirect('/ranking');
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionStatus() {
		if ($this->getRequest()->isAjax() && ($this->getParameter(0) == 'active' || $this->getParameter(0) == 'check')) {
			$type = ($this->getParameter(0) == 'active') ? 'active' : 'check';

			if ($this->getParameter(1) == 'in' || $this->getParameter(1) == 'out') {
				$db = PG::getDB();

				if (!isset($_POST['playerId']) || '' == $_POST['playerId']
						|| isset($_POST['playerId']) && AuthenticationManager::getInstance()->getPlayer()->getId() == (int) $_POST['playerId']) {

					$player = AuthenticationManager::getInstance()->getPlayer();
				} else {
					$player = PlayerEntity::findById($db, (int) $_POST['playerId']);
				}

				if ($player instanceof PlayerEntity
						&& PlayerModel::isPlayerAuthorized($player->getId())) {

					if ($type == 'check') {
						$player->setCheckedIn($this->getParameter(1) == 'in' ? true : false);
					} else {
						$player->setActive($this->getParameter(1) == 'in' ? true : false);
					}
					$player->updateToDatabase($db);

					$wsservice = WebSocketServiceManager::getInstance();

					if ($type == 'check' && $wsservice->isEnabled()) {
						$wsservice->getDataDispatcher()->triggerCheckedPlayersEvent();
					}

					return Response::withMimeType(json_encode(array('success' => true)));
				}
			}
		}

		return Response::withMimeType(json_encode(array('success' => false)));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionChangepassword() {
		$result = array(
			'success' => false,
			'redirect' => false
		);

		if ($this->getRequest()->isAjax() && isset($_POST['playerId'])) {
			$player = PlayerEntity::findById(PG::getDB(), (int) $_POST['playerId']);

			if ($player instanceof PlayerEntity) {
				$oldPassword = trim($_POST['oldPassword']);
				$newPassword = trim($_POST['newPassword']);

				if ('' != $newPassword) {
					if (PlayerModel::changePassword($player, $oldPassword, $newPassword)) {
						$result['success'] = true;

						if ($player->getId() == AuthenticationManager::getInstance()->getPlayer()->getId()) {
							AuthenticationManager::getInstance()->logout();
							$result['redirect'] = true;
						}
					}
				}
			}
		}

		return Response::withMimeType(json_encode($result));
	}

	/**
	 *
	 * @return \Response
	 */
	public function actionHandleavatar() {
		return Response::withMimeType(json_encode(PlayerModel::handleAvatarUpload()));
	}

}
