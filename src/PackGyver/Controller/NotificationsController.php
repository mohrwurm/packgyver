<?php

/**
 * PackGyver - Notifications
 */
class NotificationsController extends Controller {

	public function actionDefault() {
		$this->redirect('/');
	}

	/**
	 * 
	 * @return Response
	 */
	public function actionPull() {
		$player = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());
		$clearAfter = (isset($_POST['cleanUp']) && $_POST['cleanUp'] === 'true');

		$result = array(
			'newCount' => 0,
			'notifications' => ''
		);

		$unseen = 0;

		if ($player->getNotifications()->count() > 0) {
			foreach ($player->getNotifications() as $notification) {
				/* @var $notification Notification */
				$result['notifications'] .= $this->render('inc/notificationEntry', array('notification' => $notification->toHash()));

				if (true === $clearAfter) {
					$notification->delete();
				}

				$unseen++;
			}
		}

		$result['newCount'] = $unseen;

		return Response::withMimeType(json_encode($result), 'application/json');
	}

	public function actionClear() {
		$player = new PlayerModel(AuthenticationManager::getInstance()->getPlayer());

		foreach ($player->getNotifications() as $notification) {
			/* @var $notification Notification */
			$notification->delete();
		}

		return Response::withMimeType(null, 'text/plain');
	}

	/**
	 * 
	 * @return \Response
	 */
	public function actionDispose() {
		$result = array(
			'success' => false
		);

		$notificationEntity = NotificationEntity::findById(PG::getDB(), (int) $_POST['id']);

		if ($notificationEntity instanceof NotificationEntity) {
			$notification = new Notification();
			$notification->assignByEntity($notificationEntity);

			if ($notification->getPlayer()->getId() == AuthenticationManager::getInstance()->getPlayer()->getId()) {
				$notification->delete();
				$result['success'] = true;
			}
		}

		return Response::withMimeType(json_encode($result), 'application/json');
	}

}
