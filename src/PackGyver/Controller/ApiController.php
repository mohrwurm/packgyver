<?php

/**
 * PackGyver - Api Controller
 */
class ApiController extends Controller {

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		exit('dont. just dont.');
		$api = new ApiManager(new ApiAccess($_GET['oauth_consumer_key']));
		$error = false;
		$result = null;

		try {
			if ($api->isValid()) {
				$result = $api->handle($this->getMethod(), $this->getParameters());
			} else {
				throw new ApiException('Invalid Request');
			}
		} catch (Exception $e) {
			$result = $api->dieCrying($e);
			$error = true;
		}

		$result = $api->render($result, $error);

		return Response::withMimeType($result, 'application/json');
	}

}
