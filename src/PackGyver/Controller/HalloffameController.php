<?php

/**
 * PackGyver - Ranking Controller
 */
class HalloffameController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Halloffame::Default';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Hall Of Fame');

		if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}

		$data = array();

		$alltimeRanking = array();
		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			$alltimeRanking[] = array(
				'player' => $playerHash,
				'seasonCount' => count(SeasonArchive::getSeasonsForPlayer($playerModel)),
				'points' => $playerHash['stats']['alltime']['points']
			);
		}

		$sorter = new SortUtil();
		$data['alltimeRanking'] = array_values($sorter->sort($alltimeRanking, 'points'));

		$cache = $this->render('hof', array('data' => $data));
		Cache::getCache()->set(self::CACHE_DEFAULT, $cache, 86400); // 24 hours cached

		return new Response($cache);
	}

}
