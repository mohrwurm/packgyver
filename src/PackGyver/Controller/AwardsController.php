<?php

/**
 * PackGyver - Awards Controller
 */
class AwardsController extends Controller {

	const CACHE_DEFAULT = 'PG::Cache::Controller::Awards::Default';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Awards');

		if (Cache::getCache()->exists(self::CACHE_DEFAULT)) {
			return new Response(Cache::getCache()->get(self::CACHE_DEFAULT));
		}

		$data = array();
		$db = PG::getDB();

		$awards = AwardManager::getAwards($db);
		$data['pAwards'] = array();

		foreach ($awards as $award) {
			if ($award instanceof AwardEntity) {
				$data['pAwards'][$award->getId()] = AwardManager::getPlayerHashesForAward($db, $award);
			}
		}
		$data['awards'] = AwardManager::getAwardHashes($db);

		$cache = $this->render('awards', $data);
		Cache::getCache()->set(self::CACHE_DEFAULT, $cache);

		return new Response($cache);
	}

}
