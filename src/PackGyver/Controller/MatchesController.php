<?php

/**
 * PackGyver - Matches Controller
 */
class MatchesController extends Controller {

	const MATCH_LOAD_STEP = 8;
	const CACHE_MONSTER_PACKS = 'PG::Cache::Controller::Matches::MonsterPacks';

	/**
	 *
	 * @return \Response
	 */
	public function actionDefault() {
		$this->setMetaTitle('Spiele');

		$db = PG::getDB();
		$matchHashes = array();
        $seasonDates = SeasonManager::getCurrentSeasonDates();
        $filtered = $this->getRequest()->getParamInt('filter', Request::TYPE_GET) === 1;

		$filter=MatchFilterFactory::createFromRequest($this->getRequest());
		$filter->setDeleted(false);
		$filter->setUnsaved(false);

        if (!$filtered) {
            $filter->setDateSavedFrom(new DateTime($seasonDates['_start']));
            $filter->setDateSavedTo(new DateTime($seasonDates['_end']));
        }

        $p=new Pagination();
        $p->records_per_page(6);

        $filter->setLimit(array(($p->get_page() - 1) * $p->get_records_per_page(), $p->get_records_per_page()));

        $matches=$db->query($filter->buildSqlQuery())->fetchAll();

        $p->records((int) $db->query('SELECT FOUND_ROWS() AS rows')->fetchColumn());

        $page=$this->getRequest()->getParamInt('page', Request::TYPE_GET);
        $p->set_page($page);
        $p->selectable_pages(5);

		foreach ($matches as $mKey=>$match) {
            $matchEntity=new MatchEntity();
            $matchEntity->assignByHash($match);
			$matchModel=new MatchModel($matchEntity);

			$matchHashes[$mKey] = $matchModel->toHash();
			$matchHashes[$mKey]['dateOnly'] = DateUtil::format(strtotime($matchModel->getDateSaved()), PG::DEFAULT_DATEFORMAT);
		}

		$data = array(
			'function' => 'all',
            'pagination' => $p->render(true),
            'results' => $p->get_records(),
			'filter' => $filter->toHash(),
			'matches' => $matchHashes,
			'season' => $seasonDates
		);

		return new Response($this->render('matches/matches', $data));
	}

	/**
	 * @return Response
	 */
	public function actionMonsterPacks() {
		if (Cache::getCache()->exists(self::CACHE_MONSTER_PACKS)) {
			return new Response(Cache::getCache()->get(self::CACHE_MONSTER_PACKS));
		}

		$sql = 'SELECT * FROM `match`'
			. ' WHERE ((`resultTeamRed` = 10 && `resultTeamYellow` = 0) OR (`resultTeamRed` = 0 && `resultTeamYellow` = 10)) AND deleted = 0'
			. ' ORDER BY dateSaved DESC';

		$db = PG::getDB();
		$hash = array();
		$matches = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

		foreach ($matches as $match) {
			$entity = new MatchEntity();
			$entity->assignByHash($match);
			$match = new MatchModel($entity);
			$hash[] = $match->toHash();
		}

		$cache = $this->render('matches/monsterPacks', array('matches' => $hash));

		Cache::getCache()->set(self::CACHE_MONSTER_PACKS, $cache, 3600);

		return new Response($cache);
	}

}
