<?php

/**
 * Description of AuthTokenManager
 */
class AuthTokenManager {

	/**
	 * 
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param string $provider
	 * @param string $key
	 * @param string $token
	 * @return \AuthTokenEntity
	 */
	public static function addToken(PDO $db, PlayerEntity $player, $provider, $key, $token) {
		$entity = new AuthTokenEntity();
		$entity->setProvider($provider)
				->setPlayerId($player->getId())
				->setKey($key);

		$existing = AuthTokenEntity::findByExample($db, $entity);

		if (!empty($existing)) {
			$existing = $existing[0];

			if ($existing->getToken() != $token) {
				$existing->setToken($token);
				$existing->updateToDatabase($db);
			}

			return $existing;
		}

		$entity->setToken($token)
				->insertIntoDatabase($db);

		return $entity;
	}

	/**
	 * 
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param string $provider
	 * @param string $key
	 * @return \AuthTokenEntity|null
	 */
	public static function getTokenForPlayer(PDO $db, PlayerEntity $player, $provider, $key) {
		$entities = AuthTokenEntity::findByFilter($db, array(
					new DFC(AuthTokenEntity::FIELD_KEY, $key, DFC::EXACT),
					new DFC(AuthTokenEntity::FIELD_PLAYERID, $player->getId(), DFC::EXACT),
					new DFC(AuthTokenEntity::FIELD_PROVIDER, $provider, DFC::EXACT)
				));

		if (!empty($entities)) {
			return $entities[0];
		}

		return null;
	}

}
