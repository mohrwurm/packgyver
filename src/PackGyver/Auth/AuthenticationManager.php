<?php

/**
 * PackGyver - Authentication Manager
 *
 * @package auth
 */
class AuthenticationManager {
	/* types */

	const TYPE_USER = 0;
	const TYPE_ADMIN = 1;

	/**
	 * @var AuthenticationManager
	 */
	private static $instance;

	/**
	 * @var AuthenticationProviderInterface[]
	 */
	private static $providers = array();

	/**
	 * store user session
	 *
	 * @var Session
	 */
	private $session;

	/**
	 * @var string
	 */
	private $error;

	/**
	 * @return AuthenticationManager
	 */
	public static function getInstance() {
		if (null === self::$instance) {
			self::$instance = new AuthenticationManager();

			if (self::$instance->getPlayer() instanceof PlayerEntity) {
				$player = PlayerEntity::findById(PG::getDB(), self::$instance->getPlayer()->getId());

				if ($player instanceof PlayerEntity) {
					self::$instance->setPlayer($player);
				} else {
					self::$instance->logout();
				}
			}
		}

		return self::$instance;
	}

	/**
	 * CTOR
	 */
	public function __construct() {
		$this->session = Session::instance();
	}

	/**
	 *
	 * @return string
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 *
	 * @return bool
	 */
	public function isLoggedIn() {
		foreach (self::getProviders() as $provider) {
			/* @var $provider AuthenticationProviderAbstract */
			if ($provider->isAuthenticated()) {
				return true;
			} else {
				if ($provider->getError()) {
					$this->error = $provider->getError();
				}
			}
		}

		return false;
	}

	/**
	 * Logout from every AuthenticationProvider
	 */
	public function logout() {
		foreach (self::getProviders() as $provider) {
			/* @var $provider AuthenticationProviderAbstract */
			$provider->logout();
		}
	}

	/**
	 *
	 * @param AuthenticationManager $authManager
	 */
	public static function setInstance(AuthenticationManager $authManager) {
		self::$instance = $authManager;
	}

	/**
	 *
	 * @param string $providerId
	 * @return AuthenticationProviderInterface|null
	 */
	public static function getAuthenticationProviderForId($providerId) {
		if (isset(self::$providers[$providerId])) {
			return self::$providers[$providerId];
		}

		return null;
	}

	/**
	 *
	 * @return AuthenticationProviderInterface[]
	 */
	public static function getProviders() {
		return self::$providers;
	}

	/**
	 *
	 * @param AuthenticationProviderInterface $provider
	 */
	public static function registerProvider(AuthenticationProviderInterface $provider) {
		if (!isset(self::$providers[$provider->getId()])) {
			self::$providers[$provider->getId()] = $provider;
		}
	}

	/**
	 *
	 * @param AuthenticationProviderInterface $provider
	 */
	public function removeProvider(AuthenticationProviderInterface $provider) {
		if (!isset(self::$providers[$provider->getId()])) {
			unset(self::$providers[$provider->getId()]);
		}
	}

	/**
	 * Unset the current logged in player
	 */
	public function unsetPlayer() {
		$_SESSION['PackGyver::Player'] = null;
	}

	/**
	 * @param PlayerEntity $player
	 */
	public function setPlayer(PlayerEntity $player) {
		$_SESSION['PackGyver::Player'] = $player;
	}

	/**
	 * @return PlayerEntity
	 */
	public function getPlayer() {
		if (isset($_SESSION['PackGyver::Player']) && $_SESSION['PackGyver::Player'] instanceof PlayerEntity) {
			return $_SESSION['PackGyver::Player'];
		}

		return null;
	}

}
