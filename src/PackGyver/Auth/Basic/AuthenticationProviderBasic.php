<?php

/**
 * PackGyver - Authentication Provider Basic
 *
 * @package auth
 */
class AuthenticationProviderBasic extends AuthenticationProviderAbstract {
	/* cookie */

	const COOKIE_NAME = "__packgyver";
	const COOKIE_SEPARATOR = "::";
	const COOKIE_EXPIRATION = 1209600; // 2 weeks

	/**
	 * 
	 * @return string
	 */

	public function getId() {
		return 'basic';
	}

	/**
	 * 
	 * @return bool
	 */
	public function isAuthenticated() {
		if (!(AuthenticationManager::getInstance()->getPlayer() instanceof PlayerEntity)) {
			$this->loginFromCookie();
		}

		return (AuthenticationManager::getInstance()->getPlayer() instanceof PlayerEntity);
	}

	/**
	 * 
	 * @return bool
	 */
	public function authenticate() {
		if (!$this->getSettings()->getValue('isLoginEnabled')) {
			$this->setError($this->availableErrors[self::ERROR_LOGIN_DISABLED]);
			return false;
		}

		$email = ValidationUtil::getInput($_POST['panelEmail']);
		$password = isset($_POST['panelPassword']) ? trim($_POST['panelPassword']) : null;

		if (ValidationUtil::validateEmail($email) && strlen($password) >= 4) {
			$this->credentials['email'] = $email;

			$hashedPw = self::hash($email, $password);
			$loginCheck = PlayerEntity::findByFilter(PG::getDB(), array(
						new DFC(PlayerEntity::FIELD_EMAIL, $email, DFC::EXACT),
						new DFC(PlayerEntity::FIELD_PASSWORD, $hashedPw, DFC::EXACT)
					));

			if ($loginCheck[0] instanceof PlayerEntity) {
				$player = $loginCheck[0];

				if ($player->getDeleted() == 1) { // && $player->getId() != 1
					$this->setError($this->availableErrors[self::ERROR_DELETED]);
					return false;
				}

				if (CookieUtil::write(self::COOKIE_NAME, base64_encode($this->getCookieHash($email, $hashedPw)), self::COOKIE_EXPIRATION)) {
					$player->setLastActivity(DateUtil::formatForDB());
					$player->updateToDatabase(PG::getDB());

					AuthenticationManager::getInstance()->setPlayer($player);
					UrlUtil::redirect('/');

					return true;
				}
			} else {
				$this->setError($this->availableErrors[self::ERROR_WRONGDATA]);
			}
		} else {
			if (!ValidationUtil::validateEmail($email)) {
				$this->setError($this->availableErrors[self::ERROR_INVALID_MAIL]);
				return false;
			}

			$this->setError($this->availableErrors[self::ERROR_NODATA]);
		}

		return false;
	}

	/**
	 * 
	 * @return bool
	 */
	public function logout() {
		if (CookieUtil::read(self::COOKIE_NAME)) {
			Session::instance()->destroy();
			CookieUtil::kill(self::COOKIE_NAME);

			return true;
		}

		return false;
	}

	/**
	 * 
	 * @return bool
	 */
	private function loginFromCookie() {
		if (AuthenticationManager::getInstance()->getPlayer() instanceof PlayerEntity) {
			return true;
		}

		$cookieData = CookieUtil::read(self::COOKIE_NAME);

		if (null !== $cookieData) {
			$cookieData = explode(self::COOKIE_SEPARATOR, base64_decode($cookieData));

			if (time() <= intval($cookieData[3])) {
				$email = htmlentities(trim($cookieData[0]));
				$password = htmlentities(trim($cookieData[1]));

				if (ValidationUtil::validateEmail($email) && strlen($password) >= 4) {
					$loginCheck = PlayerEntity::findByFilter(PG::getDB(), array(
								new DFC(PlayerEntity::FIELD_EMAIL, $email, DFC::EXACT),
								new DFC(PlayerEntity::FIELD_PASSWORD, $password, DFC::EXACT)
							));

					if ($loginCheck[0] instanceof PlayerEntity) {
						$player = $loginCheck[0];

						if ($player->getDeleted() != 1) { // && $player->getId() != 1
							if (CookieUtil::write(self::COOKIE_NAME, base64_encode($this->getCookieHash($email, $password)), self::COOKIE_EXPIRATION)) {
								$player->setLastActivity(DateUtil::formatForDB());
								$player->updateToDatabase(PG::getDB());

								AuthenticationManager::getInstance()->setPlayer($player);

								return true;
							}
						}
					}
				}
			}

			CookieUtil::kill(self::COOKIE_NAME);
		}

		return false;
	}

	/**
	 * create cookie string
	 * e.g. "test@mail.com::sha1_key::session_id::expire_timestamp"
	 *
	 * @return string
	 */
	private function getCookieHash($email, $password) {
		$expires = time() + self::COOKIE_EXPIRATION;
		return $email . self::COOKIE_SEPARATOR . $password . self::COOKIE_SEPARATOR . Session::instance()->getId() . self::COOKIE_SEPARATOR . $expires;
	}

	/**
	 *
	 * @return string
	 */
	public function getRandomString() {
		$length = 22;
		$characters = '0a1b2c3d4e5f6g7h8i9jklmnopqrstuvwxyz';
		$string = '';

		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters))];
		}

		return $string;
	}

	/**
	 * hash password
	 * 
	 * @param string $email
	 * @param string $password
	 * @throws PackGyverException
	 */
	public static function hash($email, $pass) {
		$password = hash('sha256', $pass);
		$salt = md5(substr($email, 0, strpos($email, '@')));
		$sM = round(strlen($email) / 2) - 1;
		$h1 = substr($password, 0, $sM);
		$h2 = substr($password, $sM);

		return md5($h1 . $salt . $h2);
	}

    /**
     * {@inheritdoc}
     */
    public function isEnabled() {
        return (bool) $this->getSettings()->getValue('isLoginEnabled');
    }
}
