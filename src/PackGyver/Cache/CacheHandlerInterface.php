<?php

/**
 * PackGyver - Cache Handler Interface
 *
 * @package cache
 */
interface CacheHandlerInterface {

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param int|null $ttl The time-to-live in seconds
	 * @return mixed
	 */
	public function set($key, $value, $ttl = null);

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key);

	/**
	 * @param string $key
	 * @return bool
	 */
	public function exists($key);

	/**
	 * @param string $key
	 * @return bool
	 */
	public function delete($key);

	/**
	 * @return void
	 */
	public function flush();
}
