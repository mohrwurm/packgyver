<?php

/**
 * PackGyver - Cache Manager
 *
 * @package cache
 */
class CacheManager {

	/**
	 * @param PlayerEntity $player
	 */
	public static function clearPlayerCache(PlayerEntity $player) {
		$cache = Cache::getCache();

		// player home
		$cache->delete('PG::Cache::Controller::Home::' . $player->getId());

		// player profile
		self::clearPlayerProfile($cache, $player);
	}

	/**
	 * @param Cache $cache
	 * @param PlayerEntity $player
	 * @param string|null $section
	 * @return null
	 */
	public static function clearPlayerProfile(Cache $cache, PlayerEntity $player, $section = null) {
		$profileCacheKeyPrefix = sprintf('PG::Cache::Controller::Player::%d::', $player->getId());

		if (null !== $section) {
			$cache->delete($profileCacheKeyPrefix . $section);
			return;
		}

		$cache->delete($profileCacheKeyPrefix . PlayerController::SECTION_PROFILE);
		$cache->delete($profileCacheKeyPrefix . PlayerController::SECTION_MATCHES);
		$cache->delete($profileCacheKeyPrefix . PlayerController::SECTION_AWARDS);
		$cache->delete($profileCacheKeyPrefix . PlayerController::SECTION_STATS);
	}

}
