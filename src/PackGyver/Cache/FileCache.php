<?php

/**
 * PackGyver - File Cache
 */
class FileCache extends Cache {

	const CACHE_DIR = 'FileCache';

	/**
	 * {@inheritdoc}
	 */
	public function get($key) {
		return file_get_contents($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function set($key, $value, $ttl = null) {
		return false !== file_put_contents($this->getPathForKey($key), $value);
	}

	/**
	 * {@inheritdoc}
	 */
	public function exists($key) {
		return file_exists($key);
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($key) {
		return unlink($this->getPathForKey($key));
	}

	/**
	 * {@inheritdoc}
	 */
	public function flush() {

	}

	/**
	 * @param string $key
	 * @return string
	 */
	protected function getPathForKey($key) {
		return PG::getCacheDir() . DS . self::CACHE_DIR . DS . $key;
	}

}
