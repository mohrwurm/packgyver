<?php

/**
 * PackGyver - Settings Manager
 *
 * @package settings
 */
class SettingsManager {

	/**
	 *
	 * @var string
	 */
	private $group;

	/**
	 *
	 * @var SettingsEntity[]
	 */
	private $settings;

	/**
	 *
	 * @var SettingsManager[]
	 */
	private static $instances;

	/**
	 * CTOR
	 *
	 * @param string $group
	 */
	public function __construct($group) {
		$this->group = $group;
		$this->load();
	}

	/**
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function setValue($key, $value) {
		$entity = new SettingsEntity();
		$entity->assignDefaultValues();
		$entity->setKey($key);
		$entity->setGroup($this->group);

		$example = SettingsEntity::findByExample(PG::getDB(), $entity);

		if ($example[0] instanceof SettingsEntity) {
			$example[0]->setValue($value)->updateToDatabase(PG::getDB());
			return $example[0];
		}

		$entity->setValue($value)->insertIntoDatabase(PG::getDB());
		return $entity;
	}

	/**
	 *
	 * @param string $key
	 * @param mixed $default
	 */
	public function getValue($key, $default = null) {
		if (null !== $this->settings) {
			foreach ($this->settings as $setting) {
				/* @var $setting SettingsEntity */
				if ($setting->getKey() == $key) {
					return $setting->getValue();
				}
			}
		}
		return $default;
	}

	/**
	 * delete keys in group
	 *
	 * @param string $key
	 */
	public function delete($key) {
		SettingsEntity::deleteByFilter(PG::getDB(), array(
			new DFC(SettingsEntity::FIELD_GROUP, $this->group, DFC::EXACT),
			new DFC(SettingsEntity::FIELD_KEY, $key, DFC::EXACT)
				), true);
	}

	/**
	 * load settings for group
	 */
	private function load() {
		$this->settings = SettingsEntity::findByFilter(PG::getDB(), array(
					new DFC(SettingsEntity::FIELD_GROUP, $this->group)
				));
	}

	/**
	 *
	 * @param string $group
	 * @return SettingsManager
	 * @throws PackGyverException
	 */
	public static function getGroup($group) {
		if (!isset(self::$instances[$group])) {
			self::$instances[$group] = new SettingsManager($group);
		}

		return self::$instances[$group];
	}

	/**
	 *
	 * @param PDO $db
	 * @return array
	 */
	public static function getSettingsAsHash(PDO $db) {
		$hash = array();
		$sort = array(
			new DSC(SettingsEntity::FIELD_GROUP, DSC::ASC),
			new DSC(SettingsEntity::FIELD_KEY, DSC::ASC)
		);

		foreach (SettingsEntity::findByFilter($db, array(), true, $sort) as $setting) {
			/* @var $setting SettingsEntity */
			$hash[$setting->getGroup()][] = $setting->toHash();
		}

		return $hash;
	}

}

?>