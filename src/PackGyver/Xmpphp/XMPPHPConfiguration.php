<?php

/**
 * PackGyver - XMPPHP Configuration
 */
class XMPPHPConfiguration {

	/**
	 *
	 * @var string
	 */
	private $host;

	/**
	 *
	 * @var int
	 */
	private $port;

	/**
	 *
	 * @var string
	 */
	private $user;

	/**
	 *
	 * @var string
	 */
	private $password;

	/**
	 *
	 * @var string
	 */
	private $resource;

	/**
	 *
	 * @var string
	 */
	private $server;

	/**
	 *
	 * @var bool
	 */
	private $printLog = false;

	/**
	 *
	 * @var string
	 */
	private $logLevel;

	/**
	 *
	 * @var string
	 */
	public function setHost($host) {
		$this->host = $host;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getHost() {
		return $this->host;
	}

	/**
	 *
	 * @var int
	 */
	public function setPort($port) {
		$this->port = $port;
		return $this;
	}

	/**
	 *
	 * @var int
	 */
	public function getPort() {
		return $this->port;
	}

	/**
	 *
	 * @var string
	 */
	public function setUser($user) {
		$this->user = $user;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 *
	 * @var string
	 */
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 *
	 * @var string
	 */
	public function setResource($resource) {
		$this->resource = $resource;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getResource() {
		return $this->resource;
	}

	/**
	 *
	 * @var string
	 */
	public function setServer($server) {
		$this->server = $server;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getServer() {
		return $this->server;
	}

	/**
	 *
	 * @var bool
	 */
	public function setPrintLog($printLog) {
		$this->printLog = $printLog;
		return $this;
	}

	/**
	 *
	 * @var bool
	 */
	public function getPrintLog() {
		return $this->printLog;
	}

	/**
	 *
	 * @var string
	 */
	public function setLogLevel($logLevel) {
		$this->logLevel = $logLevel;
		return $this;
	}

	/**
	 *
	 * @var string
	 */
	public function getLogLevel() {
		return $this->logLevel;
	}

}
