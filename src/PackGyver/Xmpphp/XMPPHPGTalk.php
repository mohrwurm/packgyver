<?php

/**
 * PackGyver - XMPPHP Google Talk
 */
class XMPPHPGTalk extends XMPPHPBase {
	/* connection details */

	const GT_DATA_HOST = 'talk.google.com';
	const GT_DATA_PORT = 5222;
	const GT_DATA_USER = 'xxx@gmail.com';
	const GT_DATA_PASSWORD = 'xxx';
	const GT_DATA_RESOURCE = 'xmpphp';
	const GT_DATA_SERVER = 'gmail.com';

	/* message types */
	const MESSAGETYPE_MATCH_REGISTERED = 0;
	const MESSAGETYPE_MATCH_SAVED = 1;

	public function __construct() {
		$config = new XMPPHPConfiguration();
		$config->setHost(self::GT_DATA_HOST)
				->setPort(self::GT_DATA_PORT)
				->setUser(self::GT_DATA_USER)
				->setPassword(self::GT_DATA_PASSWORD)
				->setResource(self::GT_DATA_RESOURCE)
				->setServer(self::GT_DATA_SERVER);

		parent::__construct($config);
	}

	/**
	 *
	 * @param XMPPHPGTalkMessage $message
	 */
	public function sendMessage(XMPPHPGTalkMessage $message) {
        return; // missing gtalk credentials

		if ($message->isValidToSend()) {
			$msgContent = $message->getMessage();

			foreach ($message->getRecipients() as $recipient) {
				/* @var PlayerModel $recipient */
				if (null != $recipient->getGtalk()) {
					try {
						$this->getXMPP()->message($recipient->getGtalk(), $msgContent);
					} catch (XMPPHP_Exception $e) {
						PackGyverException::logException($e);
					}
				}
			}
		}
	}

}
