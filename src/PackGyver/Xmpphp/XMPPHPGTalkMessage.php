<?php

/**
 * PackGyver - Google Talk Message Interface
 */
abstract class XMPPHPGTalkMessage {

	/**
	 *
	 * @var array
	 */
	protected $parameters;

	/**
	 *
	 * @var PlayerModel[]
	 */
	protected $recipients = array();

	/**
	 *
	 * @param array $params
	 */
	public function setParameters($params) {
		$this->parameters = $params;
	}

	/**
	 *
	 * @param PlayerModel[] $recipients
	 */
	public function setRecipients($recipients) {
		$this->recipients = $recipients;
	}

	/**
	 *
	 * @return PlayerModel[] $recipients
	 */
	public function getRecipients() {
		return $this->recipients;
	}

	/**
	 *
	 * @param PlayerModel $recipient
	 */
	public function appendRecipient(PlayerModel $recipient) {
		foreach ($this->recipients as $rRecipient) {
			/* @var PlayerModel $rRecipient */
			if ($rRecipient->getId() == $recipient->getId()) {
				return;
			}
		}
		$this->recipients[] = $recipient;
	}

	/**
	 *
	 * @return bool 
	 */
	abstract public function isValidToSend();

	/**
	 *
	 * @return string 
	 */
	abstract public function getMessage();
}
