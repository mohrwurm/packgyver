<?php

/**
 * PackGyver - Widget Manager
 *
 * @package widget
 */
class WidgetManager {

	/**
	 * 
	 * @return array
	 */
	public static function getMostSeasonPointsPlus($limit = 3) {
		$archive = new SeasonArchive();
		$seasons = $archive->getSeasons();

		$i = 0;
		$tops = array();

		foreach ($seasons as $season) {
			/* @var $season \Season */

			if ($season->getPlayerCollection()->count() > 0) {
				$ranking = new RankingManager($season->getPlayerCollection());
				$season->setPlayerCollection($ranking->getPlayerCollection());

				$player = $ranking->getPlayerForRank(1);

				if ($player instanceof PlayerModel) {
					$stats = $player->getStats(PlayerStatistics::CACHETYPE_SEASON);
					$points = $stats->getPoints();

					$tops[$i]['season'] = $season->toHashSimple();
					$tops[$i]['player'] = $player->toHash();
					$tops[$i]['matchCount'] = $stats->getCountMatches();
					$tops[$i]['points'] = $points;

					$i++;
				}
			}
		}

		if (count($tops) > 0) {
			$sort = new SortUtil();
			$tops = array_values($sort->sort($tops, 'points', SortUtil::TYPE_DESC));

			if (0 !== $limit && count($tops) > $limit) {
				array_splice($tops, $limit);
			}
		}

		return $tops;
	}

	/**
	 * 
	 * @return array
	 */
	public static function getMostSeasonPointsMinus($limit = 3) {
		$archive = new SeasonArchive();
		$seasons = $archive->getSeasons();

		$i = 0;
		$tops = array();

		foreach ($seasons as $season) {
			/* @var $season \Season */

			if ($season->getPlayerCollection()->count() > 0) {
				$ranking = new RankingManager($season->getPlayerCollection());
				$season->setPlayerCollection($ranking->getPlayerCollection());

				$player = $ranking->getPlayerForRank($ranking->getPlayerCollection()->count());

				if ($player instanceof PlayerModel) {
					$stats = $player->getStats(PlayerStatistics::CACHETYPE_SEASON);
					$points = $stats->getPoints();

					$tops[$i]['season'] = $season->toHashSimple();
					$tops[$i]['player'] = $player->toHash();
					$tops[$i]['matchCount'] = $stats->getCountMatches();
					$tops[$i]['points'] = $points;

					$i++;
				}
			}
		}

		if (count($tops) > 0) {
			$sort = new SortUtil();
			$tops = array_values($sort->sort($tops, 'points', SortUtil::TYPE_ASC));

			if (0 !== $limit && count($tops) > $limit) {
				array_splice($tops, $limit);
			}
		}

		return $tops;
	}

	/**
	 *
	 * @param string $type
	 * @param int $limit
	 * @return array
	 */
	public static function getMostMonsterPacksRanking($type, $limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			if ($playerHash['stats'][$type]['monsterPackCount'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'monsterPackCount' => $playerHash['stats'][$type]['monsterPackCount']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'monsterPackCount'));

			if (0 !== $limit && count($hash) > $limit) {
				array_splice($hash, $limit);
			}
		}

		return $hash;
	}

	/**
	 *
	 * @param string $type
	 * @param int $limit
	 * @return array
	 */
	public static function getMostMonsterPacksOwnedRanking($type, $limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			if ($playerHash['stats'][$type]['ownedMonsterPackCount'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'ownedMonsterPackCount' => $playerHash['stats'][$type]['ownedMonsterPackCount']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'ownedMonsterPackCount'));

			if (count($hash) > $limit) {
				array_splice($hash, $limit);
			}
		}

		return $hash;
	}

	/**
	 *
	 * @param string $type
	 * @param int $limit
	 * @return array
	 */
	public static function getMostPacksRanking($type, $limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			if ($playerHash['stats'][$type]['packCount'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'packCount' => $playerHash['stats'][$type]['packCount']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'packCount'));

			if (count($hash) > $limit) {
				array_splice($hash, $limit);
			}
		}

		return $hash;
	}

	/**
	 *
	 * @param int $type
	 * @param int $limit
	 * @return array
	 */
	public static function getLongestWinStreak($type, $limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			if ($playerHash['stats'][$type]['streaks']['bestWinStreak'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'bestWinStreak' => $playerHash['stats'][$type]['streaks']['bestWinStreak']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'bestWinStreak'));

			if (count($hash) > $limit) {
				array_splice($hash, $limit);
			}

			//return $hash[0];
		}

		return $hash;
	}

	/**
	 *
	 * @param int $type
	 * @param int $limit
	 * @return array
	 */
	public static function getLongestLossStreak($type, $limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$playerModel = new PlayerModel($player);
			$playerHash = $playerModel->toHash(true);

			if ($playerHash['stats'][$type]['streaks']['bestLossStreak'] > 0) {
				$hash[] = array(
					'player' => $playerHash,
					'bestLossStreak' => $playerHash['stats'][$type]['streaks']['bestLossStreak']
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = array_values($sort->sort($hash, 'bestLossStreak'));

			if (count($hash) > $limit) {
				array_splice($hash, $limit);
			}

			//return $hash[0];
		}

		return $hash;
	}

	/**
	 *
	 * @param int $type
	 * @param int $limit
	 * @return array
	 */
	public static function getMostMatchesPlayer($type, $limit = null) {
		$hash = array();
		$playerMatchCount = array();

		$ranking = new RankingManager(PlayerCollection::create(true, null, false));
		$players = $ranking->getPlayerCollection()->toHash(true);

		if ('alltime' === $type) {
			foreach ($players as $player) {
				$hash[] = array(
					'player' => $player,
					'countMatches' => $player['stats'][$type]['countMatches']
				);
			}

			if (count($hash) > 0) {
				$sort = new SortUtil();
				$hash = $sort->sort($hash, 'countMatches');

				if (count($hash) > $limit) {
					array_splice($hash, $limit);
				}
			}

			return $hash;
		} else {
			foreach ($players as $player) {
				$playerMatchCount[$player['stats'][$type]['countMatches']] = $player['id'];
			}

			if (count($playerMatchCount) > 0) {
				krsort($playerMatchCount, SORT_NUMERIC);

				$matchCount = array_keys($playerMatchCount);
				$players = array_values($playerMatchCount);

				$playerModel = new PlayerModel(PlayerEntity::findById(PG::getDB(), $players[0]));

				return array(
					'matchCount' => $matchCount[0],
					'player' => $playerModel->toHash(),
					'type' => $type
				);
			}

			return $playerMatchCount;
		}
	}

	/**
	 *
	 * @return array 
	 */
	public static function getTodaysMostMatchesPlayer() {
		$sql = 'SELECT pm.playerId, count(pm.id) AS matchCount
                FROM `playerMatch` as pm
                INNER JOIN `match` AS m
                ON m.id = pm.matchId
                WHERE DATE(m.dateCreated) = DATE(CURDATE())
                AND (m.resultTeamYellow != 0 AND m.resultTeamRed != 0)
                GROUP BY pm.playerId
                ORDER BY matchCount DESC
                LIMIT 1';

		$stmt = PG::getDB()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetch();

		if (!empty($result)) {
			$playerModel = new PlayerModel(PlayerEntity::findById(PG::getDB(), $result['playerId']));

			return array(
				'matchCount' => $result['matchCount'],
				'player' => $playerModel->toHash()
			);
		}

		return array();
	}

	/**
	 *
	 * @param int $limit
	 * @return array
	 */
	public static function getAwardRanking($limit = 3) {
		$hash = array();

		foreach (PlayerModel::getPlayers(PG::getDB()) as $player) {
			/* @var $player PlayerEntity */
			$awardCount = count($player->fetchPlayerAwardEntityCollection(PG::getDB()));
			if ($awardCount !== 0) {
				$playerModel = new PlayerModel($player);

				$hash[] = array(
					'player' => $playerModel->toHash(),
					'awardCount' => $awardCount
				);
			}
		}

		if (count($hash) > 0) {
			$sort = new SortUtil();
			$hash = $sort->sort($hash, 'awardCount');

			if (count($hash) > $limit) {
				array_splice($hash, $limit);
			}
		}

		return $hash;
	}

	/**
	 *
	 * @param int $limit
	 * @return array
	 */
	public static function getAwardsGiven($limit = 3) {
		$hash = array();
		$sql = 'SELECT * FROM playerAward ORDER BY date DESC LIMIT ' . (int) $limit;

		foreach (PlayerAwardEntity::findBySql(PG::getDB(), $sql) as $key => $playerAward) {
			/* @var $playerAward PlayerAwardEntity */
			$awardEntity = $playerAward->fetchAwardEntity(PG::getDB());
			if ($awardEntity instanceof AwardEntity) {
				$playerModel = new PlayerModel($playerAward->fetchPlayerEntity(PG::getDB()));

				$hash[$key]['player'] = $playerModel->toHash();
				$hash[$key]['award'] = AwardManager::getHashForAward($awardEntity);
				$hash[$key]['award']['date'] = DateUtil::formatFromDB($playerAward->getDate());
				$hash[$key]['award']['cdate'] = DateUtil::formatISO8601(DateUtil::formatFromDB($playerAward->getDate(), true));
			}
		}

		return $hash;
	}

}
