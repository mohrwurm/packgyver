<?php

/**
 * PackGyver - json_encode
 */
class Dwoo_Plugin_json_encode extends Dwoo_Plugin {

	/**
	 * @param array $data
	 * @return string
	 */
	public function process(array $data) {
		return json_encode($data);
	}

}
