<?php

/**
 * PackGyver - Admin Manager
 */
class AdminManager {

	const CACHE_TYPE_PLAYERSTATS_ALLTIME = 'playerStatsAlltime';
	const CACHE_TYPE_PLAYERSTATS_SEASON = 'playerStatsSeason';
	const CACHE_TYPE_PLAYERSTATS = 'playerStats';
	const CACHE_TYPE_AUTOLOADER = 'autoload';
	const CACHE_TYPE_CACHEBACKEND = 'cacheBackend';
	const CACHE_TYPE_DWOO = 'dwoo';
	const CACHE_TYPE_ALL = 'cacheClear';

	/**
	 *
	 * @var array
	 */
	private static $cacheTypes = array(
		//self::CACHE_TYPE_PLAYERSTATS_ALLTIME,
		//self::CACHE_TYPE_PLAYERSTATS_SEASON,
		self::CACHE_TYPE_PLAYERSTATS,
		self::CACHE_TYPE_AUTOLOADER,
//		self::CACHE_TYPE_APC,
		self::CACHE_TYPE_DWOO
			//self::CACHE_TYPE_ALL
	);

	/**
	 * clear cache by type
	 *
	 * @param string $type
	 */
	public static function clearCache($type = null) {
		switch ($type) {
			case self::CACHE_TYPE_PLAYERSTATS_ALLTIME:
				PlayerStatisticsManager::clearPlayerStatsCache(PlayerStatistics::CACHETYPE_ALLTIME);
				break;
			case self::CACHE_TYPE_PLAYERSTATS_SEASON:
				PlayerStatisticsManager::clearPlayerStatsCache(PlayerStatistics::CACHETYPE_SEASON);
				break;
			case self::CACHE_TYPE_PLAYERSTATS:
				PlayerStatisticsManager::clearPlayerStatsCache();
				break;
			case self::CACHE_TYPE_AUTOLOADER:
				self::clearAutoloadCache();
				break;
			case self::CACHE_TYPE_CACHEBACKEND:
				self::clearCacheBackend();
				break;
			case self::CACHE_TYPE_DWOO:
				self::clearDwooCache();
				break;
			case self::CACHE_TYPE_ALL:
				foreach (self::$cacheTypes as $cacheType) {
					self::clearCache($cacheType);
				}

				Cache::getCache()->flush();

				break;
		}
	}

	/**
	 * clear autoload cache
	 */
	private static function clearAutoloadCache() {
		$file = PG::getCacheDir() . DS . 'autoload.cache';
		if (file_exists($file)) {
			unlink($file);
		}
		touch($file);
		chmod($file, 0777);
	}

	/**
	 * clear apc cache
	 */
	private static function clearCacheBackend() {
		Cache::getCache()->flush();
	}

	/**
	 * clear dwoo cache
	 */
	private static function clearDwooCache() {
		$file = PG::getCacheDir() . DS . 'dwoo' . DS;

		if (is_dir($file . 'cache')) {
			FileUtil::deleteRecursive($file . 'cache');
		}
		if (is_dir($file . 'compiled')) {
			FileUtil::deleteRecursive($file . 'compiled');
		}
		mkdir($file . 'cache', 0777);
		mkdir($file . 'compiled', 0777);
	}

	/**
	 * checkout all players
	 *
	 * @param PDO $db
	 * @param int $type
	 */
	public static function changePlayerStatus(PDO $db, $type) {
		if (1 === $type) {
			$sql = 'UPDATE `player` SET checkedIn = 1';
		} else {
			$sql = 'UPDATE `player` SET checkedIn = 0';
		}
		$db->query($sql);
	}

	/**
	 * @param int $interval
	 * @return PlayerCollection
	 */
	public static function getLoggedInPlayers($interval = 5) {
		$sql = "SELECT * FROM player WHERE lastActivity > DATE_SUB(NOW(), INTERVAL " . $interval . " MINUTE)";
		$collection = new PlayerCollection();

		foreach (PlayerEntity::findBySql(PG::getDB(), $sql) as $player) {
			$collection->appendPlayer(new PlayerModel($player));
		}

		return $collection;
	}

}
