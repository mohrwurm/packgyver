<?php

/**
 * PackGyver - Session
 */
class Session {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var Session
	 */
	private static $instance = null;

	/**
	 *
	 * @return Session
	 */
	public static function instance() {
		if (null === self::$instance) {
			self::$instance = new Session();
		}

		return self::$instance;
	}

	/**
	 * CTOR - assign sessionId
	 */
	public function __construct() {
		session_start();
		$this->id = session_id();
	}

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * read from session
	 *
	 * @param mixed $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($key, $default = null) {
		return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : $default;
	}

	/**
	 * save to session
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @return Session
	 */
	public function set($key, $value) {
		$_SESSION[$key] = $value;

		return $this;
	}

	/**
	 * delete value from session
	 *
	 * @param mixed $key
	 * @return Session
	 */
	public function delete($key) {
		if (isset($_SESSION[$key])) {
			unset($_SESSION[$key]);
		}

		return $this;
	}

	/**
	 * regenerate sessionId
	 *
	 * @return Session
	 */
	public function regenerate() {
		if (session_regenerate_id(true)) {
			$this->id = session_id();
		}

		return $this;
	}

	/**
	 * destroy session
	 */
	public function destroy() {
		session_destroy();
	}

	/**
	 * close session
	 */
	public function writeClose() {
		session_write_close();
	}

}

?>