<?php

/**
 * PackGyver - Statistics
 */
class StatisticsManager {

	/**
	 *
	 * @return string 
	 */
	public static function getStatsCacheDir() {
		$cacheDir = PG::getCacheDir() . DS . 'statistics';
		if (!is_dir($cacheDir)) {
			mkdir($cacheDir, 0755, true);
		}
		return $cacheDir;
	}

	/**
	 *
	 * @param PDO $db
	 * @return array
	 */
	public static function getStatsSet(PDO $db, $month = null, $year = null) {
		return array(
			'matchCount' => self::getMatchCount($db, $month, $year),
			'redTeamWins' => self::getRedTeamWins($db, $month, $year),
			'yellowTeamWins' => self::getYellowTeamWins($db, $month, $year),
			'monsterPackCount' => self::getMonsterPackCount($db, $month, $year),
			'packCount' => self::getPackCount($db, $month, $year),
			'matchesPerDay' => self::getMatchesPerDay($db, $month, $year)
		);
	}

	/**
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function getMatchCount(PDO $db, $month = null, $year = null) {
		$sql = 'SELECT COUNT(id) as matchCount FROM `match` WHERE (`resultTeamRed` != 0 AND `resultTeamYellow` != 0) AND deleted = 0';

		if (null !== $month && null !== $year) {
			$sql.=' AND MONTH(dateCreated) = ' . $month . ' AND YEAR(dateCreated) = ' . $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

	/**
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function getRedTeamWins(PDO $db, $month = null, $year = null) {
		$sql = 'SELECT COUNT(id) as matchCount FROM `match` WHERE ((`resultTeamRed` > `resultTeamYellow`) AND (`resultTeamRed` != 0 AND `resultTeamYellow` != 0)) AND deleted = 0';

		if (null !== $month && null !== $year) {
			$sql.=' AND MONTH(dateCreated) = ' . $month . ' AND YEAR(dateCreated) = ' . $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

	/**
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function getYellowTeamWins(PDO $db, $month = null, $year = null) {
		$sql = 'SELECT COUNT(id) as matchCount FROM `match` WHERE ((`resultTeamRed` < `resultTeamYellow`) AND (`resultTeamRed` != 0 AND `resultTeamYellow` != 0)) AND deleted = 0';

		if (null !== $month && null !== $year) {
			$sql.=' AND MONTH(dateCreated) = ' . $month . ' AND YEAR(dateCreated) = ' . $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

	/**
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function getMonsterPackCount(PDO $db, $month = null, $year = null) {
		$sql = 'SELECT COUNT(id) as matchCount FROM `match`'
				. 'WHERE ((`resultTeamRed` = 10 && `resultTeamYellow` = 0) OR (`resultTeamRed` = 0 && `resultTeamYellow` = 10)) AND deleted = 0';

		if (null !== $month && null !== $year) {
			$sql.=' AND MONTH(dateCreated) = ' . $month . ' AND YEAR(dateCreated) = ' . $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

	/**
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function getPackCount(PDO $db, $month = null, $year = null) {
		$sql = 'SELECT COUNT(id) as matchCount FROM `match`'
				. 'WHERE ((`resultTeamRed` = 10 && `resultTeamYellow` < 5) OR (`resultTeamRed` < 5 && `resultTeamYellow` = 10)) AND deleted = 0';

		if (null !== $month && null !== $year) {
			$sql.=' AND MONTH(dateCreated) = ' . $month . ' AND YEAR(dateCreated) = ' . $year;
		}

		return (int) $db->query($sql)->fetchColumn();
	}

	/**
	 *
	 * @param PDO $db
	 * @return float
	 */
	public static function getMatchesPerDay(PDO $db, $month = null, $year = null) {
		$holidays = array(
			'24.12.2011',
			'25.12.2011',
			'26.12.2011',
			'27.12.2011',
			'28.12.2011',
			'29.12.2011',
			'30.12.2011',
			'31.12.2011',
			'01.01.2012',
			'02.01.2012',
			'03.01.2012',
			'04.01.2012',
			'05.01.2012',
			'06.01.2012',
			'07.01.2012',
			'08.01.2012'
		);

		$matchCount = self::getMatchCount($db, $month, $year);

		if (null !== $month && null !== $year) {
			$workingDays = DateUtil::getWorkingDays(date('Y-m-d', mktime(null, null, null, $month, 1, $year)), DateUtil::formatForDB(), $holidays);
		} else {
			$workingDays = DateUtil::getWorkingDays(PG::getRecordStart(), DateUtil::formatForDB(), $holidays);
		}

		return round($matchCount / $workingDays, 2);
	}

}
