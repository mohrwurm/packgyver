<?php

/**
 * PackGyver - Player Statistics
 */
class PlayerStatistics {

	const DOM_ROOTNODE = 'PlayerStatistics';
	const STATS_CACHE_DIR = 'players';
	const CACHETYPE_SEASON = 0;
	const CACHETYPE_ALLTIME = 1;
	/* new monster pack points */
	const MONSTER_PACK_WIN_POINTS = 60;
	const MONSTER_PACK_LOSS_POINTS = 40;
	/* old monster pack points */
	const MONSTER_PACK_WIN_POINTS_OLD = 30;
	const MONSTER_PACK_LOSS_POINTS_OLD = 20;

	/**
	 *
	 * @var array
	 */
	private $cacheTypes = array(
		self::CACHETYPE_SEASON => 'season',
		self::CACHETYPE_ALLTIME => 'alltime'
	);

	/**
	 *
	 * @return string
	 */
	public function getCacheTypeName() {
		return $this->cacheTypes[$this->getType()];
	}

	/**
	 *
	 * @var int
	 */
	private $type;

	/**
	 *
	 * @var PlayerModel
	 */
	private $player;

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setPlayer(PlayerModel $player) {
		$this->player = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getPlayer() {
		return $this->player;
	}

	/**
	 *
	 * @var int
	 */
	private $seasonMonth;

	/**
	 *
	 * @var int
	 */
	private $points;

	/**
	 *
	 * @var int
	 */
	private $countMatches;

	/**
	 *
	 * @var int
	 */
	private $countWins;

	/**
	 *
	 * @var int
	 */
	private $countLosses;

	/**
	 *
	 * @var float
	 */
	private $matchWinRatio;

	/**
	 *
	 * @var float
	 */
	private $packQuote;

	/**
	 *
	 * @var float
	 */
	private $monsterPackQuote;

	/**
	 *
	 * @var int
	 */
	private $playerGoals;

	/**
	 *
	 * @var int
	 */
	private $enemyGoals;

	/**
	 *
	 * @var int
	 */
	private $packCount;

	/**
	 *
	 * @var int
	 */
	private $monsterPackCount;

	/**
	 *
	 * @var int
	 */
	private $ownedPackCount;

	/**
	 *
	 * @var int
	 */
	private $ownedMonsterPackCount;

	/**
	 *
	 * @var float
	 */
	private $ownedPackQuote;

	/**
	 *
	 * @var float
	 */
	private $ownedMonsterPackQuote;

	/**
	 *
	 * @var int
	 */
	private $matchesWonInGoal;

	/**
	 *
	 * @var int
	 */
	private $matchesLostInGoal;

	/**
	 *
	 * @var int
	 */
	private $matchesWonInOffense;

	/**
	 *
	 * @var int
	 */
	private $matchesLostInOffense;

	/**
	 *
	 * @var int
	 */
	private $fearedPlayer;

	/**
	 *
	 * @var array
	 */
	private $buddyPlayer;

	/**
	 *
	 * @var array
	 */
	private $fearedTeamPlayer;

	/**
	 *
	 * @var array
	 */
	private $victimPlayer;

	/**
	 *
	 * @var array
	 */
	private $streaks;

	/**
	 *
	 * @var array
	 */
	private $positions;

	/**
	 *
	 * @var float
	 */
	private $averagePoints;

	/**
	 *
	 * @var array
	 */
	private $pointTrend;

	/**
	 *
	 * @var string
	 */
	private $lastUpdate;

	/**
	 *
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 *
	 * @param int $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 *
	 * @return int
	 */
	public function getSeasonMonth() {
		return $this->seasonMonth;
	}

	/**
	 *
	 * @param int $seasonMonth
	 */
	public function setSeasonMonth($seasonMonth) {
		$this->seasonMonth = $seasonMonth;
	}

	/**
	 *
	 * @return int
	 */
	public function getPoints() {
		return $this->points;
	}

	/**
	 *
	 * @param int $points
	 */
	public function setPoints($points) {
		$this->points = $points;
	}

	/**
	 *
	 * @return int
	 */
	public function getCountMatches() {
		return $this->countMatches;
	}

	/**
	 *
	 * @param int $countMatches
	 */
	public function setCountMatches($countMatches) {
		$this->countMatches = $countMatches;
	}

	/**
	 *
	 * @return int
	 */
	public function getCountWins() {
		return $this->countWins;
	}

	/**
	 *
	 * @param int $countWins
	 */
	public function setCountWins($countWins) {
		$this->countWins = $countWins;
	}

	/**
	 *
	 * @return int
	 */
	public function getCountLosses() {
		return $this->countLosses;
	}

	/**
	 *
	 * @param int $countLosses
	 */
	public function setCountLosses($countLosses) {
		$this->countLosses = $countLosses;
	}

	/**
	 *
	 * @return float
	 */
	public function getMatchWinRatio() {
		return $this->matchWinRatio;
	}

	/**
	 *
	 * @param float $matchWinRatio
	 */
	public function setMatchWinRatio($matchWinRatio) {
		$this->matchWinRatio = $matchWinRatio;
	}

	/**
	 *
	 * @return float
	 */
	public function getPackQuote() {
		return $this->packQuote;
	}

	/**
	 *
	 * @param float $packQuote
	 */
	public function setPackQuote($packQuote) {
		$this->packQuote = $packQuote;
	}

	/**
	 *
	 * @return float
	 */
	public function getMonsterPackQuote() {
		return $this->monsterPackQuote;
	}

	/**
	 *
	 * @param float $monsterPackQuote
	 */
	public function setMonsterPackQuote($monsterPackQuote) {
		$this->monsterPackQuote = $monsterPackQuote;
	}

	/**
	 *
	 * @return int
	 */
	public function getPlayerGoals() {
		return $this->playerGoals;
	}

	/**
	 *
	 * @param int $playerGoals
	 */
	public function setPlayerGoals($playerGoals) {
		$this->playerGoals = $playerGoals;
	}

	/**
	 *
	 * @return int
	 */
	public function getEnemyGoals() {
		return $this->enemyGoals;
	}

	/**
	 *
	 * @param int $enemyGoals
	 */
	public function setEnemyGoals($enemyGoals) {
		$this->enemyGoals = $enemyGoals;
	}

	/**
	 *
	 * @return int
	 */
	public function getPackCount() {
		return $this->packCount;
	}

	/**
	 *
	 * @param int $packCount
	 */
	public function setPackCount($packCount) {
		$this->packCount = $packCount;
	}

	/**
	 *
	 * @return int
	 */
	public function getMonsterPackCount() {
		return $this->monsterPackCount;
	}

	/**
	 *
	 * @param int $monsterPackCount
	 */
	public function setMonsterPackCount($monsterPackCount) {
		$this->monsterPackCount = $monsterPackCount;
	}

	/**
	 *
	 * @return int
	 */
	public function getOwnedPackCount() {
		return $this->ownedPackCount;
	}

	/**
	 *
	 * @param int $ownedPackCount
	 */
	public function setOwnedPackCount($ownedPackCount) {
		$this->ownedPackCount = $ownedPackCount;
	}

	/**
	 *
	 * @return float
	 */
	public function getOwnedMonsterPackCount() {
		return $this->ownedMonsterPackCount;
	}

	/**
	 *
	 * @param int $ownedMonsterPackCount
	 */
	public function setOwnedMonsterPackCount($ownedMonsterPackCount) {
		$this->ownedMonsterPackCount = $ownedMonsterPackCount;
	}

	/**
	 *
	 * @return float
	 */
	public function getOwnedPackQuote() {
		return $this->ownedPackQuote;
	}

	/**
	 *
	 * @param float $ownedPackQuote
	 */
	public function setOwnedPackQuote($ownedPackQuote) {
		$this->ownedPackQuote = $ownedPackQuote;
	}

	/**
	 *
	 * @return float
	 */
	public function getOwnedMonsterPackQuote() {
		return $this->ownedMonsterPackQuote;
	}

	/**
	 *
	 * @param float $ownedMonsterPackQuote
	 */
	public function setOwnedMonsterPackQuote($ownedMonsterPackQuote) {
		$this->ownedMonsterPackQuote = $ownedMonsterPackQuote;
	}

	/**
	 *
	 * @return int
	 */
	public function getMatchesWonInGoal() {
		return $this->matchesWonInGoal;
	}

	/**
	 *
	 * @param int $matchesWonInGoal
	 */
	public function setMatchesWonInGoal($matchesWonInGoal) {
		$this->matchesWonInGoal = $matchesWonInGoal;
	}

	/**
	 *
	 * @return int
	 */
	public function getMatchesLostInGoal() {
		return $this->matchesLostInGoal;
	}

	/**
	 *
	 * @param int $matchesLostInGoal
	 */
	public function setMatchesLostInGoal($matchesLostInGoal) {
		$this->matchesLostInGoal = $matchesLostInGoal;
	}

	/**
	 *
	 * @return int
	 */
	public function getMatchesWonInOffense() {
		return $this->matchesWonInOffense;
	}

	/**
	 *
	 * @param int $matchesWonInOffense
	 */
	public function setMatchesWonInOffense($matchesWonInOffense) {
		$this->matchesWonInOffense = $matchesWonInOffense;
	}

	/**
	 *
	 * @return int
	 */
	public function getMatchesLostInOffense() {
		return $this->matchesLostInOffense;
	}

	/**
	 *
	 * @param int $matchesLostInOffense
	 */
	public function setMatchesLostInOffense($matchesLostInOffense) {
		$this->matchesLostInOffense = $matchesLostInOffense;
	}

	/**
	 *
	 * @return array
	 */
	public function getFearedPlayer() {
		return $this->fearedPlayer;
	}

	/**
	 *
	 * @param array $fearedPlayer
	 */
	public function setFearedPlayer($fearedPlayer) {
		$this->fearedPlayer = $fearedPlayer;
	}

	/**
	 *
	 * @return array
	 */
	public function getBuddyPlayer() {
		return $this->buddyPlayer;
	}

	/**
	 *
	 * @param array $buddyPlayer
	 */
	public function setBuddyPlayer($buddyPlayer) {
		$this->buddyPlayer = $buddyPlayer;
	}

	/**
	 *
	 * @return array
	 */
	public function getFearedTeamPlayer() {
		return $this->fearedTeamPlayer;
	}

	/**
	 *
	 * @param int $fearedTeamPlayer
	 */
	public function setFearedTeamPlayer($fearedTeamPlayer) {
		$this->fearedTeamPlayer = $fearedTeamPlayer;
	}

	/**
	 *
	 * @return array
	 */
	public function getVictimPlayer() {
		return $this->victimPlayer;
	}

	/**
	 *
	 * @param int $victimPlayer
	 */
	public function setVictimPlayer($victimPlayer) {
		$this->victimPlayer = $victimPlayer;
	}

	/**
	 *
	 * @return array
	 */
	public function getStreaks() {
		return $this->streaks;
	}

	/**
	 *
	 * @param array $streaks
	 */
	public function setStreaks($streaks) {
		$this->streaks = $streaks;
	}

	/**
	 *
	 * @return array
	 */
	public function getPositions() {
		return $this->positions;
	}

	/**
	 *
	 * @param array $positions
	 */
	public function setPositions($positions) {
		$this->positions = $positions;
	}

	/**
	 *
	 * @return float
	 */
	public function getAveragePoints() {
		return $this->averagePoints;
	}

	/**
	 *
	 * @param float $averagePoints
	 */
	public function setAveragePoints($averagePoints) {
		$this->averagePoints = $averagePoints;
	}

	/**
	 *
	 * @return array
	 */
	public function getPointTrend() {
		return $this->pointTrend;
	}

	/**
	 *
	 * @param array $pointTrend
	 */
	public function setPointTrend($pointTrend) {
		$this->pointTrend = $pointTrend;
	}

	/**
	 *
	 * @return string
	 */
	public function getLastUpdate() {
		return $this->lastUpdate;
	}

	/**
	 *
	 * @param string $lastUpdate
	 */
	public function setLastUpdate($lastUpdate) {
		$this->lastUpdate = $lastUpdate;
	}

	/**
	 * CTOR
	 *
	 * @param PlayerModel $player
	 * @param int $type
	 */
	public function __construct(PlayerModel $player, $type) {
		$this->player = $player;
		$this->type = $type;
	}

	/**
	 *
	 * @return string
	 */
	private function getStatsCacheDir() {
		return StatisticsManager::getStatsCacheDir() . DS . self::STATS_CACHE_DIR . DS . $this->getCacheTypeName();
	}

	/**
	 *
	 * @return string
	 */
	private function getStatsCacheFile() {
		return $this->getStatsCacheDir() . DS . $this->getPlayer()->getId() . '.xml';
	}

	/**
	 *
	 */
	public function clearStatsCache() {
		$cacheFile = $this->getStatsCacheFile();

		if (file_exists($cacheFile)) {
			unlink($cacheFile);
		}
	}

	/**
	 *
	 */
	public function recalculate() {
		$this->clearStatsCache();
		$this->calculate()->store();
	}

	/**
	 * store stats
	 */
	public function store() {
		$this->toDOM()->save($this->getStatsCacheFile());
	}

	/**
	 *
	 * @return \PlayerStatistics
	 */
	public function load() {
		$cacheDir = $this->getStatsCacheDir();
		$cacheFile = $this->getStatsCacheFile();

		if (!is_dir($cacheDir)) {
			mkdir($cacheDir, 0755, true);
		}

		if (file_exists($cacheFile) && is_readable($cacheFile)) {
			$doc = DOMUtil::newDOMDocument();
			$doc->load($cacheFile);
			$this->readFromDOM($doc->getElementsByTagName(self::DOM_ROOTNODE)->item(0));
		} else {
			$this->calculate()->store();
		}

		return $this;
	}

	/**
	 * calculate player statistics
	 *
	 * @param int $month
	 * @param int $year
	 * @return PlayerStatistics
	 */
	public function calculate($month = null, $year = null) {
		if (self::CACHETYPE_SEASON == $this->getType() && null === $month && null === $year) {
			$month = date('n');
			$year = date('Y');
		}

		// dirty, for testing only
		if ($year > 2011) {
			$monsterPackWinPoints = self::MONSTER_PACK_WIN_POINTS;
			$monsterPackLossPoints = self::MONSTER_PACK_LOSS_POINTS;
		} else {
			$monsterPackWinPoints = self::MONSTER_PACK_WIN_POINTS_OLD;
			$monsterPackLossPoints = self::MONSTER_PACK_LOSS_POINTS_OLD;
		}

		$db = PG::getDB();
		$points = 0;
		$pointTrend = array(0);
		$matchPoints = array();

		$playerGoals = 0;
		$enemyGoals = 0;

		$packCount = 0;
		$monsterPackCount = 0;
		$ownedPackCount = 0;
		$ownedMonsterPackCount = 0;

		$matchesWonInGoal = 0;
		$matchesWonInOffense = 0;

		$buddyPlayer = array();
		$fearedEnemyPlayer = array();
		$fearedTeamPlayer = array();
		$favouriteEnemyPlayer = array();

		$currentWinStreak = 0;
		$currentLossStreak = 0;
		$bestWinStreak = 0;
		$bestLossStreak = 0;
		$winStreakActive = true;
		$lossStreakActive = true;

		$positionsPlayedOffense = 0;
		$positionsPlayedGoal = 0;
		$positionsPlayedAll = 0;

		$allPlayerMatches = array_reverse(MatchManager::getPlayerMatches($db, $this->getPlayer()->getEntity(), $month, $year, null, false, false));
		$allPlayerMatchCount = count($allPlayerMatches);
		$allPlayerMatchWins = PlayerStatisticsManager::getAllMatchesPlayerWinCount($db, $this->getPlayer()->getEntity(), $month, $year);
		$allPlayerMatchLosses = $allPlayerMatchCount - $allPlayerMatchWins;

//	if ($this->getPlayer()->getId() == 2) {
//
//	    echo "<pre>";
//	    var_dump($allPlayerMatches);
//	}
		$lastMatchProcessed = null;

		$matchCount = 1;
		foreach ($allPlayerMatches as $match) {
			if ($match instanceof MatchEntity) {
				$matchStatusWin = false;
				$matchStatusTeam = null;
				$matchPlayers = $match->fetchPlayerMatchEntityCollection($db);

				if (PlayerModel::isPlayerInWinningTeam($db, $this->getPlayer()->getEntity(), $match)) {
					$matchStatusWin = true;
					$lossStreakActive = false;

					if ($lastMatchProcessed instanceof MatchEntity) {
						if (date('n', DateUtil::formatFromDB($match->getDateCreated(), true)) != date('n', DateUtil::formatFromDB($lastMatchProcessed->getDateCreated(), true))) {
							$currentWinStreak = 0;
						}
					}

					if (($currentWinStreak + 1) > $bestWinStreak) {
						$bestWinStreak = $currentWinStreak + 1;
					}
					if (false === $winStreakActive) {
						$currentWinStreak = 0;
						$currentLossStreak = 0;
					}

					$winStreakActive = true;
					$currentWinStreak++;

//		    if ($this->getPlayer()->getId() == 2) {
//				var_dump($match->getId());
//			    }

					if (MatchManager::isTeamRed($this->getPlayer()->getEntity()->getId(), $match->getId())) {
						$matchStatusTeam = MatchManager::TEAM_RED;

						$playerGoals += $match->getResultTeamRed();
						$enemyGoals += $match->getResultTeamYellow();
						$difference = $match->getResultTeamRed() - $match->getResultTeamYellow();

						if (0 == $match->getResultTeamYellow()) {
							/* monster pack */
							$addPoints = $monsterPackWinPoints;
							$points += $addPoints;
							$monsterPackCount++;
						} else if ($difference > 5) {
							/* pack */
							$addPoints = round($difference / 2) + (10 + $difference);
							$points += $addPoints;
							$packCount++;
//			    if ($match->getId() == 2333 && $this->getPlayer()->getId() == 2) {
//				echo "<pre>";
//				var_dump(1, null, $match, $difference, $addPoints, $points);
//				exit;
//			    }
//			    
						} else {
							$addPoints = 10 + $difference;
							$points += $addPoints;
						}
					} else {
						$matchStatusTeam = MatchManager::TEAM_YELLOW;

						$playerGoals += $match->getResultTeamYellow();
						$enemyGoals += $match->getResultTeamRed();
						$difference = $match->getResultTeamYellow() - $match->getResultTeamRed();

						if (0 == $match->getResultTeamRed()) {
							/* monster pack */
							$addPoints = $monsterPackWinPoints;
							$points += $addPoints;
							$monsterPackCount++;
						} else if ($difference > 5) {
							/* pack */
							$addPoints = round($difference / 2) + (10 + $difference);
							$points += $addPoints;
							$packCount++;
						} else {
							$addPoints = 10 + $difference;
							$points += $addPoints;
						}
					}

					$matchPoints[] = $addPoints;
				} else {
					$matchStatusWin = false;
					$winStreakActive = false;

					if ($lastMatchProcessed instanceof MatchEntity) {
						if (date('n', DateUtil::formatFromDB($match->getDateCreated(), true)) != date('n', DateUtil::formatFromDB($lastMatchProcessed->getDateCreated(), true))) {
							$currentLossStreak = 0;
						}
					}

					if (($currentLossStreak + 1) > $bestLossStreak) {
						$bestLossStreak = $currentLossStreak + 1;
					}
					if (false === $lossStreakActive) {
						$currentLossStreak = 0;
						$currentWinStreak = 0;
					}

					$lossStreakActive = true;
					$currentLossStreak++;

					if (MatchManager::isTeamRed($this->getPlayer()->getEntity()->getId(), $match->getId())) {
						$matchStatusTeam = MatchManager::TEAM_RED;

						$playerGoals += $match->getResultTeamRed();
						$enemyGoals += $match->getResultTeamYellow();
						$difference = $match->getResultTeamYellow() - $match->getResultTeamRed();

						if (0 == $match->getResultTeamRed()) {
							/* monster pack */
							$minPoints = $monsterPackLossPoints;
							$points -= $minPoints;
							$ownedMonsterPackCount++;
						} else if ($difference > 5) {
							/* pack */
							$minPoints = round($difference / 2) + ($difference);
							$points -= $minPoints;
							$ownedPackCount++;
						} else {
							$minPoints = $difference;
							$points -= $minPoints;
						}
					} else {
						$matchStatusTeam = MatchManager::TEAM_YELLOW;

						$playerGoals += $match->getResultTeamYellow();
						$enemyGoals += $match->getResultTeamRed();
						$difference = $match->getResultTeamRed() - $match->getResultTeamYellow();

						if (0 == $match->getResultTeamYellow()) {
							/* monster pack */
							$minPoints = $monsterPackLossPoints;
							$points -= $minPoints;
							$ownedMonsterPackCount++;
						} else if ($difference > 5) {
							/* pack */
							$minPoints = round($difference / 2) + ($difference);
							$points -= $minPoints;
							$ownedPackCount++;
						} else {
							$minPoints = $difference;
							$points -= $minPoints;
						}
					}

					$matchPoints[] = -$minPoints;
				}
				$matchCount++;
				$pointTrend[] = $points;

				$matchPlayer = null;
				foreach ($matchPlayers as $mpp) {
					if ($mpp->getPlayerId() == $this->getPlayer()->getEntity()->getId()) {
						$matchPlayer = $mpp;
						break;
					}
				}

				if ($matchPlayer instanceof PlayerMatchEntity) {
					switch ($matchPlayer->getPosition()) {
						case MatchManager::POSITION_OFFENSE:
							$positionsPlayedOffense++;
							break;
						case MatchManager::POSITION_GOAL:
							$positionsPlayedGoal++;
							break;
						case MatchManager::POSITION_ALL:
							$positionsPlayedAll++;
							break;
					}

					if (true === $matchStatusWin) {
						switch ($matchPlayer->getPosition()) {
							case MatchManager::POSITION_GOAL:
								$matchesWonInGoal++;
								break;
							case MatchManager::POSITION_OFFENSE:
								$matchesWonInOffense++;
								break;
						}
					}

					foreach ($matchPlayers as $mp) {
						if ($mp->getTeam() == $matchPlayer->getTeam()
								&& $mp->getPlayerId() != $this->getPlayer()->getEntity()->getId()) {
							$buddyPlayer[$mp->getPlayerId()]++;

							if (false === $matchStatusWin) {
								$fearedTeamPlayer[$mp->getPlayerId()]++;
							}
						}
						if ($mp->getPlayerId() != $this->getPlayer()->getEntity()->getId()) {
							if (true === $matchStatusWin) {
								// win -> victim
								if ($matchStatusTeam != $mp->getTeam()) {
									$favouriteEnemyPlayer[$mp->getPlayerId()]++;
								}
							} else {
								// loss -> feared
								if ($matchStatusTeam != $mp->getTeam()) {
									$fearedEnemyPlayer[$mp->getPlayerId()]++;
								}
							}
						}
					}
				}

				$lastMatchProcessed = $match;
			}
		}

		$this->setPoints(intval($points));

		if (count($matchPoints) > 0 && 0 !== $allPlayerMatchCount) {
			$this->setAveragePoints(round(array_sum($matchPoints) / $allPlayerMatchCount, 1));
		} else {
			$this->setAveragePoints(0);
		}

		$this->setCountMatches(intval($allPlayerMatchCount));
		$this->setCountWins(intval($allPlayerMatchWins));
		$this->setCountLosses(intval($allPlayerMatchLosses));

		if (0 != $allPlayerMatchWins && 0 != $allPlayerMatchCount) {
			$this->setMatchWinRatio(round($allPlayerMatchWins / $allPlayerMatchCount, 3) * 100);
		} else {
			$this->setMatchWinRatio(0);
		}

		if (0 != $allPlayerMatchCount && 0 != $packCount) {
			$this->setPackQuote(round(($packCount / $allPlayerMatchCount) * 100, 1));
		} else {
			$this->setPackQuote(0);
		}

		if (0 != $allPlayerMatchCount && 0 != $monsterPackCount) {
			$this->setMonsterPackQuote(round(($monsterPackCount / $allPlayerMatchCount) * 100, 1));
		} else {
			$this->setMonsterPackQuote(0);
		}

		$this->setPlayerGoals(intval($playerGoals));
		$this->setEnemyGoals(intval($enemyGoals));
		$this->setPackCount(intval($packCount));
		$this->setMonsterPackCount(intval($monsterPackCount));
		$this->setOwnedPackCount(intval($ownedPackCount));
		$this->setOwnedMonsterPackCount(intval($ownedMonsterPackCount));

		if (0 != $allPlayerMatchCount && 0 != $ownedPackCount) {
			$this->setOwnedPackQuote(round(($ownedPackCount / $allPlayerMatchCount) * 100, 1));
		} else {
			$this->setOwnedPackQuote(0);
		}

		if (0 != $allPlayerMatchCount && 0 != $ownedMonsterPackCount) {
			$this->setOwnedMonsterPackQuote(round(($ownedMonsterPackCount / $allPlayerMatchCount) * 100, 1));
		} else {
			$this->setOwnedMonsterPackQuote(0);
		}

		$statFearedPlayer = array();
		$statBuddyPlayer = array();
		$statFearedTeamPlayer = array();
		$statVictimPlayer = array();

		if (count($fearedEnemyPlayer) > 0 && is_array($fearedEnemyPlayer)) {
			arsort($fearedEnemyPlayer, SORT_NUMERIC);
			$fearedEnemyPlayerMatchCount = array_values($fearedEnemyPlayer);
			$fearedEnemyPlayer = array_keys($fearedEnemyPlayer);

			$statFearedPlayer['playerId'] = intval($fearedEnemyPlayer[0]);
			$statFearedPlayer['matchCount'] = intval($fearedEnemyPlayerMatchCount[0]);
		}
		if (count($buddyPlayer) > 0 && is_array($buddyPlayer)) {
			arsort($buddyPlayer, SORT_NUMERIC);
			$buddyPlayerMatchCount = array_values($buddyPlayer);
			$buddyPlayer = array_keys($buddyPlayer);

			// feared player isn´t considered a buddy player
			if ($buddyPlayer[0] == $fearedEnemyPlayer[0] && isset($buddyPlayer[1])) {
				$statBuddyPlayer['playerId'] = intval($buddyPlayer[1]);
				$statBuddyPlayer['matchCount'] = intval($buddyPlayerMatchCount[1]);
			} else {
				$statBuddyPlayer['playerId'] = intval($buddyPlayer[0]);
				$statBuddyPlayer['matchCount'] = intval($buddyPlayerMatchCount[0]);
			}
		}
		if (count($fearedTeamPlayer) > 0 && is_array($fearedTeamPlayer)) {
			arsort($fearedTeamPlayer, SORT_NUMERIC);
			$fearedTeamPlayerMatchCount = array_values($fearedTeamPlayer);
			$fearedTeamPlayer = array_keys($fearedTeamPlayer);

			$statFearedTeamPlayer['playerId'] = intval($fearedTeamPlayer[0]);
			$statFearedTeamPlayer['matchCount'] = intval($fearedTeamPlayerMatchCount[0]);
		}
		if (count($favouriteEnemyPlayer) > 0 && is_array($favouriteEnemyPlayer)) {
			arsort($favouriteEnemyPlayer, SORT_NUMERIC);
			$favouriteEnemyPlayerMatchCount = array_values($favouriteEnemyPlayer);
			$favouriteEnemyPlayer = array_keys($favouriteEnemyPlayer);

			if ($favouriteEnemyPlayer[0] == $fearedEnemyPlayer[0] && isset($favouriteEnemyPlayer[1])) {
				$statVictimPlayer['playerId'] = intval($favouriteEnemyPlayer[1]);
				$statVictimPlayer['matchCount'] = intval($favouriteEnemyPlayerMatchCount[1]);
			} else {
				$statVictimPlayer['playerId'] = intval($favouriteEnemyPlayer[0]);
				$statVictimPlayer['matchCount'] = intval($favouriteEnemyPlayerMatchCount[0]);
			}
		}

		$this->setFearedPlayer($statFearedPlayer);
		$this->setBuddyPlayer($statBuddyPlayer);
		$this->setFearedTeamPlayer($statFearedTeamPlayer);
		$this->setVictimPlayer($statVictimPlayer);

		$statStreaks = array();
		if ($this->getType() === self::CACHETYPE_SEASON) {
			if ($currentWinStreak > 0) {
				$statStreaks['currentStreak'] = '+' . $currentWinStreak;
			} else if ($currentLossStreak > 0) {
				$statStreaks['currentStreak'] = '-' . $currentLossStreak;
			} else {
				$statStreaks['currentStreak'] = 0;
			}

			$statStreaks['currentWinStreak'] = intval($currentWinStreak);
			$statStreaks['currentLossStreak'] = intval($currentLossStreak);
		}
		$statStreaks['bestWinStreak'] = intval($bestWinStreak);
		$statStreaks['bestLossStreak'] = intval($bestLossStreak);

		$this->setStreaks($statStreaks);

		$statPositions = array(
			'offense' => $positionsPlayedOffense,
			'goal' => $positionsPlayedGoal,
			'all' => $positionsPlayedAll
		);
		$this->setPositions($statPositions);

		$this->setMatchesWonInGoal(intval($matchesWonInGoal));
		$this->setMatchesLostInGoal($statPositions['goal'] - $matchesWonInGoal);
		$this->setMatchesWonInOffense(intval($matchesWonInOffense));
		$this->setMatchesLostInOffense($statPositions['offense'] - $matchesWonInOffense);

		if (count($pointTrend) > 30) {
			// performance
			$pointTrend = array_splice($pointTrend, -30);
		}

		$this->setPointTrend($pointTrend);

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$this->setSeasonMonth($month);
		}

		$this->setLastUpdate(DateUtil::format());

		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		$hash['type'] = $this->type;
		$hash['points'] = $this->points;
		$hash['countMatches'] = $this->countMatches;
		$hash['countWins'] = $this->countWins;
		$hash['countLosses'] = $this->countLosses;
		$hash['matchWinRatio'] = $this->matchWinRatio;
		$hash['packQuote'] = $this->packQuote;
		$hash['monsterPackQuote'] = $this->monsterPackQuote;
		$hash['playerGoals'] = $this->playerGoals;
		$hash['enemyGoals'] = $this->enemyGoals;
		$hash['packCount'] = $this->packCount;
		$hash['monsterPackCount'] = $this->monsterPackCount;
		$hash['ownedPackCount'] = $this->ownedPackCount;
		$hash['ownedMonsterPackCount'] = $this->ownedMonsterPackCount;
		$hash['ownedPackQuote'] = $this->ownedPackQuote;
		$hash['ownedMonsterPackQuote'] = $this->ownedMonsterPackQuote;
		$hash['matchesWonInGoal'] = $this->matchesWonInGoal;
		$hash['matchesLostInGoal'] = $this->matchesLostInGoal;
		$hash['matchesWonInOffense'] = $this->matchesWonInOffense;
		$hash['matchesLostInOffense'] = $this->matchesLostInOffense;
		$hash['fearedPlayer'] = $this->fearedPlayer;
		$hash['buddyPlayer'] = $this->buddyPlayer;
		$hash['fearedTeamPlayer'] = $this->fearedTeamPlayer;
		$hash['victimPlayer'] = $this->victimPlayer;
		$hash['streaks'] = $this->streaks;
		$hash['averagePoints'] = $this->averagePoints;
		$hash['pointTrend'] = $this->pointTrend;
		$hash['positions'] = $this->positions;

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$hash['seasonMonth'] = $this->seasonMonth;
		}

		$hash['lastUpdate'] = $this->lastUpdate;

		return $hash;
	}

	/**
	 *
	 * @param array $hash
	 */
	public function assignFromHash($hash) {
		$this->type = $hash['type'];
		$this->points = $hash['points'];
		$this->countMatches = $hash['countMatches'];
		$this->countWins = $hash['countWins'];
		$this->countLosses = $hash['countLosses'];
		$this->matchWinRatio = $hash['matchWinRatio'];
		$this->packQuote = $hash['packQuote'];
		$this->monsterPackQuote = $hash['monsterPackQuote'];
		$this->playerGoals = $hash['playerGoals'];
		$this->enemyGoals = $hash['enemyGoals'];
		$this->packCount = $hash['packCount'];
		$this->monsterPackCount = $hash['monsterPackCount'];
		$this->ownedPackCount = $hash['ownedPackCount'];
		$this->ownedMonsterPackCount = $hash['ownedMonsterPackCount'];
		$this->ownedPackQuote = $hash['ownedPackQuote'];
		$this->ownedMonsterPackQuote = $hash['ownedMonsterPackQuote'];
		$this->matchesWonInGoal = $hash['matchesWonInGoal'];
		$this->matchesLostInGoal = $hash['matchesLostInGoal'];
		$this->matchesWonInOffense = $hash['matchesWonInOffense'];
		$this->matchesLostInOffense = $hash['matchesLostInOffense'];
		$this->fearedPlayer = $hash['fearedPlayer'];
		$this->buddyPlayer = $hash['buddyPlayer'];
		$this->fearedTeamPlayer = $hash['fearedTeamPlayer'];
		$this->victimPlayer = $hash['victimPlayer'];
		$this->streaks = $hash['streaks'];
		$this->averagePoints = $hash['averagePoints'];
		$this->pointTrend = $hash['pointTrend'];
		$this->positions = $hash['positions'];

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$this->seasonMonth = $hash['seasonMonth'];
		}

		$this->lastUpdate = $hash['lastUpdate'];
	}

	/**
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		$doc = DOMUtil::newDOMDocument();
		$root = $doc->createElement(self::DOM_ROOTNODE);

		$root->appendChild($doc->createElement('type', $this->type));
		$root->appendChild($doc->createElement('points', $this->points));
		$root->appendChild($doc->createElement('countMatches', $this->countMatches));
		$root->appendChild($doc->createElement('countWins', $this->countWins));
		$root->appendChild($doc->createElement('countLosses', $this->countLosses));
		$root->appendChild($doc->createElement('matchWinRatio', $this->matchWinRatio));
		$root->appendChild($doc->createElement('packQuote', $this->packQuote));
		$root->appendChild($doc->createElement('monsterPackQuote', $this->monsterPackQuote));
		$root->appendChild($doc->createElement('playerGoals', $this->playerGoals));
		$root->appendChild($doc->createElement('enemyGoals', $this->enemyGoals));
		$root->appendChild($doc->createElement('packCount', $this->packCount));
		$root->appendChild($doc->createElement('monsterPackCount', $this->monsterPackCount));
		$root->appendChild($doc->createElement('ownedPackCount', $this->ownedPackCount));
		$root->appendChild($doc->createElement('ownedMonsterPackCount', $this->ownedMonsterPackCount));
		$root->appendChild($doc->createElement('ownedPackQuote', $this->ownedPackQuote));
		$root->appendChild($doc->createElement('ownedMonsterPackQuote', $this->ownedMonsterPackQuote));
		$root->appendChild($doc->createElement('matchesWonInGoal', $this->matchesWonInGoal));
		$root->appendChild($doc->createElement('matchesLostInGoal', $this->matchesLostInGoal));
		$root->appendChild($doc->createElement('matchesWonInOffense', $this->matchesWonInOffense));
		$root->appendChild($doc->createElement('matchesLostInOffense', $this->matchesLostInOffense));

		$fearedPlayer = $doc->createElement('fearedPlayer');
		$fearedPlayer->appendChild($doc->createElement('playerId', $this->fearedPlayer['playerId']));
		$fearedPlayer->appendChild($doc->createElement('matchCount', $this->fearedPlayer['matchCount']));
		$root->appendChild($fearedPlayer);

		$buddyPlayer = $doc->createElement('buddyPlayer');
		$buddyPlayer->appendChild($doc->createElement('playerId', $this->buddyPlayer['playerId']));
		$buddyPlayer->appendChild($doc->createElement('matchCount', $this->buddyPlayer['matchCount']));
		$root->appendChild($buddyPlayer);

		$fearedTeamPlayer = $doc->createElement('fearedTeamPlayer');
		$fearedTeamPlayer->appendChild($doc->createElement('playerId', $this->fearedTeamPlayer['playerId']));
		$fearedTeamPlayer->appendChild($doc->createElement('matchCount', $this->fearedTeamPlayer['matchCount']));
		$root->appendChild($fearedTeamPlayer);

		$victimPlayer = $doc->createElement('victimPlayer');
		$victimPlayer->appendChild($doc->createElement('playerId', $this->victimPlayer['playerId']));
		$victimPlayer->appendChild($doc->createElement('matchCount', $this->victimPlayer['matchCount']));
		$root->appendChild($victimPlayer);

		$streaks = $doc->createElement('streaks');

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$streaks->appendChild($doc->createElement('currentStreak', $this->streaks['currentStreak']));
			$streaks->appendChild($doc->createElement('currentWinStreak', $this->streaks['currentWinStreak']));
			$streaks->appendChild($doc->createElement('currentLossStreak', $this->streaks['currentLossStreak']));
		}

		$streaks->appendChild($doc->createElement('bestWinStreak', $this->streaks['bestWinStreak']));
		$streaks->appendChild($doc->createElement('bestLossStreak', $this->streaks['bestLossStreak']));
		$root->appendChild($streaks);

		$positions = $doc->createElement('positions');
		$positions->appendChild($doc->createElement('all', $this->positions['all']));
		$positions->appendChild($doc->createElement('goal', $this->positions['goal']));
		$positions->appendChild($doc->createElement('offense', $this->positions['offense']));
		$root->appendChild($positions);

		$root->appendChild($doc->createElement('averagePoints', $this->averagePoints));
		$root->appendChild($doc->createElement('pointTrend', serialize($this->pointTrend)));
		$root->appendChild($doc->createElement('lastUpdate', $this->lastUpdate));

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$root->appendChild($doc->createElement('seasonMonth', $this->seasonMonth));
		}

		$doc->appendChild($root);

		return $doc;
	}

	/**
	 *
	 * @param DOMElement $root
	 */
	public function readFromDOM(DOMElement $root) {
		$this->type = intval(DOMUtil::getElementValueByTagName($root, 'type'));
		$this->points = intval(DOMUtil::getElementValueByTagName($root, 'points'));
		$this->countMatches = intval(DOMUtil::getElementValueByTagName($root, 'countMatches'));
		$this->countWins = intval(DOMUtil::getElementValueByTagName($root, 'countWins'));
		$this->countLosses = intval(DOMUtil::getElementValueByTagName($root, 'countLosses'));
		$this->matchWinRatio = floatval(DOMUtil::getElementValueByTagName($root, 'matchWinRatio'));
		$this->packQuote = floatval(DOMUtil::getElementValueByTagName($root, 'packQuote'));
		$this->monsterPackQuote = floatval(DOMUtil::getElementValueByTagName($root, 'monsterPackQuote'));
		$this->playerGoals = intval(DOMUtil::getElementValueByTagName($root, 'playerGoals'));
		$this->enemyGoals = intval(DOMUtil::getElementValueByTagName($root, 'enemyGoals'));
		$this->packCount = intval(DOMUtil::getElementValueByTagName($root, 'packCount'));
		$this->monsterPackCount = intval(DOMUtil::getElementValueByTagName($root, 'monsterPackCount'));
		$this->ownedPackCount = intval(DOMUtil::getElementValueByTagName($root, 'ownedPackCount'));
		$this->ownedMonsterPackCount = intval(DOMUtil::getElementValueByTagName($root, 'ownedMonsterPackCount'));
		$this->ownedPackQuote = floatval(DOMUtil::getElementValueByTagName($root, 'ownedPackQuote'));
		$this->ownedMonsterPackQuote = floatval(DOMUtil::getElementValueByTagName($root, 'ownedMonsterPackQuote'));
		$this->matchesWonInGoal = intval(DOMUtil::getElementValueByTagName($root, 'matchesWonInGoal'));
		$this->matchesLostInGoal = intval(DOMUtil::getElementValueByTagName($root, 'matchesLostInGoal'));
		$this->matchesWonInOffense = intval(DOMUtil::getElementValueByTagName($root, 'matchesWonInOffense'));
		$this->matchesLostInOffense = intval(DOMUtil::getElementValueByTagName($root, 'matchesLostInOffense'));

		$fearedPlayer = DOMUtil::getElementByTagName($root, 'fearedPlayer');
		$fearedPlayerId = $fearedPlayer->getElementsByTagName('playerId');
		$fearedPlayerMatchCount = $fearedPlayer->getElementsByTagName('matchCount');
		if ($fearedPlayerId->length === 1 && $fearedPlayerMatchCount->length === 1) {
			$this->fearedPlayer = array();
			$this->fearedPlayer['playerId'] = intval($fearedPlayerId->item(0)->nodeValue);
			$this->fearedPlayer['matchCount'] = intval($fearedPlayerMatchCount->item(0)->nodeValue);
		}

		$buddyPlayer = DOMUtil::getElementByTagName($root, 'buddyPlayer');
		$buddyPlayerId = $buddyPlayer->getElementsByTagName('playerId');
		$buddyPlayerMatchCount = $buddyPlayer->getElementsByTagName('matchCount');
		if ($buddyPlayerId->length === 1 && $buddyPlayerMatchCount->length === 1) {
			$this->buddyPlayer = array();
			$this->buddyPlayer['playerId'] = intval($buddyPlayerId->item(0)->nodeValue);
			$this->buddyPlayer['matchCount'] = intval($buddyPlayerMatchCount->item(0)->nodeValue);
		}

		$fearedTeamPlayer = DOMUtil::getElementByTagName($root, 'fearedTeamPlayer');
		$fearedTeamPlayerId = $fearedTeamPlayer->getElementsByTagName('playerId');
		$fearedTeamPlayerMatchCount = $fearedTeamPlayer->getElementsByTagName('matchCount');
		if ($fearedTeamPlayerId->length === 1 && $fearedTeamPlayerMatchCount->length === 1) {
			$this->fearedTeamPlayer = array();
			$this->fearedTeamPlayer['playerId'] = intval($fearedTeamPlayerId->item(0)->nodeValue);
			$this->fearedTeamPlayer['matchCount'] = intval($fearedTeamPlayerMatchCount->item(0)->nodeValue);
		}

		$victimPlayer = DOMUtil::getElementByTagName($root, 'victimPlayer');
		$victimPlayerId = $victimPlayer->getElementsByTagName('playerId');
		$victimPlayerMatchCount = $victimPlayer->getElementsByTagName('matchCount');
		if ($victimPlayerId->length === 1 && $victimPlayerMatchCount->length === 1) {
			$this->victimPlayer = array();
			$this->victimPlayer['playerId'] = intval($victimPlayerId->item(0)->nodeValue);
			$this->victimPlayer['matchCount'] = intval($victimPlayerMatchCount->item(0)->nodeValue);
		}

		$streaks = DOMUtil::getElementByTagName($root, 'streaks');
		$this->streaks['bestWinStreak'] = intval($streaks->getElementsByTagName('bestWinStreak')->item(0)->nodeValue);
		$this->streaks['bestLossStreak'] = intval($streaks->getElementsByTagName('bestLossStreak')->item(0)->nodeValue);

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$this->streaks['currentStreak'] = $streaks->getElementsByTagName('currentStreak')->item(0)->nodeValue;
			$this->streaks['currentWinStreak'] = intval($streaks->getElementsByTagName('currentWinStreak')->item(0)->nodeValue);
			$this->streaks['currentLossStreak'] = intval($streaks->getElementsByTagName('currentLossStreak')->item(0)->nodeValue);
		}

		$positions = array();
		$positionsElement = DOMUtil::getElementByTagName($root, 'positions');
		$positions['all'] = $positionsElement->getElementsByTagName('all')->item(0)->nodeValue;
		$positions['goal'] = $positionsElement->getElementsByTagName('goal')->item(0)->nodeValue;
		$positions['offense'] = $positionsElement->getElementsByTagName('offense')->item(0)->nodeValue;
		$this->setPositions($positions);

		$this->averagePoints = floatval(DOMUtil::getElementValueByTagName($root, 'averagePoints'));
		$this->pointTrend = unserialize(DOMUtil::getElementValueByTagName($root, 'pointTrend'));
		$this->lastUpdate = DOMUtil::getElementValueByTagName($root, 'lastUpdate');

		if ($this->getType() === self::CACHETYPE_SEASON) {
			$this->seasonMonth = intval(DOMUtil::getElementValueByTagName($root, 'seasonMonth'));
		}
	}

}
