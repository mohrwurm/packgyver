<?php

/**
 * Description of NotificationManager
 */
class NotificationManager {

	/**
	 * 
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param NotificationEntity $lastNotification
	 * @return \Notification
	 */
	public static function getUnseenNotifications(PDO $db, PlayerEntity $player = null, NotificationEntity $lastNotification = null) {
		$results = array();

		if (null !== $player) {
			$playerId = $player->getId();
		} else {
			$playerId = AuthenticationManager::getInstance()->getPlayer()->getId();
		}

		$filter = array(
			new DFC(NotificationEntity::FIELD_PLAYERID, $playerId, DFC::EXACT)
		);

		if (null !== $lastNotification) {
			$filter[] = new DFC(NotificationEntity::FIELD_ID, $lastNotification->getId(), DFC::GREATER);
		}

		$unseen = NotificationEntity::findByFilter($db, $filter, true, array(
					new DSC(NotificationEntity::FIELD_ID, DSC::ASC)
				));

		if (!empty($unseen)) {
			foreach ($unseen as $notificationEntity) {
				$notification = new Notification();
				$notification->assignByEntity($notificationEntity);
				$results[] = $notification;
			}
		}

		return $results;
	}

}
