<?php

/**
 * PackGyver - Exception
 */
class PackGyverException extends Exception {

	/**
	 * @param Exception $e
	 */
	public static function logException(Exception $e) {
		Logger::get()->error($e->getMessage(), array('exception' => $e));
	}

}
