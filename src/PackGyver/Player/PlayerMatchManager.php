<?php

/**
 * PackGyver - Player Matches Model
 */
class PlayerMatchManager {

	/**
	 *
	 * @param MatchEntity $match
	 * @return array
	 */
	public static function getPlayerHashesForMatch(MatchEntity $match) {
		$playerMatches = PlayerMatchEntity::findByFilter(PG::getDB(), array(
					new DFC(PlayerMatchEntity::FIELD_MATCHID, $match->getId())
				));

		$playerHash = array();
		foreach ($playerMatches as $pm) {
			$playerModel = new PlayerModel(PlayerEntity::findById(PG::getDB(), $pm->getPlayerId()));
			$playerHash[$pm->getTeam()][$pm->getPosition()] = $playerModel->toHash(true);
		}

		return $playerHash;
	}

	/**
	 * check if a player has participated in a specific match
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 * @param PlayerEntity $player
	 * @return bool
	 */
	public static function hasPlayerParticipatedInMatch(PDO $db, MatchEntity $match, PlayerEntity $player = null) {
		if (null === $player) {
			$player = AuthenticationManager::getInstance()->getPlayer();
		}

		$sql = "SELECT COUNT(id) FROM `playerMatch`
                WHERE playerId = " . intval($player->getId()) . "
                AND matchId = " . intval($match->getId());

		return 0 !== intval($db->query($sql)->fetchColumn());
	}

}

?>