<?php

/**
 * Description of PlayerCollection
 */
class PlayerCollection implements IteratorAggregate, Countable {

	/**
	 *
	 * @var ArrayObject
	 */
	private $players;

	/**
	 * 
	 * @param array $players
	 */
	public function __construct(array $players = array()) {
		$this->players = new ArrayObject($players);
	}

	/**
	 * 
	 * @return int
	 */
	public function count() {
		return $this->players->count();
	}

	/**
	 * 
	 * @return Iterator
	 */
	public function getIterator() {
		return $this->players->getIterator();
	}

	/**
	 * 
	 * @param array $players
	 */
	public function setPlayers(array $players) {
		$this->players = new ArrayObject($players);
	}

	/**
	 * 
	 * @return ArrayObject
	 */
	public function getPlayers() {
		return $this->players;
	}

	/**
	 * 
	 * @param PlayerModel $player
	 */
	public function appendPlayer(PlayerModel $player) {
		$this->players->append($player);
	}

	/**
	* @param PlayerModel $player
	*/
	public function deletePlayer(PlayerModel $player) {
		foreach ($this->players as $index => $cplayer) {
			if ($cplayer->getId() == $player->getId()) {
				$this->players->offsetUnset($index);
			}
		}
	}

	/**
	 * @param PlayerModel $player
	 * @return bool
	 */
	public function existsPlayer(PlayerModel $player) {
		foreach ($this->players as $cplayer) {
			if ($cplayer->getId() == $player->getId()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param int $id
	 * @return PlayerModel|null
	 */
	public function getPlayerById($id) {
		foreach ($this as $player) {
			if ($player->getId() == $id) {
				return $player;
			}
		}

		return null;
	}

	/**
	 * 
	 * @param bool $withStats
	 * @return array
	 */
	public function toHash($withStats = false) {
		$hash = array();

		foreach ($this as $player) {
			/* @var $player PlayerModel */
			$hash[] = $player->toHash($withStats);
		}

		return $hash;
	}

	/**
	 * 
	 * @param bool $active
	 * @param bool $checkedIn
	 * @param bool $deleted
	 * @return \PlayerCollection
	 */
	public static function create($active = null, $checkedIn = null, $deleted = null) {
		$collection = new PlayerCollection();

		foreach (PlayerModel::getPlayers(PG::getDB(), $active, $checkedIn, $deleted) as $player) {
			$collection->appendPlayer(new PlayerModel($player));
		}

		return $collection;
	}

}
