<?php

/**
 * WebSocket Service - Exception
 *
 * @package websocket
 */
class WebSocketServiceException extends Exception {}
