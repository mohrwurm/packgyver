<?php

/**
 * PackGyver - Match Model Decorator
 *
 * @package websocket
 */
class WebSocketServiceMatchModelDecorator extends MatchModel {

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		$hash['id'] = $this->getMatchEntity()->getId();
		$hash['type'] = $this->getTypeName();
		$hash['dateCreated'] = DateUtil::formatFromDB($this->getDateCreated());
		$hash['createdByPlayer'] = $this->getCreatedByPlayer()->toHash();

		$hash['players'] = array();
		if ($this->getType() === self::TYPE_1ON1) {
			$redSingle = new WebSocketServicePlayerModelDecorator($this->getRedSinglePlayer()->getEntity());
			$yellowSingle = new WebSocketServicePlayerModelDecorator($this->getYellowSinglePlayer()->getEntity());

			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_ALL)] = $redSingle->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_ALL)] = $yellowSingle->toHash();
		} else {
			$redGoal = new WebSocketServicePlayerModelDecorator($this->getRedGoalPlayer()->getEntity());
			$redOffense = new WebSocketServicePlayerModelDecorator($this->getRedOffensePlayer()->getEntity());
			$yellowGoal = new WebSocketServicePlayerModelDecorator($this->getYellowGoalPlayer()->getEntity());
			$yellowOffense = new WebSocketServicePlayerModelDecorator($this->getYellowOffensePlayer()->getEntity());

			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_GOAL)] = $redGoal->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_OFFENSE)] = $redOffense->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_OFFENSE)] = $yellowOffense->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_GOAL)] = $yellowGoal->toHash();
		}

		$hash['playerIds'] = array();
		foreach ($this->getPlayers() as $player) {
			/* @var $player PlayerModel */
			$hash['playerIds'][] = $player->getId();
		}

		return $hash;
	}

}
