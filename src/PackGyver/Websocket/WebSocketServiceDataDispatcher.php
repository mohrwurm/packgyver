<?php

/**
 * WebSocket Service - Data Dispatcher
 *
 * @package websocket
 */
class WebSocketServiceDataDispatcher {

	/**
	 *
	 * @var \WebSocketServiceManager
	 */
	private $serviceManager;

	/**
	 * 
	 * @param \WebSocketServiceManager $manager
	 */
	public function __construct(\WebSocketServiceManager $manager) {
		$this->serviceManager = $manager;
	}

	/**
	 * Player CheckIn Change
	 */
	public function triggerCheckedPlayersEvent() {
		$players = array();

		foreach (PlayerModel::getPlayers(PG::getDB(), true, true, false) as $player) {
			$playerModel = new PlayerModel($player);
			$players[$playerModel->getId()]['id'] = $playerModel->getId();
			$players[$playerModel->getId()]['uid'] = $playerModel->getUrlIdentifier();
			$players[$playerModel->getId()]['firstname'] = $playerModel->getFirstname();
			$players[$playerModel->getId()]['lastname'] = $playerModel->getLastname();
			$players[$playerModel->getId()]['avatar'] = PlayerModel::getAvatarForPlayerId($playerModel->getId());
		}

		$this->serviceManager->getServiceProvider()->pushToChannel('checked-players', 'change', array(
			'players' => $players,
			'count' => count($players)
		));
	}

	/**
	 * onMatchCreate 
	 *
	 * @param PlayerEntity $player
     * @param MatchModel $match
	 */
    public function triggerCreateMatchEvent(PlayerEntity $player, MatchModel $match) {
		$matchDecorator = new WebSocketServiceMatchModelDecorator($match->getMatchEntity());

		$data = array(
			'triggeredBy' => $player->getId(),
			'match' => $matchDecorator->toHash()
		);

		$this->serviceManager->getServiceProvider()->pushToChannel('match-overview', 'create', $data);
	}

	/**
	 * onMatchScoreSaved 
	 * 
	 * @param PlayerEntity $player
	 */
	public function triggerSaveMatchEvent(PlayerEntity $player) {
		$this->serviceManager->getServiceProvider()->pushToChannel('match-overview', 'save', array(
			'triggeredBy' => $player->getId()
		));
	}

	/**
	 * onMatchDelete 
	 *
	 * @param PlayerEntity $player
	 */
	public function triggerDeleteMatchEvent(PlayerEntity $player) {
		$this->serviceManager->getServiceProvider()->pushToChannel('match-overview', 'delete', array(
			'triggeredBy' => $player->getId()
		));
	}

	/**
	 * match score has been updated
	 *
	 * @param PlayerEntity $player
	 * @param MatchEntity $match
	 * @param string $team
	 * @param int $score
	 */
	public function triggerMatchScoreEvent(PlayerEntity $player, MatchEntity $match, $team, $score) {
		$this->serviceManager->getServiceProvider()->pushToChannel('match-score', 'change', array(
			'id' => $match->getId(),
			'team' => $team,
			'score' => $score,
			'triggeredBy' => $player->getId()
		));
	}

	public function triggerNotificationReceived(PlayerEntity $player, Notification $notification) {
		$playerModel = new PlayerModel($player);
		$data = array(
			'count' => $playerModel->getNotifications()->count(),
			'notification' => array(
				'title' => $notification->getTitle(),
				'date' => $notification->getDateSent()
			)
		);

		$this->triggerPrivateChannelEvent($player, 'notification/create', $data);
	}

	/**
	 * Push data to a private player channel
	 * 
	 * @param PlayerEntity $player
	 * @param string $eventName
	 * @param array $data
	 */
	public function triggerPrivateChannelEvent(PlayerEntity $player, $eventName, array $data) {
		$this->serviceManager->getServiceProvider()->pushToChannel('player-prv-' . $player->getId(), $eventName, $data);
	}

}
