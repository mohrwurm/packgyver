<?php

/**
 * WebSocket Service - Pusher Configuration
 *
 * @package websocket.pusher
 */
class WebSocketServiceConfigurationVocket extends WebSocketServiceConfigurationAbstract {

	/**
	 *
	 * @var string
	 */
	protected $host;

	/**
	 *
	 * @var int
	 */
	protected $port;

    /**
     * @var int
     */
    protected $internalPort;

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host) {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * @param int $port
     * @return $this
     */
    public function setPort($port) {
        $this->port = $port;

        return $this;
    }

    /**
     * @return int
     */
    public function getPort() {
        return $this->port;
    }

    /**
     * @param int $port
     * @return $this
     */
    public function setInternalPort($port) {
        $this->internalPort = $port;

        return $this;
    }

    /**
     * @return int
     */
    public function getInternalPort() {
        return $this->internalPort;
    }

    /**
     * @return array
     */
    public function getOptionsHash() {
        $hash = parent::getOptionsHash();
        $hash['host'] = $this->getHost();
        $hash['port'] = $this->getPort();

        return $hash;
    }

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		$hash = parent::toHash();
		$hash['host'] = $this->getHost();
		$hash['port'] = $this->getPort();

		return $hash;
	}

}
