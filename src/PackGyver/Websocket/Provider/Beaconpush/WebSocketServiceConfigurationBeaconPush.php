<?php

/**
 * WebSocket Service - BeaconPush Configuration
 *
 * @package websocket.pusher
 */
class WebSocketServiceConfigurationBeaconPush extends WebSocketServiceConfigurationAbstract {

	/**
	 *
	 * @var string
	 */
	protected $apiVersion = '1.0.0';

}
