<?php

/**
 * WebSocket Service - Pusher Provider
 *
 * @package websocket.pusher
 * @copyright Squeeks - 2011 (partially)
 * @license MIT http://www.opensource.org/licenses/mit-license.php
 */
class WebSocketServiceProviderPusher implements WebSocketServiceProviderInterface {

	const SERVICE_KEY = 'pusher';

	/**
	 *
	 * @var \WebSocketServiceConfigurationPusher
	 */
	private $configuration;

	/**
	 * CTOR
	 * 
	 * Initialize the configuration
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Initialize the provider, load parameters from settings
	 */
	public function init() {
		$settings = SettingsManager::getGroup('pusher');

		$apiKey = $settings->getValue('apiKey');
		$apiSecret = $settings->getValue('apiSecret');
		$appId = $settings->getValue('appId');
		$requestTimeout = $settings->getValue('requestTimeout');

		if (!$apiKey) {
			$settings->setValue('apiKey', '');
		}
		if (!$apiSecret) {
			$settings->setValue('apiSecret', '');
		}
		if (!$appId) {
			$settings->setValue('appId', '');
		}
		if (!$requestTimeout) {
			$settings->setValue('requestTimeout', 30);
		}

		$this->configuration = new WebSocketServiceConfigurationPusher();
		$this->configuration->setAppId($appId)
				->setApiKey($apiKey)
				->setApiSecret($apiSecret)
				->setRequestTimeout($requestTimeout)
				->setServer('http://api.pusherapp.com')
				->setUrl('/apps/' . $appId)
				->setPort(80)
				->setLoggingEnabled(false);
	}

	/**
	 * Get the service configuration
	 * 
	 * @return \WebSocketServiceConfigurationPusher
	 */
	public function getConfiguration() {
		return $this->configuration;
	}

	/**
	 * Push data to a channel
	 * 
	 * @param string $channel
	 * @param string $eventName
	 * @param array $data
	 */
	public function pushToChannel($channel, $eventName, array $data = array()) {
		$this->trigger($channel, $eventName, $data);
	}

	/**
	 * Trigger an event by providing event name and payload. 
	 * Optionally provide a socket ID to exclude a client (most likely the sender).
	 * 
	 * @param string $channel
	 * @param string $event
	 * @param mixed $payload
	 * @param int $socketId
	 * @param bool $debug
	 * @param bool $alreadyEncoded
	 * @return bool
	 */
	public function trigger($channel, $event, $payload, $socketId = null, $debug = false, $alreadyEncoded = false) {
		# Check if we can initialize a cURL connection
		$ch = curl_init();

		# Add channel to URL..
		$sUrl = $this->getConfiguration()->getUrl() . '/channels/' . $channel . '/events';

		# Build the request
		$signature = "POST\n" . $sUrl . "\n";
		$payloadEncoded = $alreadyEncoded ? $payload : json_encode($payload);
		$query = "auth_key=" . $this->getConfiguration()->getApiKey() . "&auth_timestamp=" . time() . "&auth_version=1.0&body_md5=" . md5($payloadEncoded) . "&name=" . $event;

		# Socket ID set?
		if ($socketId !== null) {
			$query .= "&socket_id=" . $socketId;
		}

		# Create the signed signature...
		$authSignature = hash_hmac('sha256', $signature . $query, $this->getConfiguration()->getApiSecret(), false);
		$signedQuery = $query . "&auth_signature=" . $authSignature;
		$fullUrl = $this->getConfiguration()->getServer() . ':' . $this->getConfiguration()->getPort() . $sUrl . '?' . $signedQuery;

		# Set cURL opts and execute request
		curl_setopt($ch, CURLOPT_URL, $fullUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payloadEncoded);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->getConfiguration()->getRequestTimeout());

		$response = curl_exec($ch);

		curl_close($ch);

		if ($response == "202 ACCEPTED\n" && $debug == false) {
			return true;
		} elseif ($debug == true || $this->getConfiguration()->getLoggingEnabled() == true) {
			return $response;
		} else {
			return false;
		}
	}

	/**
	 * Creates a socket signature
	 * 
	 * @param string $channel
	 * @param int $socketId
	 * @param string $customData
	 * @return string
	 */
    public function socketAuth($channel, $socketId, $customData = null) {
		if ($customData) {
			$signature = hash_hmac('sha256', $socketId . ':' . $channel . ':' . $customData, $this->getConfiguration()->getApiSecret(), false);
		} else {
			$signature = hash_hmac('sha256', $socketId . ':' . $channel, $this->getConfiguration()->getApiSecret(), false);
		}

		$signature = array('auth' => $this->getConfiguration()->getApiKey() . ':' . $signature);
		// add the custom data if it has been supplied
		if ($customData) {
			$signature['channel_data'] = $customData;
		}

		return json_encode($signature);
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		return array(
			'name' => self::SERVICE_KEY,
			'config' => $this->getConfiguration()->toHash(),
			'options' => $this->getConfiguration()->getOptionsHash()
		);
	}

}

?>