<?php

/**
 * WebSocket Service - Pusher Configuration
 *
 * @package websocket.pusher
 */
class WebSocketServiceConfigurationPusher extends WebSocketServiceConfigurationAbstract {

	/**
	 *
	 * @var string
	 */
	protected $appId;

	/**
	 *
	 * @var string
	 */
	protected $server;

	/**
	 *
	 * @var int
	 */
	protected $port;

	/**
	 *
	 * @var string
	 */
	protected $url;

	/**
	 * 
	 * @return string
	 */
	public function getAppId() {
		return $this->appId;
	}

	/**
	 * 
	 * @param string $appId
	 * @return \WebSocketServiceConfigurationPusher
	 */
	public function setAppId($appId) {
		$this->appId = $appId;

		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getServer() {
		return $this->server;
	}

	/**
	 * 
	 * @param string $server
	 * @return \WebSocketServiceConfigurationPusher
	 */
	public function setServer($server) {
		$this->server = $server;

		return $this;
	}

	/**
	 * 
	 * @return int
	 */
	public function getPort() {
		return $this->port;
	}

	/**
	 * 
	 * @param int $port
	 * @return \WebSocketServiceConfigurationPusher
	 */
	public function setPort($port) {
		$this->port = $port;

		return $this;
	}

	/**
	 * 
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * 
	 * @param string $url
	 * @return \WebSocketServiceConfigurationPusher
	 */
	public function setUrl($url) {
		$this->url = $url;

		return $this;
	}

	/**
	 * 
	 * @return array
	 */
	public function toHash() {
		$hash = parent::toHash();
		$hash['appId'] = $this->getAppId();

		return $hash;
	}

}
