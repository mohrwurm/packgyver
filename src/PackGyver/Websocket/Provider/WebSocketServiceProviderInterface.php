<?php

/**
 * WebSocket Service - Provider Interface
 *
 * @package websocket
 */
interface WebSocketServiceProviderInterface {

	/**
	 * Initialize the provider, load parameters from settings
	 */
	public function init();

	/**
	 * Get the service configuration
	 * 
	 * @return \WebSocketServiceConfigurationAbstract
	 */
	public function getConfiguration();

	/**
	 * Push data to a channel
	 * 
	 * @param string $channel
	 * @param string $eventName
	 * @param array $data
	 */
	public function pushToChannel($channel, $eventName, array $data = array());

	/**
	 * @return array
	 */
	public function toHash();
}