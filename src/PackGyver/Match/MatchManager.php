<?php

/**
 * PackGyver - Match Manager
 */
class MatchManager {

	const TEAM_RED = 0;
	const TEAM_YELLOW = 1;
	const POSITION_ALL = 0;
	const POSITION_OFFENSE = 1;
	const POSITION_GOAL = 2;

	/**
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param int $month
	 * @param int $year
	 * @param int $limit
	 * @param bool $unsaved
	 * @param bool $deleted
	 * @return MatchEntity[]
	 */
	public static function getPlayerMatches(PDO $db, PlayerEntity $player, $month = null, $year = null, $limit = null, $unsaved = null, $deleted = null) {
		$sql = "SELECT *, M.id as id, PM.id AS playerMatchId FROM `match` AS M
                INNER JOIN `playerMatch` PM
                ON M.id = PM.matchId
                WHERE PM.playerId = " . intval($player->getId());

		if (null !== $unsaved) {
			if (true === $unsaved) {
				$sql .= " AND (M.resultTeamYellow != 10 AND M.resultTeamRed != 10)";
			} else {
				$sql .= " AND (M.resultTeamYellow = 10 OR M.resultTeamRed = 10)";
			}
		}

		if (null !== $deleted) {
			if (true === $deleted) {
				$sql .= " AND M.deleted = 1";
			} else {
				$sql .= " AND M.deleted = 0";
			}
		}

		if (null !== $month) {
			$sql .= " AND MONTH(M.dateCreated) = " . intval($month);
		}

		if (null !== $year) {
			$sql .= " AND YEAR(M.dateCreated) = " . intval($year);
		}

		$sql .= " ORDER BY M.dateSaved DESC";

		if (null !== $limit) {
			$sql .= " LIMIT " . intval($limit);
		}

		return MatchEntity::findBySql($db, $sql);
	}

	/**
	 *
	 * @param PDO $db
	 * @param PlayerEntity $player
	 * @param int $limit
	 * @return array
	 */
	public static function getRecentMatchesHashesForPlayer(PDO $db, PlayerEntity $player, $limit = 5) {
		$data = array();
		$playerMatches = self::getPlayerMatches($db, $player, date('n'), date('Y'), $limit, false, false);

		foreach ($playerMatches as $matchEntity) {
			$matchModel=new MatchModel($matchEntity);

			$data[$matchEntity->getId()] = $matchModel->toHash();
			$data[$matchEntity->getId()]['dateOnly'] = DateUtil::format(strtotime($matchModel->getDateSaved()), PG::DEFAULT_DATEFORMAT);
		}

		return $data;
	}

	/**
	 *
	 * @param int $playerId
	 * @param int $matchId
	 * @return bool
	 */
	public static function isTeamRed($playerId, $matchId) {
		$entities = PlayerMatchEntity::findByFilter(PG::getDB(), array(
					new DFC(PlayerMatchEntity::FIELD_MATCHID, $matchId),
					new DFC(PlayerMatchEntity::FIELD_PLAYERID, $playerId),
					new DFC(PlayerMatchEntity::FIELD_TEAM, self::TEAM_RED)
						), true);
		return !empty($entities);
	}

	/**
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public static function existsUnsavedMatch(PDO $db) {
		$sql = "SELECT COUNT(id) AS matchCount FROM `match` WHERE (resultTeamYellow != 10 AND resultTeamRed != 10) AND deleted = 0";
		return intval($db->query($sql)->fetchColumn()) > 0;
	}

	/**
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public static function isMatchUnsaved(MatchEntity $match) {
		return 10 != $match->getResultTeamRed() && 10 != $match->getResultTeamYellow();
	}

	/**
	 *
	 * @param PDO $db
	 * @return MatchEntity[]
	 */
	public static function getUnsavedMatches(PDO $db, $limit = null) {
		$sql = "SELECT * FROM `match` WHERE (resultTeamYellow != 10 AND resultTeamRed != 10) AND deleted = 0";
		$sql .= " ORDER BY id ASC";

		if (null !== $limit) {
			$sql .= " LIMIT " . intval($limit);
		}

		return MatchEntity::findBySql($db, $sql);
	}

	/**
	 * count all unsaved matches
	 *
	 * @param PDO $db
	 * @return int
	 */
	public static function countUnsavedMatches(PDO $db) {
		$sql = "SELECT COUNT(id) FROM `match` WHERE (resultTeamYellow != 10 AND resultTeamRed != 10) AND deleted = 0";
		return intval($db->query($sql)->fetchColumn());
	}

	/**
	 * count all season matches
	 *
	 * @param PDO $db
	 * @param bool $unsaved
	 * @return int
	 */
	public static function countSeasonMatches(PDO $db, $unsaved = false) {
		$sql = 'SELECT COUNT(id) FROM `match` AS M
                WHERE M.deleted = 0
				AND MONTH(M.dateCreated) = MONTH(NOW())
                AND YEAR(M.dateCreated) = YEAR(NOW())';

		if (false === $unsaved) {
			$sql .= ' AND (resultTeamYellow = 10 OR resultTeamRed = 10)';
		}

		return intval($db->query($sql)->fetchColumn());
	}

	/**
	 * check if the passed match is the currently #1 queued match (being currently played)
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 * @return bool
	 */
	public static function isMatchCurrentMatch(PDO $db, MatchEntity $match) {
		$sql = "SELECT COUNT(id) AS matchCount FROM `match` WHERE id < " . intval($match->getId()) . " AND (resultTeamYellow != 10 AND resultTeamRed != 10) AND deleted = 0";
		return 0 === intval($db->query($sql)->fetchColumn());
	}

	/**
	 * get all matches that are queued prior to the passed match
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 * @return int
	 */
	public static function getUnsavedMatchesBeforeMatchCount(PDO $db, MatchEntity $match) {
		$sql = "SELECT COUNT(id) FROM `match` WHERE id < " . intval($match->getId()) . " AND (resultTeamYellow != 10 AND resultTeamRed != 10) AND deleted = 0";
		return intval($db->query($sql)->fetchColumn());
	}

	/**
	 * check if match score is valid to save
	 *
	 * @param int $red
	 * @param int $yellow
	 * @return bool
	 */
	public static function isValidScore($red, $yellow) {
		if (!is_numeric($red) || !is_numeric($yellow)) {
			return false;
		}
		if (($red === 10 && MathUtil::between(0, 9, $yellow))
				|| ($yellow === 10 && MathUtil::between(0, 9, $red))) {
			return true;
		}

		return false;
	}

	/**
	 *
	 * @return bool
	 */
	public static function isNotificationEnabled() {
		$settings = SettingsManager::getGroup('match');
		$value = $settings->getValue('isGtalkEnabled', null);

		if (null === $value) {
			$settings->setValue('isGtalkEnabled', 1);
			return true;
		}

		return 1 === intval($value);
	}

	/**
	 * check if one can create a match
	 * 
	 * @param PDO $db
	 * @return bool
	 */
	public static function isMatchCreatable(PDO $db) {
		$settings = SettingsManager::getGroup('match');
		$limit = $settings->getValue('matchQueueLimit', null);

		if (null === $limit) {
			$settings->setValue('matchQueueLimit', 3);
			$limit = 3;
		}

		return $limit > MatchManager::countUnsavedMatches($db);
	}

	/**
	 *
	 * @param bool $deleted
	 * @param bool $unsaved
	 * @param int $month
	 * @param int $year
	 * @param mixed $limit
	 * @param string $sort
	 * @return MatchModel[]
	 */
	public static function getMatches($deleted = null, $unsaved = false, $month = null, $year = null, $limit = null, $sort = 'dateSaved') {
		if (null === $month) {
			$month = date('n', time());
		}
		if (null === $year) {
			$year = date('Y', time());
		}

		$sql = "SELECT * FROM `match` WHERE MONTH(dateCreated)=" . intval($month);

		if (null !== $year) {
			$sql .= " AND YEAR(dateCreated) = " . intval($year);
		}

		if (false === $unsaved) {
			$sql .= " AND (resultTeamRed = 10 OR resultTeamYellow = 10)";
		}

		if (null !== $deleted) {
			if (true === $deleted) {
				$sql .= " AND deleted = 1";
			} else {
				$sql .= " AND deleted = 0";
			}
		}

		$sql .= " ORDER BY " . $sort . " DESC";

		if (null !== $limit) {
			if (is_array($limit)) {
				$sql .= " LIMIT " . $limit['start'] . "," . $limit['step'];
			} else {
				$sql .= " LIMIT " . $limit;
			}
		}

		$matches = array();

		foreach (MatchEntity::findBySql(PG::getDB(), $sql) as $match) {
			$matches[] = new MatchModel($match);
		}

		return $matches;
	}

}

?>