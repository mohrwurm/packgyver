<?php

/**
 * PackGyver - Google Talk Message (Match Saved)
 */
class GTalkMatchSavedMessage extends XMPPHPGTalkMessage {

	/**
	 *
	 * @return string
	 */
	public function getMessage() {
		$match = $this->parameters['nextMatch'];
		$msg = null;

		if ($match instanceof MatchModel) {
			$msg = "~~~~~~~~~~~ DU BIST AN DER REIHE! ~~~~~~~~~~~\n\n";

			if ($match->getType() === MatchModel::TYPE_1ON1) {
				$redPlayer = $match->getRedSinglePlayer()->getFirstname() . ' ' . $match->getRedSinglePlayer()->getLastname();
				$yellowPlayer = $match->getYellowSinglePlayer()->getFirstname() . ' ' . $match->getYellowSinglePlayer()->getLastname();

				$msg .= "Team ROT: [ALLE] {$redPlayer}";
				$msg .= "\n\nTeam GELB: [ALLE] {$yellowPlayer}";
			} else {
				$redGoalPlayer = $match->getRedGoalPlayer()->getFirstname() . ' ' . $match->getRedGoalPlayer()->getLastname();
				$redOffensePlayer = $match->getRedOffensePlayer()->getFirstname() . ' ' . $match->getRedOffensePlayer()->getLastname();
				$yellowGoalPlayer = $match->getYellowGoalPlayer()->getFirstname() . ' ' . $match->getYellowGoalPlayer()->getLastname();
				$yellowOffensePlayer = $match->getYellowOffensePlayer()->getFirstname() . ' ' . $match->getYellowOffensePlayer()->getLastname();

				$msg .= "Team ROT: [TOR] {$redGoalPlayer} & [STURM] {$redOffensePlayer}";
				$msg .= "\n\nTeam GELB: [TOR] {$yellowGoalPlayer} & [STURM] {$yellowOffensePlayer}";
			}

			$msg .= "\n\n- Viel Glück, falls du es brauchst!";
		}

		return $msg;
	}

	/**
	 *
	 * @return bool
	 */
	public function isValidToSend() {
		return ($this->parameters['nextMatch'] instanceof MatchModel);
	}

}

?>