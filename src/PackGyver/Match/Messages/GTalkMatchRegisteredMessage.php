<?php

/**
 * PackGyver - Google Talk Message (Match Registered)
 */
class GTalkMatchRegisteredMessage extends XMPPHPGTalkMessage {

	/**
	 *
	 * @return string
	 */
	public function getMessage() {
		$match = $this->parameters['match'];
		$msg = null;

		if ($match instanceof MatchModel) {
			$msg = "~~~~~~~~~~~ EIN NEUES SPIEL WURDE GENERIERT! ~~~~~~~~~~~\n\n";

			if ($match->getType() === MatchModel::TYPE_1ON1) {
				$redPlayer = $match->getRedSinglePlayer()->getFirstname() . ' ' . $match->getRedSinglePlayer()->getLastname();
				$yellowPlayer = $match->getYellowSinglePlayer()->getFirstname() . ' ' . $match->getYellowSinglePlayer()->getLastname();

				$msg .= "Team ROT: [ALLE] {$redPlayer}";
				$msg .= "\n\nTeam GELB: [ALLE] {$yellowPlayer}";
			} else {
				$redGoalPlayer = $match->getRedGoalPlayer()->getFirstname() . ' ' . $match->getRedGoalPlayer()->getLastname();
				$redOffensePlayer = $match->getRedOffensePlayer()->getFirstname() . ' ' . $match->getRedOffensePlayer()->getLastname();
				$yellowGoalPlayer = $match->getYellowGoalPlayer()->getFirstname() . ' ' . $match->getYellowGoalPlayer()->getLastname();
				$yellowOffensePlayer = $match->getYellowOffensePlayer()->getFirstname() . ' ' . $match->getYellowOffensePlayer()->getLastname();

				$msg .= "Team ROT: [TOR] {$redGoalPlayer} & [STURM] {$redOffensePlayer}";
				$msg .= "\n\nTeam GELB: [TOR] {$yellowGoalPlayer} & [STURM] {$yellowOffensePlayer}";
			}

			$msg .= "\n\nHier geht´s zur Übersicht: " . PG::getSiteUrl() . "play";

			$matchesInQueue = MatchManager::getUnsavedMatchesBeforeMatchCount(PG::getDB(), $match->getMatchEntity());

			if ($matchesInQueue === 1) {
				$msg .= "\n\n - Derzeit wird noch ein Spiel ausgetragen. Du wirst benachrichtigt, sobald es beendet wurde.";
			} else if ($matchesInQueue > 1) {
				$msg .= "\n\n - Derzeit sind noch {$matchesInQueue} Spiele in der Warteschlange vor dir.";
				$msg .= " Du wirst benachrichtigt, sobald dein Spiel an der Reihe ist!";
			} else {
				$msg .= "\n\n - Es sind keine Spiele in der Warteschlange. Du kannst sofort loslegen!";
			}
		}

		return $msg;
	}

	/**
	 *
	 * @return bool
	 */
	public function isValidToSend() {
		return ($this->parameters['match'] instanceof MatchModel);
	}

}

?>