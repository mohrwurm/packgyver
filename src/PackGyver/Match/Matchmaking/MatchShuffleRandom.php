<?php

/**
 * PackGyver - Matchmaking Shuffler
 */
class MatchShuffleRandom extends MatchShuffler {

	/**
	 *
	 * @return \MatchModel
	 */
	public function shuffle() {
		$players = $this->doShuffle(array_values($this->getPlayers()));

		if ($this->getType() === MatchModel::TYPE_2ON2) {
			$typeValue = 4;
		} else {
			$typeValue = 2;
		}

		$players = array_splice($players, 0, $typeValue);

		if (true === $this->getParticipate()) {
			$inMatch = false;
			$loggedInPlayer = AuthenticationManager::getInstance()->getPlayer();

			foreach ($players as $player) {
				/* @var PlayerModel $player */
				if ($player->getId() == $loggedInPlayer->getId()) {
					$inMatch = true;
					break;
				}
			}

			if (false === $inMatch) {
				$players[mt_rand(0, $typeValue)] = new PlayerModel($loggedInPlayer);
			}
		}

		$matchEntity = new MatchEntity();
		$matchEntity->assignDefaultValues();
		$match = new MatchModel($matchEntity);

		if ($this->getType() === MatchModel::TYPE_2ON2) {
			$match->setRedGoalPlayer($players[0]);
			$match->setYellowGoalPlayer($players[1]);
			$match->setRedOffensePlayer($players[2]);
			$match->setYellowOffensePlayer($players[3]);
		} else {
			$match->setRedSinglePlayer($players[0]);
			$match->setYellowSinglePlayer($players[1]);
		}

		return $match;
	}

	/**
	 * Fisher-Yates Shuffle Algorithm
	 *
	 * @param array $players
	 * @return array
	 */
	private function doShuffle($players) {
		$this->generateSeed();

		for ($i = count($players) - 1; $i > 0; $i--) {
			$j = mt_rand(0, $i);
			$tmp = $players[$i];
			$players[$i] = $players[$j];
			$players[$j] = $tmp;
		}

		return $players;
	}

	/**
	 * Generate the seed for mt_srand()
	 */
	private function generateSeed() {
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);

		mt_srand($seed);
	}

	/**
	 *
	 * @param array $players
	 * @param int $iterations
	 * @return array
	 *
	 * @deprecated
	 */
	private function doShuffleDeprecated($players, $iterations) {
		for ($i = 0; $i < $iterations; $i++) {
			shuffle($players);
		}

		return $players;
	}

}

?>