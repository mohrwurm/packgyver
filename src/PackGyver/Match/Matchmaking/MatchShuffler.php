<?php

/**
 * PackGyver - Matchmaking Shuffler
 */
abstract class MatchShuffler {

	/**
	 *
	 * @var array
	 */
	private $players = array();

	/**
	 *
	 * @var int
	 */
	private $type;

	/**
	 *
	 * @var bool
	 */
	private $participate;

	/**
	 *
	 * @param array $players
	 */
	public function setPlayers($players) {
		$this->players = $players;
	}

	/**
	 *
	 * @return array
	 */
	public function getPlayers() {
		return $this->players;
	}

	/**
	 *
	 * @param int $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 *
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 *
	 * @param bool $type
	 */
	public function setParticipate($participate) {
		$this->participate = $participate;
	}

	/**
	 *
	 * @return bool
	 */
	public function getParticipate() {
		return $this->participate;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function appendPlayer(PlayerModel $player) {
		if (!isset($this->players[$player->getId()])) {
			$this->players[$player->getId()] = $player;
		}
	}

	/**
	 *
	 * @param int $type
	 */
	public function __construct($type, $participate) {
		$this->type = $type;
		$this->participate = $participate;
	}

	/**
	 *
	 * @return \MatchModel
	 */
	abstract public function shuffle();
}

?>