<?php

/**
 * PackGyver - Match Model
 */
class MatchModel extends ObservableSubject {
	/* types */

	const TYPE_1ON1 = 0;
	const TYPE_2ON2 = 1;

	/* teams */
	const TEAM_LEFT = 0;
	const TEAM_RIGHT = 1;

	/* positions */
	const POSITION_ALL = 0;
	const POSITION_OFFENSE = 1;
	const POSITION_GOAL = 2;

	/**
	 *
	 * @var MatchEntity
	 */
	private $matchEntity;

	/**
	 *
	 * @var PlayerMatchEntity[]
	 */
	private $playerMatchEntities;

	/**
	 *
	 * @var int
	 */
	private $scoreRed;

	/**
	 *
	 * @var int
	 */
	private $scoreYellow;

	/**
	 *
	 * @var int
	 */
	private $type;

	/**
	 *
	 * @var bool
	 */
	private $random;

	/**
	 *
	 * @var bool
	 */
	private $deleted;

	/**
	 *
	 * @var string
	 */
	private $dateCreated;

	/**
	 *
	 * @var string
	 */
	private $dateSaved;

	/**
	 *
	 * @var PlayerModel
	 */
	private $createdByPlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $savedByPlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $redSinglePlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $yellowSinglePlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $redGoalPlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $redOffensePlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $yellowGoalPlayer;

	/**
	 *
	 * @var PlayerModel
	 */
	private $yellowOffensePlayer;

	/**
	 *
	 * @var array
	 */
	private static $typeNames = array(
		self::TYPE_1ON1 => '1on1',
		self::TYPE_2ON2 => '2on2'
	);

	/**
	 *
	 * @var array
	 */
	private static $teamNames = array(
		self::TEAM_LEFT => 'left',
		self::TEAM_RIGHT => 'right'
	);

	/**
	 *
	 * @var array
	 */
	private static $positionNames = array(
		self::POSITION_ALL => 'all',
		self::POSITION_GOAL => 'goal',
		self::POSITION_OFFENSE => 'offense'
	);

	/**
	 *
	 * @return MatchEntity
	 */
	public function getMatchEntity() {
		return $this->matchEntity;
	}

	/**
	 *
	 * @param MatchEntity $match
	 */
	public function setMatchEntity(MatchEntity $match) {
		$this->matchEntity = $match;
	}

	/**
	 *
	 * @return int
	 */
	public function getScoreRed() {
		return $this->scoreRed;
	}

	/**
	 *
	 * @param int $score
	 */
	public function setScoreRed($score) {
		$this->scoreRed = $score;
	}

	/**
	 *
	 * @return int
	 */
	public function getScoreYellow() {
		return $this->scoreYellow;
	}

	/**
	 *
	 * @param int $score
	 */
	public function setScoreYellow($score) {
		$this->scoreYellow = $score;
	}

	/**
	 *
	 * @return int
	 */
	public function getType() {
		if (null === $this->type) {
			if ($this->getRedSinglePlayer() instanceof PlayerModel
					&& $this->getYellowSinglePlayer() instanceof PlayerModel) {

				$this->setType(self::TYPE_1ON1);
			} else if ($this->getRedGoalPlayer() instanceof PlayerModel
					&& $this->getRedOffensePlayer() instanceof PlayerModel
					&& $this->getYellowGoalPlayer() instanceof PlayerModel
					&& $this->getYellowOffensePlayer() instanceof PlayerModel) {

				$this->setType(self::TYPE_2ON2);
			}
		}
		return $this->type;
	}

	/**
	 *
	 * @param int $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 *
	 * @return string
	 */
	public function getTypeName() {
		return self::getNameForMatchType($this->getType());
	}

	/**
	 *
	 * @param int $type
	 * @return string
	 */
	public static function getNameForMatchType($type) {
		return self::$typeNames[$type];
	}

	/**
	 *
	 * @param int $position
	 * @return string
	 */
	public static function getNameForPosition($position) {
		return self::$positionNames[$position];
	}

	/**
	 *
	 * @param int $team
	 * @return string
	 */
	public static function getNameForTeam($team) {
		return self::$teamNames[$team];
	}

	/**
	 *
	 * @return bool
	 */
	public function isRandom() {
		return true == $this->random;
	}

	/**
	 *
	 * @param bool $random
	 */
	public function setRandom($random) {
		$this->random = $random;
	}

	/**
	 * 
	 * @param bool $deleted
	 */
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	/**
	 * 
	 * @return bool
	 */
	public function isDeleted() {
		return true === $this->deleted;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getRedSinglePlayer() {
		return $this->redSinglePlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setRedSinglePlayer(PlayerModel $player) {
		$this->redSinglePlayer = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getYellowSinglePlayer() {
		return $this->yellowSinglePlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setYellowSinglePlayer(PlayerModel $player) {
		$this->yellowSinglePlayer = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getRedGoalPlayer() {
		return $this->redGoalPlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setRedGoalPlayer(PlayerModel $player) {
		$this->redGoalPlayer = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getRedOffensePlayer() {
		return $this->redOffensePlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setRedOffensePlayer(PlayerModel $player) {
		$this->redOffensePlayer = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getYellowGoalPlayer() {
		return $this->yellowGoalPlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setYellowGoalPlayer(PlayerModel $player) {
		$this->yellowGoalPlayer = $player;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getYellowOffensePlayer() {
		return $this->yellowOffensePlayer;
	}

	/**
	 *
	 * @param PlayerModel $player
	 */
	public function setYellowOffensePlayer(PlayerModel $player) {
		$this->yellowOffensePlayer = $player;
	}

	/**
	 *
	 * @return PlayerMatchEntity[]
	 */
	public function getPlayerMatchEntities() {
		return $this->playerMatchEntities;
	}

	/**
	 *
	 * @param int $dateCreated
	 */
	public function setDateCreated($dateCreated) {
		$this->dateCreated = $dateCreated;
	}

	/**
	 *
	 * @return int
	 */
	public function getDateCreated() {
		return $this->dateCreated;
	}

	/**
	 *
	 * @param int $dateSaved
	 */
	public function setDateSaved($dateSaved) {
		$this->dateSaved = $dateSaved;
	}

	/**
	 *
	 * @return int
	 */
	public function getDateSaved() {
		return $this->dateSaved;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getSavedByPlayer() {
		return $this->savedByPlayer;
	}

	/**
	 *
	 * @param PlayerModel $savedByPlayer
	 */
	public function setSavedByPlayer(PlayerModel $savedByPlayer) {
		$this->savedByPlayer = $savedByPlayer;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getCreatedByPlayer() {
		return $this->createdByPlayer;
	}

	/**
	 *
	 * @param PlayerModel $createdByPlayer
	 */
	public function setCreatedByPlayer(PlayerModel $createdByPlayer) {
		$this->createdByPlayer = $createdByPlayer;
	}

	/**
	 *
	 * @param MatchEntity $matchEntity
	 */
	public function __construct(MatchEntity $matchEntity = null) {
		if ($matchEntity instanceof MatchEntity) {
			$this->assignByEntity($matchEntity);
		}
	}

	/**
	 *
	 * @param MatchEntity $matchEntity
	 */
	public function assignByEntity(MatchEntity $matchEntity) {
		$db = PG::getDB();

		$this->matchEntity = $matchEntity;
		$this->fetchPlayerMatchEntities();

		$this->setScoreRed($this->matchEntity->getResultTeamRed());
		$this->setScoreYellow($this->matchEntity->getResultTeamYellow());

		$this->setDateCreated($this->matchEntity->getDateCreated());
		$this->setDateSaved($this->matchEntity->getDateSaved());

		$createdByPlayer = PlayerEntity::findById($db, $this->matchEntity->getCreatedByPlayerId());
		if ($createdByPlayer instanceof PlayerEntity) {
			$this->setCreatedByPlayer(new PlayerModel($createdByPlayer));
		}

		$savedByPlayer = PlayerEntity::findById($db, $this->matchEntity->getSavedByPlayerId());
		if ($savedByPlayer instanceof PlayerEntity) {
			$this->setSavedByPlayer(new PlayerModel($savedByPlayer));
		}

		$this->setRandom(1 == $this->matchEntity->getRandom());
		$this->setDeleted(1 == $this->matchEntity->getDeleted());
	}

	/**
	 *
	 * @return PlayerMatchEntity[]
	 */
	public function fetchPlayerMatchEntities() {
		if ($this->getMatchEntity() instanceof MatchEntity && $this->getMatchEntity()->getId() != null) {
			$db = PG::getDB();
			$this->playerMatchEntities = $this->getMatchEntity()->fetchPlayerMatchEntityCollection($db);

			if (count($this->playerMatchEntities) > 0) {
				foreach ($this->playerMatchEntities as $pmEntity) {
					/* @var $pmEntity PlayerMatchEntity */
					$playerEntity = $pmEntity->fetchPlayerEntity($db);

					if (self::POSITION_ALL == $pmEntity->getPosition()) {
						if (self::TEAM_LEFT == $pmEntity->getTeam()) {
							$this->redSinglePlayer = new PlayerModel($playerEntity);
						} else {
							$this->yellowSinglePlayer = new PlayerModel($playerEntity);
						}
						continue;
					} else if (self::POSITION_GOAL == $pmEntity->getPosition()) {
						if (self::TEAM_LEFT == $pmEntity->getTeam()) {
							$this->redGoalPlayer = new PlayerModel($playerEntity);
						} else {
							$this->yellowGoalPlayer = new PlayerModel($playerEntity);
						}
						continue;
					} else if (self::POSITION_OFFENSE == $pmEntity->getPosition()) {
						if (self::TEAM_LEFT == $pmEntity->getTeam()) {
							$this->redOffensePlayer = new PlayerModel($playerEntity);
						} else {
							$this->yellowOffensePlayer = new PlayerModel($playerEntity);
						}
						continue;
					}
				}
			}

			return $this->playerMatchEntities;
		}

		return array();
	}

	/**
	 * check request for player data and match type (1on1, 2on2) 
	 */
	public function assignPlayersFromRequest() {
		$db = PG::getDB();
		$teamRedGoal = ('' != $_POST['teamRedGoal'] && PlayerModel::existsPlayer($db, intval($_POST['teamRedGoal'])));
		$teamRedOffense = ('' != $_POST['teamRedOffense'] && PlayerModel::existsPlayer($db, intval($_POST['teamRedOffense'])));
		$teamYellowGoal = ('' != $_POST['teamYellowGoal'] && PlayerModel::existsPlayer($db, intval($_POST['teamYellowGoal'])));
		$teamYellowOffense = ('' != $_POST['teamYellowOffense'] && PlayerModel::existsPlayer($db, intval($_POST['teamYellowOffense'])));

		/* team red */
		if (true === $teamRedGoal) {
			if (true === $teamRedOffense) {
				$this->setRedGoalPlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamRedGoal']))));
			} else {
				$this->setRedSinglePlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamRedGoal']))));
			}
		}
		if (true === $teamRedOffense) {
			if (true === $teamRedGoal) {
				$this->setRedOffensePlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamRedOffense']))));
			}
		}

		/* team yellow */
		if (true === $teamYellowGoal) {
			if (true === $teamYellowOffense) {
				$this->setYellowGoalPlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamYellowGoal']))));
			} else {
				$this->setYellowSinglePlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamYellowGoal']))));
			}
		}
		if (true === $teamYellowOffense) {
			if (true === $teamYellowGoal) {
				$this->setYellowOffensePlayer(new PlayerModel(PlayerEntity::findById($db, intval($_POST['teamYellowOffense']))));
			}
		}
	}

	/**
	 * get all players for the match
	 *
	 * @return PlayerModel[]
	 */
	public function getPlayers() {
		if ($this->getType() === self::TYPE_1ON1) {
			return array(
				$this->getRedSinglePlayer(),
				$this->getYellowSinglePlayer()
			);
		} else {
			return array(
				$this->getRedGoalPlayer(),
				$this->getRedOffensePlayer(),
				$this->getYellowGoalPlayer(),
				$this->getYellowOffensePlayer()
			);
		}
	}

	/**
	 * fill player match entities
	 */
	private function fillPlayerMatchEntities() {
		$this->playerMatchEntities = array();

		if ($this->getType() === self::TYPE_1ON1) {
			$this->playerMatchEntities[0] = new PlayerMatchEntity();
			$this->playerMatchEntities[0]->assignDefaultValues();
			$this->playerMatchEntities[0]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getRedSinglePlayer()->getId())
					->setPosition(self::POSITION_ALL)
					->setTeam(self::TEAM_LEFT);

			$this->playerMatchEntities[1] = new PlayerMatchEntity();
			$this->playerMatchEntities[1]->assignDefaultValues();
			$this->playerMatchEntities[1]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getYellowSinglePlayer()->getId())
					->setPosition(self::POSITION_ALL)
					->setTeam(self::TEAM_RIGHT);
		} else {
			$this->playerMatchEntities[0] = new PlayerMatchEntity();
			$this->playerMatchEntities[0]->assignDefaultValues();
			$this->playerMatchEntities[0]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getRedGoalPlayer()->getId())
					->setPosition(self::POSITION_GOAL)
					->setTeam(self::TEAM_LEFT);

			$this->playerMatchEntities[1] = new PlayerMatchEntity();
			$this->playerMatchEntities[1]->assignDefaultValues();
			$this->playerMatchEntities[1]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getRedOffensePlayer()->getId())
					->setPosition(self::POSITION_OFFENSE)
					->setTeam(self::TEAM_LEFT);

			$this->playerMatchEntities[2] = new PlayerMatchEntity();
			$this->playerMatchEntities[2]->assignDefaultValues();
			$this->playerMatchEntities[2]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getYellowGoalPlayer()->getId())
					->setPosition(self::POSITION_GOAL)
					->setTeam(self::TEAM_RIGHT);

			$this->playerMatchEntities[3] = new PlayerMatchEntity();
			$this->playerMatchEntities[3]->assignDefaultValues();
			$this->playerMatchEntities[3]->setMatchId($this->getMatchEntity()->getId())
					->setPlayerId($this->getYellowOffensePlayer()->getId())
					->setPosition(self::POSITION_OFFENSE)
					->setTeam(self::TEAM_RIGHT);
		}
	}

	/**
	 *
	 * @return bool
	 */
	public function isValid() {
		$players = array();

		if (self::TYPE_1ON1 === $this->getType()) {
			$players = array(
				$this->getRedSinglePlayer(),
				$this->getYellowSinglePlayer()
			);
		} else if (self::TYPE_2ON2 === $this->getType()) {
			$players = array(
				$this->getRedGoalPlayer(),
				$this->getRedOffensePlayer(),
				$this->getYellowGoalPlayer(),
				$this->getYellowOffensePlayer()
			);
		}

		if (count($players) > 0) {
			foreach ($players as $player) {
				if (!$player instanceof PlayerModel) {
					return false;
				}
			}

			return true;
		}

		return false;
	}

	/**
	 *
	 * @return bool
	 */
	public function register(PDO $db) {
		if ($this->isValid()) {
			$this->matchEntity = new MatchEntity();
			$this->matchEntity->assignDefaultValues();
			$this->matchEntity->setDateCreated($this->getDateCreated())
					->setRandom($this->isRandom())
					->setDeleted(false)
					->setCreatedByPlayerId($this->getCreatedByPlayer()->getId())
					->setSavedByPlayerId($this->getSavedByPlayer()->getId())
					->setResultTeamRed(0)
					->setResultTeamYellow(0);

			$this->matchEntity->insertIntoDatabase($db);
			$this->fillPlayerMatchEntities();

			foreach ($this->getPlayerMatchEntities() as $playerMatchEntity) {
				/* @var $playerMatchEntity PlayerMatchEntity */
				$playerMatchEntity->insertIntoDatabase($db);
			}

			/* foreach ($this->getPlayers() as $player) {
			  $notification = new Notification();
			  $notification->setPlayer($player);
			  $notification->setTitle('Ein Spiel wurde generiert!');
			  $notification->setMessage('Du wurdest in einem Spiel ausgelost!');
			  $notification->setDateSent(DateUtil::formatForDB());
			  $notification->save();
			  } */

			return true;
		}

		return false;
	}

	/**
	 * save match
	 * 
	 * @todo implement correctly
	 */
	public function save(PDO $db) {
		$success = false;

		if ($this->isValid()) {
			$isEdit = (null != $this->getMatchEntity()->getId());

			$this->getMatchEntity()->setResultTeamRed($this->getScoreRed())
					->setResultTeamYellow($this->getScoreYellow())
					->setDeleted($this->isDeleted());

			if (true === $isEdit) {
				$playersChanged = false;
				$matchDummy = new MatchModel(MatchEntity::findById($db, $this->getMatchEntity()->getId()));

				if ($this->getType() === self::TYPE_1ON1) {
					if ($this->getRedSinglePlayer()->getId() != $matchDummy->getRedSinglePlayer()->getId()
							|| $this->getYellowSinglePlayer() != $matchDummy->getYellowSinglePlayer()->getId()) {

						$playersChanged = true;
					}
				} else {
					if ($this->getRedGoalPlayer()->getId() != $matchDummy->getRedGoalPlayer()->getId()
							|| $this->getRedOffensePlayer()->getId() != $matchDummy->getRedOffensePlayer()->getId()
							|| $this->getYellowGoalPlayer()->getId() != $matchDummy->getYellowGoalPlayer()->getId()
							|| $this->getYellowOffensePlayer()->getId() != $matchDummy->getYellowOffensePlayer()->getId()) {

						$playersChanged = true;
					}
				}

				if (true === $playersChanged) {
					$this->clearPlayerMatches();
					$this->fillPlayerMatchEntities();

					foreach ($this->getPlayerMatchEntities() as $playerMatchEntity) {
						/* @var $playerMatchEntity PlayerMatchEntity */
						$playerMatchEntity->insertIntoDatabase($db);
					}
				}

				$this->getMatchEntity()->updateToDatabase($db);

				foreach ($this->getPlayers() as $matchPlayer) {
					/* @var MatchModel $matchPlayer */
					$matchPlayer->getStats(PlayerStatistics::CACHETYPE_SEASON)->calculate();
				}

				$success = true;
			} else {
				$date = DateUtil::formatForDB((null !== $this->getDateSaved()) ? intval($this->getDateSaved()) : time());

				$this->getMatchEntity()->setDateSaved($date)
						->setSavedByPlayerId($this->getSavedByPlayer()->getId())
						->insertIntoDatabase($db);

				$this->fillPlayerMatchEntities();

				foreach ($this->getPlayerMatchEntities() as $playerMatchEntity) {
					$playerMatchEntity->insertIntoDatabase($db);
				}

				$success = true;
			}
		}

		return $success;
	}

	/**
	 *
	 * @param PDO $db
	 */
	public function updateScore(PDO $db) {
		if (null != $this->getMatchEntity()->getId()) {
			$this->getMatchEntity()->setSavedByPlayerId($this->getSavedByPlayer()->getId())
					->setDateSaved($this->getDateSaved())
					->setResultTeamRed($this->getScoreRed())
					->setResultTeamYellow($this->getScoreYellow())
					->updateToDatabase($db);
		}
	}

	/**
	 * delete match and recalculate player stats season/alltime
	 *
	 * @param PDO $db
	 * @param bool $recalculate
	 */
	public function delete(PDO $db, $recalculate = true) {
		$this->clearPlayerMatches();
		$this->getMatchEntity()->deleteFromDatabase($db);

		if (true === $recalculate) {
			$this->recalculatePlayerStats();
		}
	}

	/**
	 * Recalculate the Season stats for the players in the match
	 * 
	 * @param int $type
	 */
	public function recalculatePlayerStats($type = PlayerStatistics::CACHETYPE_SEASON) {
		foreach ($this->getPlayerMatchEntities() as $playerMatchEntity) {
			$player = new PlayerModel(PlayerEntity::findById(PG::getDB(), $playerMatchEntity->getPlayerId()));
			$player->getStats($type)->recalculate();
		}
	}

	/**
	 *
	 * @return array
	 */
	public function toHash() {
		$hash = array();

		$hash['id'] = $this->getMatchEntity()->getId();
		$hash['type'] = $this->getTypeName();
		$hash['dateCreated'] = DateUtil::formatFromDB($this->getDateCreated());
		$hash['dateSaved'] = DateUtil::formatFromDB($this->getDateSaved());
		$hash['createdByPlayer'] = $this->getCreatedByPlayer()->toHash();
		$hash['savedByPlayer'] = $this->getSavedByPlayer()->toHash();
		$hash['random'] = $this->isRandom();
		$hash['deleted'] = $this->isDeleted();

		$hash['score'] = array();
		$hash['score'][self::getNameForTeam(self::TEAM_LEFT)] = $this->getScoreRed();
		$hash['score'][self::getNameForTeam(self::TEAM_RIGHT)] = $this->getScoreYellow();

		$hash['players'] = array();
		if ($this->getType() === self::TYPE_1ON1) {
			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_ALL)] = $this->getRedSinglePlayer()->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_ALL)] = $this->getYellowSinglePlayer()->toHash();
		} else {
			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_GOAL)] = $this->getRedGoalPlayer()->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_LEFT)][self::getNameForPosition(self::POSITION_OFFENSE)] = $this->getRedOffensePlayer()->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_OFFENSE)] = $this->getYellowOffensePlayer()->toHash();
			$hash['players'][self::getNameForTeam(self::TEAM_RIGHT)][self::getNameForPosition(self::POSITION_GOAL)] = $this->getYellowGoalPlayer()->toHash();
		}

		$hash['playerIds'] = array();
		foreach ($this->getPlayers() as $player) {
			/* @var $player PlayerModel */
			$hash['playerIds'][] = $player->getId();
		}

		return $hash;
	}

	/**
	 *
	 * @param int $type
	 */
	public function notifyPlayers($type) {
		if (!MatchManager::isNotificationEnabled()) {
			return false;
		}

		$valid = false;
		$message = null;
		$db = PG::getDB();

		switch ($type) {
			case XMPPHPGTalk::MESSAGETYPE_MATCH_REGISTERED:
				if (true === MatchManager::isMatchUnsaved($this->getMatchEntity())) {
					$valid = true;
					$params = array(
						'match' => $this
					);

					/* do not spam the creator of the match */
					$recipients = array();

					foreach ($this->getPlayers() as $player) {
						if ($player->getId() == $this->getCreatedByPlayer()->getId()) {
							continue;
						}

						$recipients[] = $player;
					}

					$message = new GTalkMatchRegisteredMessage();
					$message->setParameters($params);
					$message->setRecipients($recipients);
				}
				break;

			case XMPPHPGTalk::MESSAGETYPE_MATCH_SAVED:
				$isCurrentMatch = MatchManager::isMatchCurrentMatch($db, $this->getMatchEntity());

				if (true === $isCurrentMatch) {
					$nextMatch = MatchManager::getUnsavedMatches($db, 1);

					if ($nextMatch[0] instanceof MatchEntity) {
						$nextMatchModel = new MatchModel($nextMatch[0]);

						$valid = true;
						$params = array(
							'nextMatch' => $nextMatchModel
						);

						$message = new GTalkMatchSavedMessage();
						$message->setParameters($params);
						$message->setRecipients($nextMatchModel->getPlayers());
					}
				}
				break;

			default:
				return;
		}

		if (true === $valid && $message instanceof XMPPHPGTalkMessage) {
			$gtalk = new XMPPHPGTalk();
			$gtalk->sendMessage($message);
			$gtalk->disconnect();
		}
	}

	/**
	 * delete all playerMatches for a match
	 *
	 * @param PDO $db
	 * @param MatchEntity $match
	 */
	private function clearPlayerMatches() {
		if (null != $this->getMatchEntity()->getId()) {
			PlayerMatchEntity::deleteByFilter(PG::getDB(), array(
				new DFC(PlayerMatchEntity::FIELD_MATCHID, $this->getMatchEntity()->getId())
			));
		}
	}

	/**
	 *
	 * @param MatchEntity $match
	 * @param PlayerEntity $player
	 * @return bool
	 */
	public static function hasPermission(MatchEntity $match, PlayerEntity $player = null) {
		if (null === $player) {
			$player = AuthenticationManager::getInstance()->getPlayer();
		}

		return PlayerModel::isPlayerAdmin() || PlayerMatchManager::hasPlayerParticipatedInMatch(PG::getDB(), $match, $player);
	}

	/**
	 * 
	 * @return array
	 */
	public static function getActiveMatchTypes() {
		$settings = SettingsManager::getGroup('match');
		$types = $settings->getValue('activeMatchTypes');
		$activeTypes = array();

		if (null !== $types) {
			$types = explode('|', $types);

			if (in_array('1on1', $types)) {
				$activeTypes[self::TYPE_1ON1] = '1on1';
			}
			if (in_array('2on2', $types)) {
				$activeTypes[self::TYPE_2ON2] = '2on2';
			}
		} else {
			$settings->setValue('activeMatchTypes', '1on1|2on2');
		}

		return $activeTypes;
	}

}
