<?php

class MatchFilter
{

    const MATCH_RESULT_TYPE_MPACK = 'mp';
    const MATCH_RESULT_TYPE_PACK = 'p';

    private $dateCreatedFrom;
    private $dateCreatedTo;
    private $dateSavedFrom;
    private $dateSavedTo;
    private $createdBy;
    private $savedBy;
    private $deleted;
    private $unsaved;
    private $matchResultType;

    private $orderBy;
    private $limit = array(0, 10);
    private $sortMode = DSC::DESC;

    /**
     * @param DateTime $dateCreatedFrom
     * @return $this
     */
    public function setDateCreatedFrom(DateTime $dateCreatedFrom)
    {
        $this->dateCreatedFrom = $dateCreatedFrom;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreatedFrom()
    {
        return $this->dateCreatedFrom;
    }

    /**
     * @param DateTime $dateCreatedTo
     * @return $this
     */
    public function setDateCreatedTo(DateTime $dateCreatedTo)
    {
        $this->dateCreatedTo = $dateCreatedTo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreatedTo()
    {
        return $this->dateCreatedTo;
    }

    /**
     * @param DateTime $dateSavedFrom
     * @return $this
     */
    public function setDateSavedFrom(DateTime $dateSavedFrom)
    {
        $this->dateSavedFrom = $dateSavedFrom;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateSavedFrom()
    {
        return $this->dateSavedFrom;
    }

    /**
     * @param DateTime $dateSavedTo
     * @return $this
     */
    public function setDateSavedTo(DateTime $dateSavedTo)
    {
        $this->dateSavedTo = $dateSavedTo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateSavedTo()
    {
        return $this->dateSavedTo;
    }

    /**
     * @param PlayerModel $savedBy
     * @return $this
     */
    public function setSavedBy(PlayerModel $savedBy)
    {
        $this->savedBy = $savedBy;

        return $this;
    }

    /**
     * @return PlayerModel
     */
    public function getSavedBy()
    {
        return $this->savedBy;
    }

    /**
     * @param PlayerModel $createdBy
     * @return $this
     */
    public function setCreatedBy(PlayerModel $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return PlayerModel
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int $orderBy
     * @throws InvalidArgumentException
     */
    public function setOrderBy($orderBy)
    {
        $validOrderBy = array('dateCreated', 'dateSaved');

        if (!in_array($orderBy, $validOrderBy)) {
            throw new InvalidArgumentException('NIPE');
        }

        $this->orderBy = $orderBy;
    }

    /**
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param mixed $sortMode
     */
    public function setSortMode($sortMode)
    {
        $this->sortMode = $sortMode;
    }

    /**
     * @return mixed
     */
    public function getSortMode()
    {
        return $this->sortMode;
    }

    /**
     * @return string
     */
    public function getSortModeString()
    {
        return $this->sortMode === DSC::ASC ? 'ASC' : 'DESC';
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $unsaved
     */
    public function setUnsaved($unsaved)
    {
        $this->unsaved = $unsaved;
    }

    /**
     * @return bool
     */
    public function getUnsaved()
    {
        return $this->unsaved;
    }

    /**
     * @param string $matchResultType
     */
    public function setMatchResultType($matchResultType)
    {
        $this->matchResultType = $matchResultType;
    }

    /**
     * @return string
     */
    public function getMatchResultType()
    {
        return $this->matchResultType;
    }

    /**
     * @return array
     */
    public function getMatchResultTypes()
    {
        return array(self::MATCH_RESULT_TYPE_MPACK, self::MATCH_RESULT_TYPE_PACK);
    }

    public function setLimit(array $limit)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function buildSqlQuery()
    {
        $db = PG::getDB();
        $where = array();
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM `match`';

        if ($this->dateCreatedFrom && $this->dateCreatedTo) {
            $this->dateCreatedFrom->setTime(0, 0, 0);
            $this->dateCreatedTo->setTime(23, 59, 59);

            $where[] = sprintf(
                'dateCreated BETWEEN %s AND %s',
                $db->quote(DateUtil::formatForDB($this->dateCreatedFrom)),
                $db->quote(DateUtil::formatForDB($this->dateCreatedTo))
            );
        }

        if ($this->dateSavedFrom && $this->dateSavedTo) {
            $this->dateSavedFrom->setTime(0, 0, 0);
            $this->dateSavedTo->setTime(23, 59, 59);

            $where[] = sprintf(
                'dateSaved BETWEEN %s AND %s',
                $db->quote(DateUtil::formatForDB($this->dateSavedFrom)),
                $db->quote(DateUtil::formatForDB($this->dateSavedTo))
            );
        }

        if ($this->createdBy) {
            $where[] = sprintf('createdByPlayerId = %s', $db->quote($this->createdBy->getId()));
        }

        if ($this->savedBy) {
            $where[] = sprintf('savedByPlayerId = %s', $db->quote($this->savedBy->getId()));
        }

        if (null !== $this->deleted) {
            $where[] = sprintf('deleted = %d', $this->deleted ? 1 : 0);
        }

        if (null !== $this->matchResultType) {
            if ($this->matchResultType === self::MATCH_RESULT_TYPE_MPACK) {
                $where[] = '((resultTeamRed = 0 && resultTeamYellow = 10) || (resultTeamRed = 10 && resultTeamYellow = 0))';
            } else {
                $where[] = '((resultTeamRed < 5 && resultTeamYellow = 10) || (resultTeamRed = 10 && resultTeamYellow < 5))';
            }
        }

        if (null !== $this->unsaved) {
            if ($this->unsaved) {
                $where[] = '(resultTeamRed = 0 AND resultTeamYellow = 0)';
            } else {
                $where[] = '(resultTeamRed > 0 OR resultTeamYellow > 0)';
            }
        }

        if (!empty($where)) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        if ($this->orderBy) {
            $sortMode = $this->sortMode === DSC::ASC ? 'ASC' : 'DESC';
            $sql .= sprintf(' ORDER BY %s %s', $this->orderBy, $sortMode);
        }

        $sql .= ' LIMIT ' . implode(', ', $this->limit);

        return $sql;
    }

    /**
     * @return array
     */
    public function toHash()
    {
        return array(
            'dateCreatedFrom' => $this->dateCreatedFrom ? $this->dateCreatedFrom->format('d.m.Y') : null,
            'dateCreatedTo' => $this->dateCreatedTo ? $this->dateCreatedTo->format('d.m.Y') : null,
            'dateSavedFrom' => $this->dateSavedFrom ? $this->dateSavedFrom->format('d.m.Y') : null,
            'dateSavedTo' => $this->dateSavedTo ? $this->dateSavedTo->format('d.m.Y') : null,
            'createdBy' => $this->createdBy ? $this->createdBy->getId() : null,
            'savedBy' => $this->savedBy ? $this->savedBy->getId() : null,
            'matchResultType' => $this->matchResultType,
            'sortMode' => $this->getSortModeString()
        );
    }

}