<?php

class MatchFilterFactory
{

    /**
     * @param Request $request
     * @return MatchFilter
     */
    public static function createFromRequest(Request $request)
    {
        $filter = new MatchFilter();

        $dateCreatedFrom = $request->getParam('dateCreatedFrom', Request::TYPE_GET);
        $dateCreatedTo = $request->getParam('dateCreatedTo', Request::TYPE_GET);

        if ($dateCreatedFrom && $dateCreatedTo) {
            $filter->setDateCreatedFrom(new DateTime($dateCreatedFrom))
                ->setDateCreatedTo(new DateTime($dateCreatedTo));
        }

        $dateSavedFrom = $request->getParam('dateSavedFrom', Request::TYPE_GET);
        $dateSavedTo = $request->getParam('dateSavedTo', Request::TYPE_GET);

        if ($dateSavedFrom && $dateSavedTo) {
            $filter->setDateSavedFrom(new DateTime($dateSavedFrom))
                ->setDateSavedTo(new DateTime($dateSavedTo));
        }

        $db = PG::getDB();

        $createdBy = $request->getParamInt('createdBy', Request::TYPE_GET);

        if ($createdBy > 0) {
            $createdByPlayer = self::getPlayer($db, $createdBy);

            if ($createdByPlayer) {
                $filter->setCreatedBy($createdByPlayer);
            }
        }

        $savedBy = $request->getParam('savedBy', Request::TYPE_GET);

        if ($savedBy > 0) {
            $savedByPlayer = self::getPlayer($db, $savedBy);

            if ($savedByPlayer) {
                $filter->setSavedBy($savedByPlayer);
            }
        }

        $orderBy = $request->getParam('orderBy', Request::TYPE_GET);

        if (!$orderBy) {
            $orderBy = 'dateCreated';
        }
        $filter->setOrderBy($orderBy);

        $matchResultType = $request->getParam('matchResultType', Request::TYPE_GET);
        if (in_array($matchResultType, $filter->getMatchResultTypes())) {
            $filter->setMatchResultType($matchResultType);
        }

        $sortMode = $request->getParam('sortMode', Request::TYPE_GET);
        $filter->setSortMode(strtoupper($sortMode) === 'ASC' ? DSC::ASC : DSC::DESC);

        return $filter;
    }

    /**
     * @param PDO $db
     * @param int $playerId
     * @return null|PlayerModel
     */
    private static function getPlayer(PDO $db, $playerId)
    {
        $player = PlayerEntity::findById($db, $playerId);

        if ($player) {
            return new PlayerModel($player);
        }

        return null;
    }

}