<?php

/**
 * API - Access
 *
 * @package api
 */
class ApiAccess {

	/**
	 *
	 * @var string
	 */
	private $apiKey;

	/**
	 *
	 * @var string
	 */
	private $apiSecret;

	/**
	 *
	 * @var ApiEntity
	 */
	private $apiEntity;

	/**
	 *
	 * @var PlayerModel
	 */
	private $player;

	/**
	 *
	 * @var OAuthConsumer
	 */
	private $consumer;

	/**
	 *
	 * @var OAuthSignatureMethod
	 */
	private $signatureMethod;

	/**
	 *
	 * @var OAuthRequest
	 */
	private $authRequest;

	/**
	 *
	 * @param type $apiKey 
	 */
	public function __construct($apiKey) {
		$this->apiKey = $apiKey;
	}

	/**
	 *
	 * @return string
	 */
	public function getApiKey() {
		return $this->apiKey;
	}

	/**
	 *
	 * @return string
	 */
	public function getApiSecret() {
		return $this->apiSecret;
	}

	/**
	 *
	 * @return ApiEntity
	 */
	public function getApiEntity() {
		return $this->apiEntity;
	}

	/**
	 *
	 * @return PlayerModel
	 */
	public function getPlayer() {
		return $this->player;
	}

	/**
	 *
	 * @return OAuthConsumer
	 */
	public function getConsumer() {
		return $this->consumer;
	}

	/**
	 *
	 * @return OAuthSignatureMethod
	 */
	public function getSignatureMethod() {
		return $this->signatureMethod;
	}

	/**
	 *
	 * @param OAuthSignatureMethod $signature
	 */
	public function setSignatureMethod(OAuthSignatureMethod $signature) {
		$this->signatureMethod = $signature;
	}

	/**
	 *
	 * @return OAuthSignatureMethod
	 */
	public function getAuthRequest() {
		return $this->authRequest;
	}

	/**
	 *
	 * @param OAuthRequest $request
	 */
	public function setAuthRequest(OAuthRequest $request) {
		$this->authRequest = $request;
	}

	/**
	 *
	 * @return bool
	 */
	private function verifyUser() {
		if (strlen($this->getApiKey()) > 0) {
			$apiEntity = ApiEntity::findByFilter(PG::getDB(), array(
						new DFC(ApiEntity::FIELD_KEY, $this->getApiKey())
					));

			if ($apiEntity[0] instanceof ApiEntity) {
				$this->apiEntity = $apiEntity[0];
				$this->player = new PlayerModel($this->apiEntity->fetchPlayerEntity(PG::getDB()));
				$this->consumer = new OAuthConsumer($this->getApiKey(), $this->apiEntity->getSecret());

				return true;
			}
		}

		return false;
	}

	/**
	 * login the current set player 
	 */
	private function loginUser() {
		if ($this->getPlayer() instanceof PlayerModel) {
			$authManager = new AuthenticationManager();
			$authManager->setPlayer($this->getPlayer()->getEntity());
			AuthenticationManager::setInstance($authManager);
		}
	}

	/**
	 *
	 * @return bool
	 */
	public function isValid() {
		if ($this->verifyUser()) {
			$this->loginUser();

			$method = $_SERVER['REQUEST_METHOD'];
			$uri = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			$sig = trim($_GET['oauth_signature']);

			$this->setAuthRequest(new OAuthRequest($method, $uri));
			$this->setSignatureMethod(new OAuthSignatureMethod_HMAC_SHA1());

			return $this->getSignatureMethod()->check_signature($this->getAuthRequest(), $this->getConsumer(), null, $sig);
		}
	}

}

?>