<?php

/**
 * Description of ApiProvider
 */
class ApiProvider {

	private $provider;
	private $map;
	private $capabilities;

	/**
	 * Setter: provider
	 * @param String $provider
	 * @return void
	 */
	public function setProvider($provider) {
		$this->provider = $provider;
	}

	/**
	 * Setter: map
	 * @param String $map
	 * @return void
	 */
	public function setMap($map) {
		$this->map = $map;
	}

	/**
	 * Setter: capabilities
	 * @param String $capabilities
	 * @return void
	 */
	public function setCapabilities($capabilities) {
		$this->capabilities = $capabilities;
	}

	/**
	 * Getter: provider
	 * @return String
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * Getter: map
	 * @return String
	 */
	public function getMap() {
		return $this->map;
	}

	/**
	 * Getter: capabilities
	 * @return String
	 */
	public function getCapabilities() {
		return $this->capabilities;
	}

}

?>