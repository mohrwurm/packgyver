<?php

/**
 * API - Provider Abstract
 *
 * @package api
 */
abstract class ApiProviderAbstract {

	const PROVIDER_DIR = 'provider';
	const PROVIDER_XML_FILE = 'provider.xml';

	/**
	 *
	 * @var ApiProviderAbstract[]
	 */
	private static $providers;

	/**
	 * load the providers from the 'provider.xml' file
	 */
	private static function loadProviders() {
		$xml = dirname(__FILE__) . DS . self::PROVIDER_DIR . DS . self::PROVIDER_XML_FILE;

		$dom = DOMUtil::newDOMDocument();
		$dom->load($xml);

		foreach ($dom->getElementsByTagName('Provider') as $provider) {
			/* @var $provider DOMElement */
			$capabilities = array();

			foreach ($provider->getElementsByTagName('Capability') as $capability) {
				/* @var $capability DOMElement */
				$capabilities[] = $capability->nodeValue;
			}

			$providerItem = new ApiProvider();
			$providerItem->setProvider($provider->getAttribute('class'));
			$providerItem->setMap($provider->getAttribute('map'));
			$providerItem->setCapabilities($capabilities);

			self::$providers[$providerItem->getMap()] = $providerItem;
		}
	}

	/**
	 *
	 * @return ApiProviderAbstract[]
	 */
	public static function getProviders() {
		if (null === self::$providers) {
			self::loadProviders();
		}

		return self::$providers;
	}

	/**
	 *
	 * @param string $id
	 * @return \ApiProviderAbstract|null 
	 */
	public static function getProviderByMapping($id) {
		if (null === self::$providers) {
			self::loadProviders();
		}

		if (isset(self::$providers[$id])) {
			return self::$providers[$id];
		}

		return null;
	}

}

?>