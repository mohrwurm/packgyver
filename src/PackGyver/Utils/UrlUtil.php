<?php

/**
 * PackGyver - URL Util
 *
 * @package auth
 */
class UrlUtil {

	/**
	 * redirect to the given URI and exit
	 *
	 * @param string $uri
	 */
	public static function redirect($uri) {
		header('Location: ' . $uri);
		exit;
	}

}
