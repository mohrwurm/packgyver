<?php

/**
 * PackGyver - DOM Util
 */
class DOMUtil extends DOMDocument {

	/**
	 * Create new DOMDocument
	 *
	 * @return DOMDocument $dom
	 */
	public static function newDOMDocument() {
		$dom = new DOMDocument('1.0', 'UTF-8');
		return $dom;
	}

	/**
	 * @param DOMElement $node
	 * @param string $name
	 * @return DOMElement
	 */
	public static function getElementByTagName(DOMElement $node, $name) {
		$elements = $node->getElementsByTagName($name);
		if ($elements->length > 0) {
			return $elements->item(0);
		}
		return null;
	}

	/**
	 * @param DOMElement $node
	 * @param string $name
	 * @return string
	 */
	public static function getElementValueByTagName(DOMElement $node, $name) {
		$element = self::getElementByTagName($node, $name);
		if ($element instanceof DOMElement) {
			return $element->nodeValue;
		}
		return null;
	}

}
