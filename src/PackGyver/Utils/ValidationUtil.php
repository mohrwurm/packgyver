<?php

/**
 * PackGyver - Validation Util
 */
class ValidationUtil {

	/**
	 * @param string $email
	 * @return bool
	 */
	public static function validateEmail($email) {
		$expression = '/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD';
		return 1 === preg_match($expression, $email);
	}

	/**
	 * @param string $value
	 * @return null|string
	 */
	public static function getInput($value) {
		return ('' != trim($value)) ? trim(htmlspecialchars($value)) : null;
	}

	/**
	 * @param string $value
	 * @return int|null
	 */
	public static function getInputInt($value) {
		return ('' != $value) ? (int) $value : null;
	}

}
