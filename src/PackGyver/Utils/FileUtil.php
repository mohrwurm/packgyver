<?php

/**
 * PackGyver - File Util
 */
class FileUtil {

	/**
	 * Filter file extension
	 *
	 * @param string $filename
	 * @return string | null
	 */
	public static function getFileExtension($filename) {
		if (strpos($filename, ".")) {
			return substr($filename, strrpos($filename, '.') + 1);
		}
		return null;
	}

	/**
	 * Get file contents of defined file
	 *
	 * @param string $filename
	 * @return string | null
	 */
	public static function getFileContents($filename) {
		if (file_exists($filename)) {
			return file_get_contents($filename);
		}
		return null;
	}

	/**
	 * Set file contents of defined file
	 *
	 * @param string $filename
	 * @param string $content
	 * @param bool $append
	 * @return string bool
	 */
	public static function setFileContents($filename, $content, $append = false) {
		return file_put_contents($filename, $content, $append ? FILE_APPEND : null);
	}

	/**
	 * delete directory recursive
	 *
	 * @param string $directory
	 * @param string $filter PCRE filter
	 */
	public static function deleteRecursive($directory, $filter = null) {
		$sourceDir = realpath($directory);

		if (is_dir($sourceDir)) {
			foreach (scandir($sourceDir) as $entry) {
				if ($entry == '.' || $entry == '..' || (null !== $filter && 0 != preg_match($filter, $entry))) {
					continue;
				}
				$src = $sourceDir . DIRECTORY_SEPARATOR . $entry;
				if (is_dir($src)) {
					self::deleteRecursive($src);
				} else {
					unlink($src);
				}
			}

			rmdir($sourceDir);
		}
	}

	/**
	 * count files in a directory - not recursive
	 *
	 * @param string $dir
	 * @param bool $noDirs
	 * @return int
	 */
	public static function countFilesInDir($dir, $noDirs = false) {
		$c = 0;

		if (is_dir($dir)) {
			foreach (scandir($dir) as $file) {
				if (true === $noDirs && is_dir($dir . DS . $file)) {
					continue;
				}
				if ($file != '.' && $file != '..') {
					$c++;
				}
			}
		}

		return $c;
	}

	/**
	 * @param string $dir
	 * @param bool $absoluteNames
	 * @param bool $noDirs
	 * @param bool $recursive
	 * @return array
	 */
	public static function getFilesFromDir($dir, $absoluteNames = false, $noDirs = true, $recursive = false) {
		$files = array();

		if (is_dir($dir)) {
			foreach (scandir($dir) as $file) {
				$path = $dir . DS . $file;

				if ($file !== '.' && $file !== '..') {
					if (is_file($path)) {
						if (true === $absoluteNames) {
							$files[] = $path;
						} else {
							$files[] = $file;
						}
					} else if (is_dir($path) && false === $noDirs) {
						if (true === $absoluteNames) {
							$files[] = $path;
						} else {
							$files[] = $file;
						}

						if (true === $recursive) {
							foreach (self::getFilesFromDir($path, $absoluteNames, $noDirs, $recursive) as $rFile) {
								$files[] = $rFile;
							}
						}
					}
				}
			}
		}

		return $files;
	}

}
