<?php

/**
 * PackGyver - Pagination Util
 */
class Pagination extends Zebra_Pagination {

	/**
	 * {@inheritdoc}
	 */
	public function __construct() {
		parent::__construct();

		$this->records_per_page(10);
		$this->labels('<i class="icon-chevron-left"></i>', '<i class="icon-chevron-right"></i>');
		$this->padding(false);
	}

}
