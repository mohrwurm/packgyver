<?php

/**
 * PackGyver - DateAgo Util
 */
class DateAgoUtil extends DateTime {

	/**
	 * @var array
	 */
	protected $strings_en = array(
		'y' => array('1 year ago', '%d years ago'),
		'n' => array('1 month ago', '%d months ago'),
		'd' => array('1 day ago', '%d days ago'),
		'h' => array('1 hour ago', '%d hours ago'),
		'i' => array('1 minute ago', '%d minutes ago'),
		's' => array('now', '%d secons ago'),
	);

	/**
	 * @var array
	 */
	protected $strings_de = array(
		'y' => array('1 year ago', 'Vor %d Jahren'),
		'n' => array('1 month ago', 'Vor %d Monaten'),
		'd' => array('1 day ago', 'Vor %d Tagen'),
		'h' => array('1 hour ago', 'Vor %d Stunden'),
		'i' => array('1 minute ago', 'Vor %d Minuten'),
		's' => array('now', 'Vor %d Sekunden'),
	);

	/**
	 * Returns the difference from the current time in the format X time ago
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->getFormatted();
	}

	/**
	 * Returns the difference from the current time in the format X time ago
	 *
	 * @return string
	 */
	public function getFormatted() {
		$now = new DateTime('now');
		$diff = $this->diff($now);

		foreach ($this->strings_de as $key => $value) {
			if (($text = $this->getDiffText($key, $diff))) {
				return $text;
			}
		}

		return '';
	}

	/**
	 * Try to construct the time diff text with the specified interval key
	 * 
	 * @param string $intervalKey A value of: [y,m,d,h,i,s]
	 * @param DateInterval $diff
	 * @return string|null
	 */
	protected function getDiffText($intervalKey, $diff) {
		$pluralKey = 1;
		$value = $diff->$intervalKey;

		if ($value > 0) {
			if ($value < 2) {
				$pluralKey = 0;
			}
			return sprintf($this->strings_de[$intervalKey][$pluralKey], $value);
		}

		return null;
	}

}
