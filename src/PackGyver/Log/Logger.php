<?php

use Monolog\Logger as MonoLogger;
use Monolog\Handler\StreamHandler;

/**
 * PackGyver - Logger
 */
class Logger extends MonoLogger {

	private static $instance;

	/**
	 * @return MonoLogger
	 */
	public static function get() {
		if (null === self::$instance) {
			self::$instance = new self('packgyver');
			self::$instance->pushHandler(new StreamHandler(self::getPath()));
		}

		return self::$instance;
	}

	/**
	 * @param int $days
	 * @param int $limit
	 * @return array
	 */
	public static function getLogs($days = 3, $limit = 100) {
		$reader = new ReverseLogReader(self::getPath(), $days);
		$logs = array();
		$i = 0;

		foreach ($reader as $log) {
			if (empty($log)) {
				continue;
			}

			if ($i === $limit) {
				break;
			}

			$logs[] = $log;
			$i++;
		}

		return $logs;
	}

	/**
	 * @return string
	 */
	private static function getPath() {
		return PG::getAppDir() . 'logs/app.log';
	}

}
