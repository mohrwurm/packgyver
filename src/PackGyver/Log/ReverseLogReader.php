<?php

use Dubture\Monolog\Reader\LogReader;

/**
 * PackGyver - Reverse Log Reader
 */
class ReverseLogReader extends LogReader {

	/**
	 * {@inheritdoc}
	 */
	public function __construct($file, $days = 1, $pattern = 'default') {
		$this->file = new \ReverseFileObject($file, 'r');
		$i = 0;

		while (!$this->file->eof()) {
			$this->file->current();
			$this->file->next();
			$i++;
		}

		$this->days = $days;
		$this->pattern = $pattern;

		$this->lineCount = $i;
		$this->parser = $this->getDefaultParser($days, $pattern);
	}

}